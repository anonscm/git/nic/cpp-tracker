#!/bin/bash

# use this script when you work on the cpp tracker AND the iOS app at the same time and want to integrate
# local tracker changes into the locally bulilt app without having to do the commit/wait for jenkins/pod update
# dance

TARGET=../sellfio-ios/Sellfio/Pods/BLETracker/libtracker

if [ -d $TARGET ]
then
    chmod a+w $TARGET/include/*
    cp -r build-ios/dist/include $TARGET
    chmod a+w $TARGET/lib/*
    cp -r build-ios/dist/lib $TARGET
else
    echo $TARGET does not exist
fi
