import json
from pprint import pprint

with open("histogramLog_1.json") as data_file: 
	
    histograms_json_object1 = json.load(data_file)


with open("histogramLog_2.json") as data_file:    

	histograms_json_object2 = json.load(data_file)


with open("fingerprints_data.json") as data_file:    
    fingerprints_json_object = json.load(data_file)

vectorFingerprintsTransmiterID = {}

vectorTransmiterIDSum1 = {}
vectorTransmiterIDCount1 = {}
vectorTransmiterIDMeanValue1 = {}


vectorTransmiterIDSum2 = {}
vectorTransmiterIDCount2 = {}
vectorTransmiterIDMeanValue2 = {}




#Compute fingerprints
for histogram in fingerprints_json_object:
    # now song is a dictionary
    #print histogram
    #print
    for attribute, histogramValues in histogram.iteritems():
    	if attribute == "histogram":

    		#iterate AP
    		#json_ap = json.load(value)
    		for transmiterID, transmiterValues in histogramValues.iteritems():

		        if transmiterID not in vectorFingerprintsTransmiterID.keys():
		        	vectorFingerprintsTransmiterID[transmiterID] = 1




#Compute histograms for measures 1
for histogram in histograms_json_object1:
    # now song is a dictionary
    #print histogram
    #print
    for attribute, histogramValues in histogram.iteritems():
    	if attribute == "histogram":

    		#iterate AP
    		#json_ap = json.load(value)
    		for transmiterID, transmiterValues in histogramValues.iteritems():
    			value = 0.0
		        for dB, weight in transmiterValues.iteritems():
		        	value = value + (float(dB)*float(weight))

		        init = False

		        if transmiterID in vectorFingerprintsTransmiterID:
			        if transmiterID not in vectorTransmiterIDSum1.keys():
			        	vectorTransmiterIDSum1[transmiterID] = value
			        	vectorTransmiterIDCount1[transmiterID] = 1
			        	init = True

			        if init == False:
			        	vectorTransmiterIDSum1[transmiterID] = vectorTransmiterIDSum1[transmiterID] + value
			        	vectorTransmiterIDCount1[transmiterID] = vectorTransmiterIDCount1[transmiterID] + 1



#Compute histograms for measures 5
for histogram in histograms_json_object2:
    # now song is a dictionary
    #print histogram
    #print
    for attribute, histogramValues in histogram.iteritems():
    	if attribute == "histogram":

    		#iterate AP
    		#json_ap = json.load(value)
    		for transmiterID, transmiterValues in histogramValues.iteritems():
    			value = 0.0
		        for dB, weight in transmiterValues.iteritems():
		        	value = value + (float(dB)*float(weight))

		        init = False

		        if transmiterID in vectorFingerprintsTransmiterID:
			        if transmiterID not in vectorTransmiterIDSum2.keys():
			        	vectorTransmiterIDSum2[transmiterID] = value
			        	vectorTransmiterIDCount2[transmiterID] = 1
			        	init = True

			        if init == False:
			        	vectorTransmiterIDSum2[transmiterID] = vectorTransmiterIDSum2[transmiterID] + value
			        	vectorTransmiterIDCount2[transmiterID] = vectorTransmiterIDCount2[transmiterID] + 1




#Transmiter in measures1 but not in measures5
print "Transmiter in measures1 but not in measures2"
for transmiterID in vectorTransmiterIDSum1:
	if transmiterID not in vectorTransmiterIDSum2:
		print transmiterID, " in 1 but not in 2"

print

#Transmiter in measures5 but not in measures1
print "Transmiter in measures2 but not in measures1"
for transmiterID in vectorTransmiterIDSum2:
	if transmiterID not in vectorTransmiterIDSum1:
		print transmiterID, " in 2 but not in 1"		

print		


#Comparision 1-2
print "Comparision 1-2"
count = 0
for transmiterID in vectorTransmiterIDSum1:
	count = count + 1
	vectorTransmiterIDMeanValue1[transmiterID] = vectorTransmiterIDSum1[transmiterID] / vectorTransmiterIDCount1[transmiterID]
	print "(1) ", transmiterID, vectorTransmiterIDMeanValue1[transmiterID], " Apears" , vectorTransmiterIDCount1[transmiterID], " times"

	if transmiterID in vectorTransmiterIDSum2:
		vectorTransmiterIDMeanValue2[transmiterID] = vectorTransmiterIDSum2[transmiterID] / vectorTransmiterIDCount2[transmiterID]
		print "(2) ", transmiterID, vectorTransmiterIDMeanValue2[transmiterID], " Apears" , vectorTransmiterIDCount2[transmiterID], " times"

	print

print
print "Total Transmiters in 1: ", count

print
print


#Comparision 2-1
print "Comparision 2-1"
count = 0
for transmiterID in vectorTransmiterIDSum2:
	count = count + 1
	vectorTransmiterIDMeanValue2[transmiterID] = vectorTransmiterIDSum2[transmiterID] / vectorTransmiterIDCount2[transmiterID]
	print "(2) ", transmiterID, vectorTransmiterIDMeanValue2[transmiterID], " Apears" , vectorTransmiterIDCount2[transmiterID], " times"

	if transmiterID in vectorTransmiterIDSum1:
		vectorTransmiterIDMeanValue1[transmiterID] = vectorTransmiterIDSum1[transmiterID] / vectorTransmiterIDCount1[transmiterID]
		print "(1) ", transmiterID, vectorTransmiterIDMeanValue1[transmiterID], " Apears" , vectorTransmiterIDCount1[transmiterID], " times"

	print


print
print "Total Transmiters in 2: ", count
