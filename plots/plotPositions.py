import matplotlib.pyplot as plt
import numpy as np
from math import *

smartphoneData = np.loadtxt("positions.txt")
cppTrackerData = np.loadtxt("../build/dist/bin/log/positions.txt")

plt.xlabel('Longitude')
plt.ylabel('Latitude')
font = {'size'   : 16}
plt.rc('font', **font)

plt.plot(smartphoneData[:,2],smartphoneData[:,1], '-ro')
plt.plot(cppTrackerData[:,1],cppTrackerData[:,0], '-bo')

plt.show()
