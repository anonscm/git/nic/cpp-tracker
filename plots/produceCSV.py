#!/usr/bin/env python

# sudo apt-get install python-setuptools
# sudo easy_install pip
# sudo pip install pyyaml

import os
import yaml
import os.path
import argparse

# cast string to Double
def getDouble(str):
	return float(str)

# cast string to Integer
def getInteger(str):
	return int(str)

# cast string to Boolean
def getBoolean(str)	:
	return bool(str)


#Get the parameter in its correct type
def getOutParametersType(name, value):
	if name == 'motion_model' or \
		 name == 'num_particles' or \
		 name == 'pct_random_particles' or \
		 name == 'cluster_radius' or \
		 name == 'sigma_sensor' or \
		 name == 'sigma_init' or \
		 name == 'sigma_action' or \
		 name == 'sigma_random_particles' or \
		 name == 'localisation_update_rate' or \
		 name == 'outlier_detection_threshold' or \
		 name == 'sufficient_measurements_init' or \
		 name == 'num_preprocessed_fingerprints' or \
		 name == 'num_neighbours' or \
		 name == 'movement_threshold' or \
		 name == 'localisation_mode':

		 if not isinstance( value, int ):
	 		return getInteger(value)
	
	if name == 'has_gyroscope' or \
		name == 'qfd_use_log_scale' or \
		name == 'enable_outlier_detection':

		if not isinstance( value, bool ):
	 		return getBoolean(value)

	if name == 'low_pass_filter_weight_accelerometer' or \
		name == 'low_pass_filter_weight_magnetometer' or \
		name == 'low_pass_filter_weight_compass' or \
		name == 'motion_detection_threshold' or \
		name == 'ogm_black_pixel_value' or \
		name == 'ogm_white_pixel_value' or \
		name == 'ogm_grey_pixel_value':

		if not isinstance( value, float ):
	 		return getDouble(value)
	
	return value

#Get the number of lines in a file
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

#Get the id from the meanFileName
#Mean file mean_2015-08-07-14-52-34_outlier_detection_threshold=1_enable_outlier_detection=true.txt
def getIdFromMeanFileName(fileName):
	return fileName.split("_")[1]

#Initialize the output parameters from YALM file
def initializeOutParametersFromYalm(originalSource, id):
	yalmFileName = originalSource+"config-"+id+".yaml"

	#Tracker [0.8.1.343.126-4b16d30] config 2015-08-07-15-02-13
	#    0             1                2              3
	#Check tracker version
	f = open(yalmFileName, 'r')
	content = f.read()
	f.close()
	contentSplit = content.split(' ')
	trackerVersion = contentSplit[1]

	# Check signal, wifi or btle
	histogramFileName = "histogramLog_wifi-"+id+".json"
	signal = 'wifi'
	if os.path.isfile(originalSource+histogramFileName)==False:
		signal = 'btle'

	# Check scenario, static or moving, counting the rows in groundTruth file
	scenario = 'static'
	groundTruthFileName = "groundTruthLongLat_"+id+".txt"
	lines = file_len(originalSource+groundTruthFileName)
	if lines>1:
		scenario = 'moving'


	with open(yalmFileName, 'r') as stream:
	    yalmParameters = yaml.load(stream)

	outParameters = {'ID':id, \
					 'map':'ITAB', \
					 'mobile device':'standalone', \
					 'tracker version':trackerVersion, \
					 'date':id, \
					 'mean':0, \
					 'standard deviation':0, \
					 'scenario':scenario,  \
					 'signal':signal,  \
					 'low_pass_filter_weight_accelerometer':getOutParametersType('low_pass_filter_weight_accelerometer', yalmParameters['low_pass_filter_weight_accelerometer']), \
					 'low_pass_filter_weight_magnetometer':getOutParametersType('low_pass_filter_weight_magnetometer', yalmParameters['low_pass_filter_weight_magnetometer']), \
					 'low_pass_filter_weight_compass':getOutParametersType('low_pass_filter_weight_compass', yalmParameters['low_pass_filter_weight_compass']), \
					 'motion_detection_threshold':getOutParametersType('motion_detection_threshold', yalmParameters['motion_detection_threshold']), \
					 'motion_model':getOutParametersType('motion_model', yalmParameters['motion_model']), \
					 'has_gyroscope':getOutParametersType('has_gyroscope', yalmParameters['has_gyroscope']),  \
					 'ogm_black_pixel_value':getOutParametersType('ogm_black_pixel_value', yalmParameters['ogm_pixel_black']), \
					 'ogm_white_pixel_value':getOutParametersType('ogm_white_pixel_value', yalmParameters['ogm_pixel_white']), \
					 'ogm_grey_pixel_value':getOutParametersType('ogm_grey_pixel_value', yalmParameters['ogm_pixel_grey']), \
					 'num_particles':getOutParametersType('num_particles', yalmParameters['num_particles']), \
					 'pct_random_particles':getOutParametersType('pct_random_particles', yalmParameters['num_random_particles']), \
					 'cluster_radius':getOutParametersType('cluster_radius', yalmParameters['cluster_radius']), \
					 'sigma_sensor':getOutParametersType('sigma_sensor', yalmParameters['sigma_sensor']), \
					 'sigma_init':getOutParametersType('sigma_init', yalmParameters['sigma_init']), \
					 'sigma_action':getOutParametersType('sigma_action', yalmParameters['sigma_action']), \
					 'sigma_random_particles':getOutParametersType('sigma_random_particles', yalmParameters['sigma_random_particles']), \
					 'localisation_update_rate':getOutParametersType('localisation_update_rate', yalmParameters['localisation_update_rate']), \
					 'enable_outlier_detection':getOutParametersType('enable_outlier_detection', yalmParameters['enable_outlier_detection']), \
					 'outlier_detection_threshold':getOutParametersType('outlier_detection_threshold', yalmParameters['outlier_detection_threshold']), \
					 'sufficient_measurements_init':getOutParametersType('sufficient_measurements_init', yalmParameters['sufficient_measurements_init']), \
					 'num_preprocessed_fingerprints':getOutParametersType('num_preprocessed_fingerprints', yalmParameters['num_preprocessed_fingerprints']), \
					 'num_neighbours':getOutParametersType('num_neighbours', yalmParameters['num_neighbours']), \
					 'movement_threshold':getOutParametersType('movement_threshold', yalmParameters['movement_threshold']), \
					 'qfd_use_log_scale':getOutParametersType('qfd_use_log_scale', yalmParameters['qfd_use_log_scale']),  \
					 'localisation_mode':getOutParametersType('localisation_mode', yalmParameters['localisation_mode'])}

	return outParameters 


#Load the parameters for each one
# Load parameters from mean_2015-08-07-14-52-34_outlier_detection_threshold=1_enable_outlier_detection=true.txt
def loadLocalParameters(id, meanSource, meanFileName, outParameters):
	f = open(meanSource+meanFileName, 'r')
	content = f.read()
	f.close()


	# Mean: 6.29048173479 Standard deviation: 4.01482464982
	#   0        1           2        3            4
	contentSplit = content.split(' ')

	#mean
	outParameters['mean'] = contentSplit[1]

	#standard deviation
	outParameters['standard deviation'] = contentSplit[4]

	#local parameters
	# mean_2015-08-07-14-52-34_outlier_detection_threshold=1_enable_outlier_detection=true.txt
	#	outlier_detection_threshold=1
	# 	enable_outlier_detection=true

	# mean_2015-08-07-14-52-34_movement_threshold=60.txt
	#	movement_threshold=60
	

	#Clean the string getting only the parameters part
	# mean_2015-08-07-14-52-34_outlier_detection_threshold=1_enable_outlier_detection=true.txt
	# outlier_detection_threshold=1_enable_outlier_detection=true
	params = meanFileName.split("mean_"+id+"_")
	nameAux = params[1]
	params = nameAux.split(".txt")
	nameAux = params[0]

	numParameters = len(nameAux.split('='))-1
	i=1
	while i<=numParameters:
		name = nameAux.split('=')[0]
		if i == numParameters:
			value = nameAux.split('=')[1]
		else:
			value = nameAux.split('=')[1].split('_')[0]
			nameAux = nameAux.split(name+"="+value)[1]

		#Update outParameters
		outParameters[name]=value
		i=i+1

	return outParameters


if __name__ == "__main__":


	parser = argparse.ArgumentParser(description='This is a demo script by nixCraft.')
	parser.add_argument('-meanSource','--meanSource', help='meanSource folder required',required=True)
	parser.add_argument('-originalSource','--originalSource',help='originalSource required', required=True)
	parser.add_argument('-outSourceFile','--outSourceFile',help='Output file required', required=True)
	args = parser.parse_args()


	#meanSource = "/home/dseca/Schreibtisch/data/mean-sources/"
	#originalSource = "/home/dseca/Schreibtisch/data/all-sources/"
	#outSourceFile = "/home/dseca/Schreibtisch/data/outCSV.csv"

	meanSource = args.meanSource
	originalSource = args.originalSource
	outSourceFile = args.outSourceFile

	yalmParameters = []
	outParameters = []


	#Generate csv file 
	f = open(outSourceFile, 'w')
	f.write("ID,map,mobile device,tracker version,date,mean,standard deviation,scenario,signal,low_pass_filter_weight_accelerometer ,low_pass_filter_weight_magnetometer ,low_pass_filter_weight_compass ,motion_detection_threshold ,motion_model ,has_gyroscope ,ogm_black_pixel_value ,ogm_white_pixel_value ,ogm_grey_pixel_value ,num_particles ,pct_random_particles ,cluster_radius ,sigma_sensor ,sigma_init ,sigma_action ,sigma_random_particles ,localisation_update_rate ,enable_outlier_detection ,outlier_detection_threshold ,sufficient_measurements_init ,num_preprocessed_fingerprints ,num_neighbours ,movement_threshold ,qfd_use_log_scale ,localisation_mode")
	

	files = os.listdir(meanSource)

	for meanFileName in files:
		if "mean" in meanFileName:
			print 'Computing '+meanFileName+' ...'
			# mean_2015-08-07-14-52-34_outlier_detection_threshold=1_enable_outlier_detection=true.txt

			# id
			id = getIdFromMeanFileName(meanFileName)

			# initialize out parameters from yalm parameters
			outParameters = initializeOutParametersFromYalm(originalSource, id)

			# local parameteres
			outParameters = loadLocalParameters(id, meanSource, meanFileName, outParameters)
			

			f.write('\n'+str(outParameters['ID'])+','+\
					str(outParameters['map'])+','+\
					str(outParameters['mobile device'])+','+\
					str(outParameters['tracker version'])+','+\
					str(outParameters['date'])+','+\
					str(outParameters['mean'])+','+\
					str(outParameters['standard deviation'])+','+\
					str(outParameters['scenario'])+','+\
					str(outParameters['signal'])+','+\
					str(outParameters['low_pass_filter_weight_accelerometer'])+','+\
					str(outParameters['low_pass_filter_weight_magnetometer'])+','+\
					str(outParameters['low_pass_filter_weight_compass'])+','+\
					str(outParameters['motion_detection_threshold'])+','+\
					str(outParameters['motion_model'])+','+\
					str(outParameters['has_gyroscope'])+','+\
					str(outParameters['ogm_black_pixel_value'])+','+\
					str(outParameters['ogm_white_pixel_value'])+','+\
					str(outParameters['ogm_grey_pixel_value'])+','+\
					str(outParameters['num_particles'])+','+\
					str(outParameters['pct_random_particles'])+','+\
					str(outParameters['cluster_radius'])+','+\
					str(outParameters['sigma_sensor'])+','+\
					str(outParameters['sigma_init'])+','+\
					str(outParameters['sigma_action'])+','+\
					str(outParameters['sigma_random_particles'])+','+\
					str(outParameters['localisation_update_rate'])+','+\
					str(outParameters['enable_outlier_detection'])+','+\
					str(outParameters['outlier_detection_threshold'])+','+\
					str(outParameters['sufficient_measurements_init'])+','+\
					str(outParameters['num_preprocessed_fingerprints'])+','+\
					str(outParameters['num_neighbours'])+','+\
					str(outParameters['movement_threshold'])+','+\
					str(outParameters['qfd_use_log_scale'])+','+\
					str(outParameters['localisation_mode']))

	f.close()

	print ''
	print 'Done!'
	print 'You can find the result in '+ outSourceFile