import json
from pprint import pprint
import matplotlib.pyplot as plt
import numpy as np
from math import *
from scipy.misc import imread

def denormalize(values, numberscans):
	# values [[dB, val], [dB, val], ...]
	factor = 0
	for i in range(2, numberscans+1):
		foundFactor = True
		for k, a in values.iteritems():
			b = i*float(a)
			r = abs(b - round(b))
			if r > 0.001:
				foundFactor = False
				break
			
		if foundFactor:
			factor = i
			break

	result=[]
	for k, a in values.iteritems():
		count = int(round(a*factor))
		for j in range(count):
			result.append(k)
	
	return result
	
def removeAccessPoints(data, fingerprintNames, accessPoints):
	print 'call removeAccessPoints'
	for fp in data:
		for fpName in fingerprintNames:
			if fp["id"] == fpName:
				removeKeys = []
				histogram = fp["histogram"]
				for key, value in histogram.iteritems():
					for i in accessPoints:
						if key == i:
							removeKeys.append(key)
					
				for i in removeKeys:
					print 'remove ' + i
					del histogram[i]


def removeAllAccessPoints(data, accessPoints):
	print 'call removeAllAccessPoints'
	for fp in data:
		removeKeys = []
		histogram = fp["histogram"]

		toHoldSet = set(accessPoints)

		for key, value in histogram.iteritems():
			if key not in toHoldSet:
				removeKeys.append(key)

		for i in removeKeys:
			print 'remove ' + i
			del histogram[i]
			
def save(data, name):
	with open(name, 'wb') as filep:
		json.dump(data, filep)



json_data = open('fingerprints_data_15-02-20_2.json')
data = json.load(json_data)
json_data.close()

#for wifi
removeAllAccessPoints(data, ['80:1f:02:a7:48:86', '80:1f:02:a7:42:68', '80:1f:02:a7:3f:96', '80:1f:02:a7:40:5c', '80:1f:02:a7:42:e6'])

#for btle
#removeAllAccessPoints(data, ['bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_1', 'bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_2', 'bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_3', 'bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_4'])

# (FP, (AP, mean))
means=[]

# (FP, (AP, std))
covariance=[]
stddev=[]

constancy=[]

# for all fingerprints
for i in data:
	#print "\n"
	#print i["id"]
	#print "\n"
	
	tmpmean=[]
	tmpdenorm=[]
	tmpcov=[]
	tmpstd=[]
	tmpconstancy=[]
	
	noAP = 0
	
	# for all access points
	for key, value in i["histogram"].iteritems():
		tmpdenorm.append((key, denormalize(value, 10)))
		
		mean = 0
		for k, v in value.iteritems():
			mean += float(k)*float(v)			
		tmpmean.append((key, mean))
		
		covsum = 0
		for db in tmpdenorm[noAP][1]:
			covsum += (float(db) - mean)**2	
		cov = covsum/(len(tmpdenorm[noAP][1])-1)
		tmpcov.append((key, cov))
		tmpstd.append((key, sqrt(cov)))
		tmpconstancy.append((key, len(tmpdenorm[noAP][1])))

		noAP += 1
	
	means.append((i["id"],tmpmean))
	covariance.append((i["id"],tmpcov))
	stddev.append((i["id"],tmpstd))
	constancy.append((i["id"],tmpconstancy))

	
#print means
#print covariance
print stddev
#print constancy

accesspoints=[]

for i in data:
	for key, value in i["histogram"].iteritems():
		accesspoints.append(key)

accesspoints = set(accesspoints)
#print accesspoints
print len(accesspoints)


heat=[]

apno = 0
for ap in accesspoints:
	tmpheat=[]

	idx = 0
	for i in data:	 
		# for all access points
		for j in range(len(means[idx][1])):
			if means[idx][1][j][0] == ap:
				tmpheat.append((i["point"]["mLatitudeE6"], i["point"]["mLongitudeE6"], means[idx][1][j][1]))
		idx += 1
	heat.append((ap, tmpheat))
	print str(apno) + " " + str(ap) + " " + str(len(tmpheat))
	apno += 1

#print heat
#img = imread('pennymap.jpg')
#plt.imshow(img, zorder=0, extent=[9343586, 9371829, 45497706, 45543790])
img = imread('Tarent_Messestand_FINAL_draufsicht.jpg')
plt.imshow(img, zorder=0, extent=[7060955, 7062179, 50721000, 50722520])

#for r in range(len(accesspoints)):
shownap = 0
lat=[]
longi=[]
weight=[]

for i in range(len(heat[shownap][1])):
	lat.append(heat[shownap][1][i][0])
	longi.append(heat[shownap][1][i][1])
	weight.append(heat[shownap][1][i][2])

#print heat[shownap][0]
#print lat
#print longi
#print weight

wcounts, xedges, yedges = np.histogram2d(longi, lat, bins=[84,195], normed=False, weights=weight)
extent = min(longi), max(longi), min(lat), max(lat)
#extent = xedges[0], xedges[-1], yedges[0], yedges[-1]
wcounts[ wcounts==0 ] = np.nan

#plt.clf()
plt.imshow(wcounts.T, extent=extent, origin="lower", alpha=1.0, interpolation='none')
#plt.plot(longi, lat, 'ko')
plt.axis([min(longi), max(longi), min(lat), max(lat)])
plt.clim(-110,-35)
#plt.clim(0,1)
plt.colorbar()
plt.show()


#removeAccessPoints(data, ['FP-2', 'FP-6'], ['bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_113', 'bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_112', 'bec26202-a8d8-4a94-b0fc-9ac1de37daa6_10_111'])

#save(data, 'data2.json')















