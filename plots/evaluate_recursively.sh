#!/bin/bash
# Make plots for each dataset, which has a ground truth file, in a leaf-directory under .
# Normal usage would be to execute this in the "logs" directory. No parameters, but:
# Set the path to the python script here:
EVALUATE_POSITIONS=~/evaluatePositions.py

for leaf_dir in `find . -type d -links 2`
do
    leaf_dir=`basename $leaf_dir`
    pushd $leaf_dir

    # Maybe we were started in one specific logs subdirectory:
    if [ "$leaf_dir" == "." ]
    then
        leaf_dir=${PWD##*/}
    fi

    if [ -f groundTruthLongLat.txt ]
    then
        if [ -w . ]
        then
            img=plot-$leaf_dir
            mf=mean-$leaf_dir.txt
        else
            img=/tmp/plot-$leaf_dir
            mf=/tmp/mean-$leaf_dir.txt
            echo "We don't have write permissions here. Output files will go to /tmp."
        fi

        scale=`grep scale ../../data/*.osm | perl -pe "s#.*<tag k='indoor_scale' v='(.*?)' />#\1#"`
        $EVALUATE_POSITIONS --gt groundTruthLongLat.txt --pos positionsLongLat.txt --img $img --meanFile $mf --scale $scale
    else
        echo "...skipped, because of missing groundTruthLongLat.txt (probably not a log from the admin app)"
    fi
    
    popd > /dev/null
    echo
done
