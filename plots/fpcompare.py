import matplotlib.pyplot as plt
import numpy as np
from math import *

data = np.loadtxt("fp2ogbtle.txt")
data2 = np.loadtxt("2ogbtle.txt")

plt.xlabel('Longitude')
plt.ylabel('Latitude')
font = {'size'   : 16}
plt.rc('font', **font)

for i in range(0,len(data)):
	plt.plot([data[i,1], data2[i,1]],[data[i,0], data2[i,0]], color='g')

plt.plot(data[:,1],data[:,0], 'bo')
plt.plot(data2[:,1],data2[:,0], 'ro')

plt.show()
