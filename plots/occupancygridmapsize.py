from math import *

#calculate ogm size

minlat = 50.72002481098940
minlong = 7.05722200000000
maxlat = 50.72419000000000
maxlong = 7.06482721704576

scale = 55
resolution = 0.5#m

# calculate (x,y) position for (lat,long)
latitudeLineDistance = 111325
earthShapeConstants = [111412.84, -93.5, 0.118]


minlatrad = minlat * pi/180
maxlatrad = maxlat * pi/180

minlim = earthShapeConstants[0] * cos(minlatrad) + earthShapeConstants[1] * cos(3*minlatrad) + earthShapeConstants[2] * cos(5*minlatrad)
maxlim = earthShapeConstants[0] * cos(maxlatrad) + earthShapeConstants[1] * cos(3*maxlatrad) + earthShapeConstants[2] * cos(5*maxlatrad)

minx = minlong * minlim
maxx = maxlong * maxlim

miny = minlat * latitudeLineDistance
maxy = maxlat * latitudeLineDistance

#calculate size of occupancy gridmap
diffx = abs(minx - maxx)
diffy = abs(miny - maxy)

cellsize = resolution * scale

dimx = ceil(diffx/cellsize)
dimy = ceil(diffy/cellsize)

size = int(max(dimx, dimy))

print "set the lower size of the image to {}".format(size)

    
    
    
