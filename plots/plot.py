import matplotlib.pyplot as plt
import numpy as np
from math import *

def lowpassfilter(oldvalue, weight, data):
	newvalue = oldvalue * (1-weight) + data * weight
	return newvalue
	
	
def compassfilter(oldvalue, weight, data):
	newx = cos(oldvalue) * (1-weight) + cos(data) * weight
	newy = sin(oldvalue) * (1-weight) + sin(data) * weight
	newvalue = atan2(newy, newx)
	if (newvalue < 0):
		newvalue += 2*pi
	return newvalue


def plotAllAxes():
	# get right sensor data
	#data = np.loadtxt("Sensor Daten/AK8975_Magnetic_field_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	#data = np.loadtxt("Sensor Daten/K3DH_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	data = np.loadtxt("Sensor Daten/Linear_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")

	# plot style settings
	plt.xlabel('time [ns]')
	#plt.ylabel('['+u'\xb5T]')
	plt.ylabel('[m/s^2]')
	plt.rc('lines', linewidth=1)
	font = {'size'   : 16}
	plt.rc('font', **font)
	
	# plot raw data
	plt.plot(data[:,0],data[:,2], 'r--')
	plt.plot(data[:,0],data[:,3], 'g--')
	plt.plot(data[:,0],data[:,4], 'b--')

	weights = [0, 1, 0.5, 0.3, 0.2, 0.1, 0.05, 0.02]

	# filter data
	xVal=[]
	yVal=[]
	zVal=[]
	xVal.append(0)
	yVal.append(0)
	zVal.append(0)

	for i in range(0,len(data)):
		xVal.append(lowpassfilter(xVal[i], weights[3], data[i][2]))
		yVal.append(lowpassfilter(yVal[i], weights[3], data[i][3]))
		zVal.append(lowpassfilter(zVal[i], weights[3], data[i][4]))

	del xVal[0]
	del yVal[0]
	del zVal[0]

	# plot filtered data
	plt.rc('lines', linewidth=2)
	plt.plot(data[:,0], xVal[:], 'r-')
	plt.plot(data[:,0], yVal[:], 'g-')
	plt.plot(data[:,0], zVal[:], 'b-')

	plt.show()



def plotVectors():
	# get right sensor data
	#data = np.loadtxt("Sensor Daten/AK8975_Magnetic_field_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	#data = np.loadtxt("Sensor Daten/K3DH_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	data = np.loadtxt("Sensor Daten/Linear_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")

	# plot style settings
	plt.xlabel('time [ns]')
	#plt.ylabel('['+u'\xb5T]')
	plt.ylabel('[m/s^2]')
	plt.rc('lines', linewidth=1)
	font = {'size'   : 16}
	plt.rc('font', **font)

	#calculate vector length
	newvec=[]
	for i in range(len(data)):
		length = sqrt(data[i][2]*data[i][2] + data[i][3]*data[i][3] + data[i][4]*data[i][4])
		newvec.append(length)

	# plot raw vector length
	plt.plot(data[:,0], newvec, 'r--')

	weights = [0, 1, 0.5, 0.3, 0.2, 0.1, 0.05, 0.02]

	# filter sensor data
	xVal=[]
	yVal=[]
	zVal=[]
	xVal.append(0)
	yVal.append(0)
	zVal.append(0)

	for i in range(0,len(data)):
		xVal.append(lowpassfilter(xVal[i], weights[3], data[i][2]))
		yVal.append(lowpassfilter(yVal[i], weights[3], data[i][3]))
		zVal.append(lowpassfilter(zVal[i], weights[3], data[i][4]))

	del xVal[0]
	del yVal[0]
	del zVal[0]

	#calculate vector length
	newfiltvec=[]
	for i in range(len(xVal)):
		length = sqrt(xVal[i]*xVal[i] + yVal[i]*yVal[i] + zVal[i]*zVal[i])
		newfiltvec.append(length)

	# plot filtered vector length
	plt.rc('lines', linewidth=2)
	plt.plot(data[:,0], newfiltvec, 'b-')

	plt.show()
	
	

def plotAzimuth():
	# get right data
	magndata = np.loadtxt("Sensor Daten/AK8975_Magnetic_field_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	accdata = np.loadtxt("Sensor Daten/K3DH_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")

	# plot style settings
	plt.xlabel('value number')
	plt.ylabel('angle [rad]')
	plt.rc('lines', linewidth=1)
	font = {'size'   : 16}
	plt.rc('font', **font)

	azimuth = calculateAzimuth(accdata, magndata)
	# plot raw azimuth
	plt.rc('lines', linewidth=1)
	plt.plot(azimuth, 'r--', label='raw')

	weights = [0, 1, 0.5, 0.3, 0.2, 0.1, 0.05, 0.02]

	#filter azimuth
	filazimuth=[]
	filazimuth.append(0)
	for i in range(0,len(azimuth)):
		filazimuth.append(compassfilter(filazimuth[i], weights[6], azimuth[i]))

	del filazimuth[0]
	
	plt.rc('lines', linewidth=2)
	plt.plot(filazimuth, 'b-', label='0.05')
	
	filazimuth2=[]
	filazimuth2.append(0)
	for i in range(0,len(azimuth)):
		filazimuth2.append(compassfilter(filazimuth2[i], weights[4], azimuth[i]))

	del filazimuth2[0]
	
	plt.rc('lines', linewidth=2)
	plt.plot(filazimuth2, 'g-', label='0.2')
	
	filazimuth3=[]
	filazimuth3.append(0)
	for i in range(0,len(azimuth)):
		filazimuth3.append(compassfilter(filazimuth3[i], weights[2], azimuth[i]))

	del filazimuth3[0]
	
	plt.rc('lines', linewidth=2)
	plt.plot(filazimuth3, 'm-', label='0.5')
	
	plt.legend()
	plt.show()

def calculateAzimuth(accdata, magndata):

	azimuth=[]
	# calculate azimuth
	for i in range(len(magndata)):
		
		#getRotationMatrix android algorithm
		Ax = accdata[i][2]
		Ay = accdata[i][3]	
		Az = accdata[i][4]

		Ex = magndata[i][2]
		Ey = magndata[i][3]	
		Ez = magndata[i][4]

		Hx = Ey*Az - Ez*Ay
		Hy = Ez*Ax - Ex*Az
		Hz = Ex*Ay - Ey*Ax
		
		normH = sqrt(Hx*Hx + Hy*Hy + Hz*Hz)
		if (normH < 0.1):
			print "BOOJA KAPUTT!"
			
		invH = 1/normH
		Hx *= invH
		Hy *= invH
		Hz *= invH
		
		invA = 1/sqrt(Ax*Ax + Ay*Ay + Az*Az)
		Ax *= invA
		Ay *= invA
		Az *= invA
		
		Mx = Ay*Hz - Az*Hy
		My = Az*Hx - Ax*Hz
		Mz = Ax*Hy - Ay*Hx
		
		#getOrientation android algorithm
		azimuthval = atan2(Hy, My)
		# change to deg and 0-359?
		azimuth.append(azimuthval)

	return azimuth
		
def fieseFunktion():
	# get right data
	magndata = np.loadtxt("Sensor Daten/AK8975_Magnetic_field_Sensor_27_Mar_2014_11-35-03_GMT.txt")
	accdata = np.loadtxt("Sensor Daten/K3DH_Acceleration_Sensor_27_Mar_2014_11-35-03_GMT.txt")

	# plot style settings
	plt.xlabel('value number')
	plt.ylabel('angle [rad]')
	plt.rc('lines', linewidth=1)
	font = {'size'   : 16}
	plt.rc('font', **font)

	azimuth = calculateAzimuth(accdata, magndata)
	# plot raw azimuth
	plt.rc('lines', linewidth=1)
	plt.plot(azimuth, 'r--', label='raw')

	weights = [0, 1, 0.5, 0.3, 0.2, 0.1, 0.07, 0.05, 0.02]
	
	#filter azimuth
	filazimuth=[]
	filazimuth.append(0)
	for i in range(0,len(azimuth)):
		filazimuth.append(lowpassfilter(filazimuth[i], weights[7], azimuth[i]))

	del filazimuth[0]
	
	plt.rc('lines', linewidth=2)
	plt.plot(filazimuth, 'b-', label='filter azimuth (0.05)')
	
	# filter sensor data and then calculate azimuth!
	# filter data
	# magnetometer
	xVal=[]
	yVal=[]
	zVal=[]
	xVal.append(0)
	yVal.append(0)
	zVal.append(0)

	for i in range(0,len(magndata)):
		xVal.append(lowpassfilter(xVal[i], weights[4], magndata[i][2]))
		yVal.append(lowpassfilter(yVal[i], weights[4], magndata[i][3]))
		zVal.append(lowpassfilter(zVal[i], weights[4], magndata[i][4]))

	del xVal[0]
	del yVal[0]
	del zVal[0]
	
	xVal2=[]
	yVal2=[]
	zVal2=[]
	xVal2.append(0)
	yVal2.append(0)
	zVal2.append(0)

	# accelerometer
	for i in range(0,len(accdata)):
		xVal2.append(lowpassfilter(xVal2[i], weights[3], accdata[i][2]))
		yVal2.append(lowpassfilter(yVal2[i], weights[3], accdata[i][3]))
		zVal2.append(lowpassfilter(zVal2[i], weights[3], accdata[i][4]))

	del xVal2[0]
	del yVal2[0]
	del zVal2[0]
	
	azimuthfiltered=[]
	# calculate azimuth
	for i in range(len(magndata)):
		
		#getRotationMatrix android algorithm
		Ax = xVal2[i]
		Ay = yVal2[i]	
		Az = zVal2[i]

		Ex = xVal[i]
		Ey = yVal[i]	
		Ez = zVal[i]

		Hx = Ey*Az - Ez*Ay
		Hy = Ez*Ax - Ex*Az
		Hz = Ex*Ay - Ey*Ax
		
		normH = sqrt(Hx*Hx + Hy*Hy + Hz*Hz)
		if (normH < 0.1):
			print "BOOJA KAPUTT!"
			
		invH = 1/normH
		Hx *= invH
		Hy *= invH
		Hz *= invH
		
		invA = 1/sqrt(Ax*Ax + Ay*Ay + Az*Az)
		Ax *= invA
		Ay *= invA
		Az *= invA
		
		Mx = Ay*Hz - Az*Hy
		My = Az*Hx - Ax*Hz
		Mz = Ax*Hy - Ay*Hx
		
		#getOrientation android algorithm
		azimuthval = atan2(Hy, My)
		# change to deg and 0-359?
		azimuthfiltered.append(azimuthval)
		
	plt.plot(azimuthfiltered, 'c-', label='filter raw (m=0.02, a=0.2)')
	
	plt.legend()
	plt.show()


if __name__ == "__main__":
	# plot all axes of raw and filtered sensor data
#	plotAllAxes()0,2345

	# plot the vector lengths of raw and filtered sensor data
#	plotVectors() 

	# calculate the azimuth out of the sensor data and filter it
#	plotAzimuth()

	# compare azimuth with pre-filtered sensor data and filtered azimuth
#	fieseFunktion()

#	print atan2(-0.13825, 0.83937)


# To not forget how some things work...

#plt.errorbar(xValues, mean, yerr=cov, marker='.', linestyle='-', ecolor='r', capsize=10, markeredgewidth=3)
#plt.yscale('log')
#plt.xticks(xValues,xValues, rotation='45')
#plt.legend(numpoints = 1)
