import matplotlib.pyplot as plt
import numpy as np
from math import *

'''data2 = np.loadtxt("azimuth.txt")
data = np.loadtxt("filteredazimuth.txt")

plt.xlabel('timesteps')
plt.ylabel('azimuth')
font = {'size'   : 16}
plt.rc('font', **font)


plt.plot(data, '-r')
plt.plot(data2, '--b')'''

'''plt.plot(data[0,0],data[0,1], 'ro', markersize = 16)
plt.plot(data[3,0],data[3,1], 'ro', markersize = 16)

plt.plot(data[5,0],data[5,1], 'bo', markersize = 16)
plt.plot(data[6,0],data[6,1], 'bo', markersize = 16)
plt.plot(data[7,0],data[7,1], 'bo', markersize = 16)

plt.plot(data[8,0],data[8,1], 'go', markersize = 20)'''


#plt.show()


data = np.loadtxt("fp2ogbtle.txt")
data2 = np.loadtxt("2ogbtle.txt")

plt.xlabel('Latitude')
plt.ylabel('Longitude')
font = {'size'   : 16}
plt.rc('font', **font)

for i in range(0,len(data)):
	plt.plot([data[i,0], data2[i,0]],[data[i,1], data2[i,1]])

plt.plot(data[:,0],data[:,1], 'bo')
plt.plot(data2[:,0],data2[:,1], 'ro')

plt.show()
