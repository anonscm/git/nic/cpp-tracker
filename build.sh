#!/bin/bash
mkdir -p build
cd build
set -e
cmake -DUSE_GDB=true -DBUILD_STANDALONE=true -DBUILD_TEST=false -DBUILD_JNI=false -G "Unix Makefiles" ../
make install
cd -
