#!/bin/bash
ndk_prebuilt_make=$ANDROID_NDK/prebuilt/windows-x86_64/bin/make.exe

sh build-android-common.sh $ndk_prebuilt_make armeabi
sh build-android-common.sh $ndk_prebuilt_make armeabi-v7a
sh build-android-common.sh $ndk_prebuilt_make x86

cd jni/src/main/jni
ndk-build
cd -

cd jni/android
./gradlew install 
cd -