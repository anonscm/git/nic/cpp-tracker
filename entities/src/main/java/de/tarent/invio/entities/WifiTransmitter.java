package de.tarent.invio.entities;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * A physical Wifi-AccessPoint (AP), which can contain any number of Virtual-Access-Points.
 */
public class WifiTransmitter extends Transmitter {

    @SerializedName("name")
    public String name;

    @SerializedName("networks")
    public List<NetworkNode> networks = new ArrayList<>();


    public WifiTransmitter() {
    }


    public WifiTransmitter(double lat, double lon, double height, String name, String vendor) {
        this.lat = lat;
        this.lon = lon;
        this.height = height;
        this.name = name;
        this.vendor = vendor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WifiTransmitter that = (WifiTransmitter) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return !(networks != null ? !networks.equals(that.networks) : that.networks != null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (networks != null ? networks.hashCode() : 0);
        return result;
    }
}
