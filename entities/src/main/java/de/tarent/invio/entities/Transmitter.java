package de.tarent.invio.entities;

import com.google.gson.annotations.SerializedName;


/**
 * A Transmitter is anything that transmits signals which can be measured by the handset/invio-client, i.e. a Wifi-AP
 * or a bluetooth-beacon.
 */
public class Transmitter {

    @SerializedName("lat")
    public double lat;
    @SerializedName("lon")
    public double lon;

    @SerializedName("height")
    public double height;

    @SerializedName("vendor")
    public String vendor;

}
