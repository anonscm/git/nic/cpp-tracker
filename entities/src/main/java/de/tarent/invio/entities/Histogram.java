package de.tarent.invio.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The Histogram stores the distribution of wifi signal strength levels (over multiple measurements) for a set of
 * access points which are identified by their BSSID.
 * It is as Map<String bssid, Map<Integer dB, Float fraction>> with a name (id).
 * The keySet of this map is the set of all encountered BSSIDs.
 * The values are a map of signal strength level distributions.
 */
public class Histogram extends HashMap<String, Map<Integer, Float>> {

    private String id;


    /**
     * Construct a new, empty and nameless Histogram
     */
    public Histogram() {
        super();
    }

    /**
     * Construct a new, empty Histogram with an ID.
     *
     * @param id the ID (name) of this Histogram.
     */
    public Histogram(final String id) {
        super();
        this.id = id;
    }

    /**
     * Constructor that copies a given Histogram to create a new one.
     *
     * @param histogram the Histogram to be copied
     */
    public Histogram(final Histogram histogram) {
        this.id = histogram.id;
        this.putAll(histogram);
    }

    /**
     * Returns a {@link Set} of keys that exist in both given histograms.
     *
     * @param histogram1 the first histogram
     * @param histogram2 the second histogram
     * @return the {@link Set} of common keys
     */
    public static Set<String> getCommonKeys(final Histogram histogram1, final Histogram histogram2) {
        final Set<String> result = new HashSet<String>();
        result.addAll(histogram1.keySet());
        result.retainAll(histogram2.keySet());

        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Histogram histogram = (Histogram) o;

        return !(id != null ? !id.equals(histogram.id) : histogram.id != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
