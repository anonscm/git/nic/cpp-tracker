package de.tarent.invio.entities;

import com.google.gson.annotations.SerializedName;


/**
 * A NetworkNode represents one specific Wifi-network on one specific AP.
 */
public class NetworkNode {

    @SerializedName("macAddress")
    public String macAddress; // = BSSID

    @SerializedName("ssid")
    public String ssid; // = the name of the network.

    @SerializedName("channelNumber")
    public int channelNumber;

    @SerializedName("band")
    public double frequencyBand;


    public NetworkNode(String macAddress, String ssid, int channelNumber, double frequencyBand) {
        this.macAddress = macAddress;
        this.ssid = ssid;
        this.channelNumber = channelNumber;
        this.frequencyBand = frequencyBand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkNode that = (NetworkNode) o;

        if (channelNumber != that.channelNumber) return false;
        if (Double.compare(that.frequencyBand, frequencyBand) != 0) return false;
        if (macAddress != null ? !macAddress.equals(that.macAddress) : that.macAddress != null) return false;
        return !(ssid != null ? !ssid.equals(that.ssid) : that.ssid != null);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = macAddress != null ? macAddress.hashCode() : 0;
        result = 31 * result + (ssid != null ? ssid.hashCode() : 0);
        result = 31 * result + channelNumber;
        temp = Double.doubleToLongBits(frequencyBand);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
