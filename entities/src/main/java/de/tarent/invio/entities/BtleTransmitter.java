package de.tarent.invio.entities;

import com.google.gson.annotations.SerializedName;


/**
 * A Bluetooth-Beacon.
 */
public class BtleTransmitter extends Transmitter {

    @SerializedName("majorId")
    public String majorId;

    @SerializedName("minorId")
    public String minorId;

    @SerializedName("powerLevel")
    public String powerLevel;

    @SerializedName(("sendInterval"))
    public int sendInterval;


    public BtleTransmitter() {
    }


    public BtleTransmitter(double lat, double lon, double height, String majorId, String minorId, String powerLevel, String vendor, int sendInterval) {
        this.lat = lat;
        this.lon = lon;
        this.height = height;
        this.majorId = majorId;
        this.minorId = minorId;
        this.powerLevel = powerLevel;
        this.vendor = vendor;
        this.sendInterval = sendInterval;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BtleTransmitter that = (BtleTransmitter) o;

        if (sendInterval != that.sendInterval) return false;
        if (majorId != null ? !majorId.equals(that.majorId) : that.majorId != null) return false;
        if (minorId != null ? !minorId.equals(that.minorId) : that.minorId != null) return false;
        return !(powerLevel != null ? !powerLevel.equals(that.powerLevel) : that.powerLevel != null);

    }

    @Override
    public int hashCode() {
        int result = majorId != null ? majorId.hashCode() : 0;
        result = 31 * result + (minorId != null ? minorId.hashCode() : 0);
        result = 31 * result + (powerLevel != null ? powerLevel.hashCode() : 0);
        result = 31 * result + sendInterval;
        return result;
    }
}
