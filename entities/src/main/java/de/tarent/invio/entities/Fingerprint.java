package de.tarent.invio.entities;


/**
 * A Fingerprint is a wifi Histogram at a specific position.
 */
public class Fingerprint {

    /**
     * The histogram that was measured at some point.
     */
    private Histogram histogram;

    /**
     * The point where the histogram was measured.
     */
    private InvioGeoPoint point;

    /**
     * An ID, for display, ordering, etc.
     */
    private String id;

    /**
     * The number of scans made for a wifi fingerprint.
     */
    private int scanCount = 0;

    /**
     * The time scanned (in seconds) for bluetooth fingerprint.
     */
    private int scanTime = 0;

    /**
     * Constructor.<p/>
     * <b>When uploading wifi fingerprints, use Fingerprint(Histogram, InvioGeoPoint, int)!!!</b>
     *
     * @param histogram the {@link Histogram}
     * @param geoPoint  the {@link InvioGeoPoint}
     */
    public Fingerprint(final Histogram histogram, final InvioGeoPoint geoPoint) {
        this.histogram = histogram;
        this.point = geoPoint;
        this.id = histogram.getId();
    }

    /**
     * Constructor.
     *
     * @param histogram the {@link Histogram}
     * @param geoPoint the {@link InvioGeoPoint}
     * @param scanCount the number of scans as an integer
     * @param scanTime the time scanned as an Integer
     */
    public Fingerprint(final Histogram histogram, final InvioGeoPoint geoPoint,
                       final int scanCount, final int scanTime) {
        this(histogram, geoPoint);
        this.scanCount = scanCount;
        this.scanTime = scanTime;
    }

    public Histogram getHistogram() {
        return histogram;
    }

    public InvioGeoPoint getPoint() {
        return point;
    }

    public String getId() {
        return id;
    }

    public int getScanCount() {
        return scanCount;
    }

    public void setScanCount(int count) {
        scanCount = count;
    }

    public int getScanTime() {
        return scanTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Fingerprint that = (Fingerprint) o;

        if (histogram != null ? !histogram.equals(that.histogram) : that.histogram != null) return false;
        if (point != null ? !point.equals(that.point) : that.point != null) return false;
        return !(id != null ? !id.equals(that.id) : that.id != null);
    }

    @Override
    public int hashCode() {
        int result = histogram != null ? histogram.hashCode() : 0;
        result = 31 * result + (point != null ? point.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
