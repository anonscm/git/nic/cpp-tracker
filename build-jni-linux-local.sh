#!/bin/bash
# make tracker library with jni support
# This script, in combination with the CMakeLists.txt, builds the tracker
# using a locally installed boost library. For example if you have a 32 bit system
# and cannot use the boost version we have in this repo.
# It then builds the jni-linux stuff and installs the resulting jar.
os=`uname`
con="-"
arch=`uname -m`
dir=$os$con$arch

mkdir -p build-jni/$dir
cd build-jni/$dir
set -e
cmake -DBUILD_STANDALONE=true -DBUILD_TEST=false -DBUILD_JNI_LOCAL=true -G "Unix Makefiles" ../../
make install
cd -

#copy the library to the current arch resource folder
mkdir -p jni/linux/resources/lib/$dir
cp build-jni/$dir/dist/jni/libtracker-jni.so jni/linux/resources/lib/$dir

cd jni/linux
./gradlew install
