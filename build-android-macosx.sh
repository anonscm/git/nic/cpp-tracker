#!/bin/bash

# build the cpp-tracker and jni stuff on a mac
# NB: as of 2015-07-27, android-ndk r9 is required, r10e does not work!

ndk_prebuilt_make=$ANDROID_NDK/prebuilt/darwin-x86_64/bin/make

chmod +x build-android-common.sh

case $1 in
"") 	ARCH="armeabi armeabi-v7a x86";;
esac

for arch in $ARCH
do
	./build-android-common.sh $ndk_prebuilt_make $arch || exit 1
done

cd jni/src/main/jni
$ANDROID_NDK/ndk-build
cd -
