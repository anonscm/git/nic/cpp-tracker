#!/bin/bash
ndk_prebuilt_make=$ANDROID_NDK/prebuilt/linux-x86_64/bin/make

chmod +x build-android-common.sh

case $1 in
"") 	ARCH="armeabi armeabi-v7a x86";;
*)      ARCH="$1";;
esac

for arch in $ARCH
do
	./build-android-common.sh $ndk_prebuilt_make $arch || exit 1
done

cd jni/src/main/jni
ndk-build
cd -
