#include "Histogram.h"
#include "gmock/gmock.h"

using namespace std;

TEST(HistogramTest, getCommonKeys) {    
    map<int, float> value;
    
    Histogram histogramOne("one");
    histogramOne.insert("one", value);
    histogramOne.insert("two", value);
    histogramOne.insert("three", value);
    histogramOne.insert("five", value);
    histogramOne.insert("six", value);
    histogramOne.insert("seven", value);
    histogramOne.insert("eight", value);
    histogramOne.insert("nine", value);
    
    
    Histogram histogramTwo("two");
    histogramTwo.insert("two", value);
    histogramTwo.insert("three", value);
    histogramTwo.insert("four", value);
    histogramTwo.insert("ten", value);
    histogramTwo.insert("eleven", value);
    histogramTwo.insert("twelve", value);
    histogramTwo.insert("thirteen", value);
    histogramTwo.insert("fourteen", value);
    
    set<string> firstPreRun = Histogram::getCommonKeys(histogramOne, histogramTwo);
    set<string> secondPreRun = Histogram::getCommonKeys(histogramTwo, histogramOne);
    set<string> thirdPreRun = Histogram::getCommonKeys(histogramOne, histogramTwo);
    set<string> fourthPreRun = Histogram::getCommonKeys(histogramTwo, histogramOne);    
    set<string> commonKeys = Histogram::getCommonKeys(histogramOne, histogramTwo);
            
    //ASSERT_THAT(commonKeys, ElementsAre(make_pair("two", value), make_pair("three", value)));
    bool foundOne = (commonKeys.find("one") != commonKeys.end());
    bool foundTwo = (commonKeys.find("two") != commonKeys.end());
    bool foundThree = (commonKeys.find("three") != commonKeys.end());
    bool foundFour = (commonKeys.find("four") != commonKeys.end());
    bool foundFive = (commonKeys.find("five") != commonKeys.end());
    bool foundSix = (commonKeys.find("six") != commonKeys.end());
    bool foundSeven = (commonKeys.find("seven") != commonKeys.end());
    bool foundEight = (commonKeys.find("eight") != commonKeys.end());
    bool foundNine = (commonKeys.find("nine") != commonKeys.end());
    bool foundTen = (commonKeys.find("ten") != commonKeys.end());
    bool foundEleven = (commonKeys.find("eleven") != commonKeys.end());
    bool foundTwelve = (commonKeys.find("twelve") != commonKeys.end());
    bool foundThirteen = (commonKeys.find("thirteen") != commonKeys.end());
    bool foundFourteen = (commonKeys.find("fourteen") != commonKeys.end());
    
    ASSERT_FALSE(foundOne);
    ASSERT_TRUE(foundTwo);
    ASSERT_TRUE(foundThree);
    ASSERT_FALSE(foundFour);
    ASSERT_FALSE(foundFive);
    ASSERT_FALSE(foundSix);
    ASSERT_FALSE(foundSeven);
    ASSERT_FALSE(foundEight);
    ASSERT_FALSE(foundNine);
    ASSERT_FALSE(foundTen);
    ASSERT_FALSE(foundEleven);
    ASSERT_FALSE(foundTwelve);
    ASSERT_FALSE(foundThirteen);
    ASSERT_FALSE(foundFourteen);
}

TEST(HistogramTest, retainAll) {
    map<int, float> valueOne;
    valueOne.insert(make_pair(1, 1.0f));
    
    Histogram histogram("one");
    histogram.insert("one", valueOne);
    histogram.insert("two", valueOne);
    histogram.insert("three", valueOne);
    histogram.insert("four", valueOne);
    histogram.insert("five", valueOne);
    
    // Because we want to make sure that regardless what the values are, only the
    // map keys are looked at when retaining.
    map<int, float> valueTwo;
    valueTwo.insert(make_pair(2, 2.0f));
    
    // We only want to keep the odd values from the histogram.
    set<string> commonKeys;
    commonKeys.insert("one");
    commonKeys.insert("three");
    commonKeys.insert("five");   
    
    histogram.retainAll(commonKeys);
    
    bool foundOne = (histogram.find("one") != histogram.end());
    bool foundTwo = (histogram.find("two") != histogram.end());
    bool foundThree = (histogram.find("three") != histogram.end());
    bool foundFour = (histogram.find("four") != histogram.end());
    bool foundFive = (histogram.find("five") != histogram.end());
    
    ASSERT_TRUE(foundOne);
    ASSERT_FALSE(foundTwo);
    ASSERT_TRUE(foundThree);
    ASSERT_FALSE(foundFour);
    ASSERT_TRUE(foundFive);
}
