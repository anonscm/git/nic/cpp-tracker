#include "UserLocator.h"
#include "GeoPoint.h"
#include "DeadReckoning.h"
#include "gmock/gmock.h"
#include "FileParser.h"
#include "TrackerConfig.h"
#include <memory>
#include <iostream>

using namespace std;

class DeadReckoningTestImpl : public DeadReckoning {
     public:
        DeadReckoningTestImpl() : DeadReckoning()  { }
         
        GeoPoint getDelta() {
            return GeoPoint::makePoint(2,3);
        }

};

TEST(UserLocatorTest, startStopTracking) {   
    UserLocator userLocator(LOCALISATION_PARTICLEFILTER);
    
    userLocator.startTracking();
    // userLocator.startTracking();

    ASSERT_TRUE(userLocator.getIsTracking());
    
    userLocator.stopTracking();
    
    ASSERT_TRUE(!userLocator.getIsTracking());
}

TEST(UserLocatorTest, testAddHistogram) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    vector<Fingerprint> allFingerprints = FileParser::getFingerprints("res/fingerprints_data_full.json");
    userLocator.setFingerprints(allFingerprints); 
    vector<Fingerprint> fingerprints = FileParser::getFingerprints("res/test_fingerprint.json");
    
    userLocator.setSufficientNumMeasurements(0);
    userLocator.setLocalisationMode(LOCALISATION_WIFI);
   
    // Because there is only one Fingerprint we can get the first one.
    Fingerprint fingerprint = fingerprints[0];
    Histogram histogram = fingerprint.getHistogram();
    
    userLocator.addHistogram(histogram);
    userLocator.localisationIteration();

    ASSERT_NEAR(51265475, userLocator.getCurrentPosition().getLatitudeE6(), 100000);
    ASSERT_NEAR(-21861, userLocator.getCurrentPosition().getLongitudeE6(), 100000);
    ASSERT_EQ(0, userLocator.getCurrentPosition().getDistance());
}

TEST(UserLocatorTest, testAddHistogramWithoutMatch) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    // only one FP with one histogram
    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-10, 10.f));
    tmpHist.insert(make_pair(-40, 10.f));
    p.insert("00:01:02:03:04:05", tmpHist);
    GeoPoint fpPos(2, 9);
    Fingerprint fp(p, fpPos);
    vector<Fingerprint> fingerprints;
    fingerprints.push_back(fp);
    userLocator.setFingerprints(fingerprints); 
    
    userLocator.setSufficientNumMeasurements(0);
    userLocator.setLocalisationMode(LOCALISATION_WIFI);
    
    // histogram doesn't match FPs
    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    q.insert("02:00:00:00:00:01", tmpHist2);
    
    userLocator.addHistogram(q);
    userLocator.localisationIteration();

    // there should be no position = (0,0)
    ASSERT_NEAR(0, userLocator.getCurrentPosition().getLatitudeE6(), 1.f);
    ASSERT_NEAR(0, userLocator.getCurrentPosition().getLongitudeE6(), 1.f);
    ASSERT_EQ(0, userLocator.getCurrentPosition().getDistance());   
}

TEST(UserLocatorTest, testCalculateDistances) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    vector<Fingerprint> allFingerprints = FileParser::getFingerprints("res/fingerprints_data_full.json");
    userLocator.setFingerprints(allFingerprints); 
    vector<Fingerprint> fingerprints = FileParser::getFingerprints("res/test_fingerprint.json");
    
    // Because there is only one Fingerprint we can get the first one.
    Fingerprint fingerprint = fingerprints[0];
    Histogram histogram = fingerprint.getHistogram();
    
    vector<GeoPoint> distanceSet = userLocator.calculateDistances(histogram);

    // we don't care about the values, see QFD tests
    ASSERT_EQ(allFingerprints.size(), distanceSet.size());   
    ASSERT_TRUE(distanceSet[allFingerprints.size()-1].getDistance() > 0);
}

TEST(UserLocatorTest, testCalculateDistancesNoMatch) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    // only one FP with one histogram
    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-10, 10.f));
    tmpHist.insert(make_pair(-40, 10.f));
    p.insert("00:01:02:03:04:05", tmpHist);
    GeoPoint fpPos(2, 9);
    Fingerprint fp(p, fpPos);
    vector<Fingerprint> fingerprints;
    fingerprints.push_back(fp);
    userLocator.setFingerprints(fingerprints); 
    
    // histogram doesn't match FPs
    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    q.insert("02:00:00:00:00:01", tmpHist2);
    
    vector<GeoPoint> distanceSet = userLocator.calculateDistances(q);

    // we don't care about the values, see QFD tests
    ASSERT_EQ(0, distanceSet.size());       
}

TEST(UserLocatorTest, testGetNearestNeighbours) {
    TrackerConfig* config = TrackerConfig::getInstance();
    config->setValue(NUM_NEIGHBOURS, "3");
                        
    UserLocator userLocator(LOCALISATION_WIFI);
    vector<GeoPoint> distanceSet;
    
    distanceSet.push_back(GeoPoint(0, 0, 0.5f));
    distanceSet.push_back(GeoPoint(3, 6, 0.1f));
    distanceSet.push_back(GeoPoint(7, 9, 0.78f));
    distanceSet.push_back(GeoPoint(13, 4, 0.9f));
    
    vector<GeoPoint> neighbours = userLocator.getNearestNeighbours(distanceSet);
    
    ASSERT_EQ(3, neighbours.size());
    
    ASSERT_NEAR(3, neighbours[0].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(6, neighbours[0].getLongitudeE6(), 0.1f);
    ASSERT_EQ(0.1f, neighbours[0].getDistance());
    ASSERT_NEAR(0, neighbours[1].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(0, neighbours[1].getLongitudeE6(), 0.1f);
    ASSERT_EQ(0.5f, neighbours[1].getDistance());
    ASSERT_NEAR(7, neighbours[2].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(9, neighbours[2].getLongitudeE6(), 0.1f);
    ASSERT_EQ(0.78f, neighbours[2].getDistance());
}

TEST(UserLocatorTest, testGetNearestNeighborLessPointsThanNeighbours) {
    UserLocator userLocator(LOCALISATION_WIFI);
    vector<GeoPoint> distanceSet;
    
    distanceSet.push_back(GeoPoint(0, 0, 0.5f));
    distanceSet.push_back(GeoPoint(3, 6, 0.1f));
    
    vector<GeoPoint> neighbours = userLocator.getNearestNeighbours(distanceSet);
    
    ASSERT_EQ(2, distanceSet.size());
    ASSERT_EQ(2, neighbours.size());
    
    ASSERT_NEAR(3, neighbours[0].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(6, neighbours[0].getLongitudeE6(), 0.1f);
    ASSERT_EQ(0.1f, neighbours[0].getDistance());
    ASSERT_NEAR(0, neighbours[1].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(0, neighbours[1].getLongitudeE6(), 0.1f);
    ASSERT_EQ(0.5f, neighbours[1].getDistance());
}

TEST(UserLocatorTest, testRemoveOutliers) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    vector<GeoPoint> nearestNeighbours;
    nearestNeighbours.push_back(GeoPoint(0, 0));
    nearestNeighbours.push_back(GeoPoint(1, 1));
    nearestNeighbours.push_back(GeoPoint(500, 500));
        
    vector<GeoPoint> withoutoutliers = userLocator.removeOutliers(nearestNeighbours);
    
    ASSERT_EQ(3, nearestNeighbours.size());
    // ASSERT_EQ(2, withoutoutliers.size());
    
    ASSERT_NEAR(0, withoutoutliers[0].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(0, withoutoutliers[0].getLongitudeE6(), 0.1f);
    ASSERT_NEAR(1, withoutoutliers[1].getLatitudeE6(), 0.1f);
    ASSERT_NEAR(1, withoutoutliers[1].getLongitudeE6(), 0.1f);
    
}

TEST(UserLocatorTest, testInterpolatePosition) {
    UserLocator userLocator(LOCALISATION_WIFI);
    
    vector<GeoPoint> points;
    GeoPoint tmpPoint = GeoPoint::makePoint(0,0);
    tmpPoint.setDistance(0.9f);
    points.push_back(tmpPoint);
    
    tmpPoint = GeoPoint::makePoint(3,8);
    tmpPoint.setDistance(0.7f);
    points.push_back(tmpPoint);
    
    tmpPoint = GeoPoint::makePoint(12,2);
    tmpPoint.setDistance(0.41f);
    points.push_back(tmpPoint);
    
    GeoPoint position = userLocator.interpolatePosition(points);

    ASSERT_NEAR(9.7f, position.getX(), 0.2f);
    ASSERT_NEAR(2.8f, position.getY(), 0.2f);
    
    tmpPoint = GeoPoint::makePoint(23,4);
    tmpPoint.setDistance(0.39f);
    points.push_back(tmpPoint);
    
    position = userLocator.interpolatePosition(points);
    
    // takes the position of the point if it has a distance less than 0.4f
    ASSERT_NEAR(23.f, position.getX(), 0.2f);
    ASSERT_NEAR(4.f, position.getY(), 0.2f);
}

TEST(UserLocatorTest, testFilterPosition) {
     UserLocator userLocator(LOCALISATION_WIFI);
     userLocator.setMovementThreshold(30.f);
     
     GeoPoint position;
     userLocator.getNextWifiPosition().setXY(2, 2);
     position.setXY(3.f, 4.f);
     userLocator.filterPosition(position);
     
     ASSERT_NEAR(3.f, userLocator.getNextWifiPosition().getX(), 0.2f);
     ASSERT_NEAR(4.f, userLocator.getNextWifiPosition().getY(), 0.2f);
     
     position.setXY(30, 40);
     userLocator.filterPosition(position);
     
     ASSERT_NEAR(16.5f, userLocator.getNextWifiPosition().getX(), 0.2f);
     ASSERT_NEAR(22.f, userLocator.getNextWifiPosition().getY(), 0.2f);
    
}

TEST(UserLocatorTest, testInitCenter) {
    UserLocator userLocator(LOCALISATION_WIFI);
    GeoPoint min(0,0);
    GeoPoint max(1000,1000);
    userLocator.setBoundingBox(min, max);
    
    userLocator.initCenter();
    
    ASSERT_NEAR(500, userLocator.getCurrentPosition().getLatitudeE6(), 1.f);
    ASSERT_NEAR(500, userLocator.getCurrentPosition().getLongitudeE6(), 1.f);    
}

TEST(UserLocatorTest, testTrackingWithDeadReckoning) {
    DeadReckoningTestImpl* deadReckoning = new DeadReckoningTestImpl();
    UserLocator userLocator(LOCALISATION_DEADRECKONING);
    userLocator.setLocalisationMode(LOCALISATION_DEADRECKONING); // nooooo
    userLocator.setScale(1000.f);
    userLocator.setDeadReckoning(deadReckoning);

    GeoPoint min, max;
    min.setXY(0,0);
    max.setXY(2,2);
    userLocator.setBoundingBox(min, max);
    
    userLocator.trackWithoutParticleFilter();
    
    ASSERT_NEAR(3, userLocator.getCurrentPosition().getX(), 0.3f);
    ASSERT_NEAR(4, userLocator.getCurrentPosition().getY(), 0.3f);  
}

class ObstacleCorrectionTest : public ::testing::Test {
protected:
    /**
     * Utility function that accepts a string to initiliaze a oppucancy grid map
     *
     * # or X = wall
     * Every other character = free space
     */
    void loadFromString(const char* chars, int width, int height) {
        std::vector<float> pixelValues;
        pixelValues.reserve(width * height);

        std::string str(chars);
        for(char& c : str) {
            switch(c){
            case 'X':
            case '#':
                pixelValues.push_back(0.0f);
                break;
            default:
                pixelValues.push_back(1.0f);
                break;
            }
        }

        gridMap = std::shared_ptr<OccupancyGridMap>(new OccupancyGridMap(pixelValues, width, height));
        GeoPoint min, max;
        min.setXY(0, 0);
        max.setXY(width * 10, height * 10);
        gridMap->setBoundingBox(min, max);

        userLocator = std::shared_ptr<UserLocator>(new UserLocator(LOCALISATION_WIFI));
        userLocator->setGridMap(gridMap.get());
    }

    void setTestUserPosition(int x, int y){
        V2i pOldLatlon = gridMap->getRealPosition(V2i(x, y));
        GeoPoint pOld(pOldLatlon.latitude, pOldLatlon.longitude);
        userLocator->setCurrentPosition(pOld, true);
    }

    void setUserPosition(int x, int y){
        V2i pNewLatlon = gridMap->getRealPosition(V2i(x, y));
        GeoPoint pNew(pNewLatlon.latitude, pNewLatlon.longitude);
        userLocator->setCurrentPosition(pNew);
    }

    void EXPECT_POSITION(int x, int y){
        GeoPoint pCorrected = userLocator->getCurrentPosition();
        V2i pixelCorrected = gridMap->getGridPosition(V2i(pCorrected.getLatitudeE6(), pCorrected.getLongitudeE6()));
        ASSERT_EQ(x, pixelCorrected.x);
        ASSERT_EQ(y, pixelCorrected.y);
    }

    std::shared_ptr<OccupancyGridMap> gridMap;
    std::shared_ptr<UserLocator> userLocator;
};
/*
    Obstacle Free

    Pold -> O
    Pnew -> N
    Pcor -> @


  O           N=@
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionFree) {
    loadFromString("          "
                   "          "
                   "          "
                   "          "
                   "          "
                   "          "
                   "          "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(6, 3);
    EXPECT_POSITION(6, 3);
}

/*
    Obstacle Type1

    Pold -> O
    Pnew -> N
    Pcor -> @

        ++
        ++
        ++
     O  ++  N=@
        ++
        ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionBasicWallWithPositionInFreeSpace) {
    loadFromString("          "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(6, 3);
    EXPECT_POSITION(6, 3);
}

/*
    Obstacle Type1

    Pold -> O
    Pnew -> N
    Pcor -> @

        ++
        ++
        ++
     0 @N+
        ++
        ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionBasicWallWithPositionInWall) {
    loadFromString("          "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(3, 3);
    EXPECT_POSITION(2, 3);
}

/*
    Obstacle Type1

    Pold -> O
    Pnew -> N
    Pcor -> @

1)
        ++
        ++
        ++
  O    @+N
        ++
        ++

2)
        ++
        ++
       O++
       @++
        ++
        N+


3)
        ++
        ++
        ++
     O=@++
        +N
        ++


4)
        ++
        +N
        ++
     O=@++
        ++
        ++


5)
        ++
        ++       N=@
        ++
       O++
        ++
        ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionBasicWallMultipleCalls) {
    loadFromString("          "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(3, 3);
    EXPECT_POSITION(2, 3);

    setUserPosition(3, 6);
    EXPECT_POSITION(2, 3);

    setUserPosition(4, 5);
    EXPECT_POSITION(2, 3);

    setUserPosition(4, 2);
    EXPECT_POSITION(2, 3);

    setUserPosition(6, 3);
    EXPECT_POSITION(6, 3);
}

/*
    Obstacle Type2

    Pold -> O
    Pnew -> N
    Pcor -> @

        ++++++
        ++++N+
        ++
       @++
        ++
     O  ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionCornerWallWithPositionInWall) {
    loadFromString("          "
                   "   ###### "
                   "   ###### "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(1, 6);
    setUserPosition(8, 2);
    EXPECT_POSITION(2, 5);
}

/*
    Obstacle Type3

    Pold -> O
    Pnew -> N
    Pcor -> @

        ++  ++
        ++  ++
        ++  ++
    O  @+N  ++
        ++  ++
        ++  ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionDoubleWallPositionInWall) {
    loadFromString("          "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(4, 3);
    EXPECT_POSITION(2, 3);
}

/*
    Obstacle Type3

    Pold -> O
    Pnew -> N
    Pcor -> @

        ++  ++
        ++  ++
        ++  ++
    O  @++  +N
        ++  ++
        ++  ++
*/
TEST_F(ObstacleCorrectionTest, testObstacleCorrectionDoubleWallPositionInSecondWall) {
    loadFromString("          "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "   ##  ## "
                   "          ", 10, 8);

    setTestUserPosition(1, 3);
    setUserPosition(8, 3);
    EXPECT_POSITION(2, 3);
}

TEST_F(ObstacleCorrectionTest, testObstacleCorrectionHoleWallWithPositionInHole) {
    loadFromString("P         "
                   "   ###    "
                   "   #E#    "
                   "   ###    "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(0, 0);
    setUserPosition(4, 2);
    EXPECT_POSITION(4, 2);
}

TEST_F(ObstacleCorrectionTest, testObstacleCorrectionHoleWallWithPositionInWall) {
    loadFromString("          "
                   "   ###    "
                   "   # #    "
                   "   ###    "
                   "   ##     "
                   "   ##     "
                   "   ##     "
                   "          ", 10, 8);

    setTestUserPosition(0, 2);
    setUserPosition(5, 2);
    EXPECT_POSITION(2, 2);

    setTestUserPosition(8, 2);
    setUserPosition(5, 2);
    EXPECT_POSITION(6, 2);
}

TEST_F(ObstacleCorrectionTest, testObstacleNew) {
    loadFromString("          "
                   "   ###    "
                   "   # #    "
                   "   ###    "
                   "   ##     "
                   "   ##   ##"
                   "   ##   ##"
                   "        ##", 10, 8);

    setTestUserPosition(0, 0);
    setUserPosition(9, 7);
    EXPECT_POSITION(2, 1);
}

TEST_F(ObstacleCorrectionTest, testObstacleCorrectionUnreachable) {
    loadFromString(" #########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "######### ", 10, 8);

    setTestUserPosition(0, 0);
    setUserPosition(9, 7);
    EXPECT_POSITION(9, 7);
}

TEST_F(ObstacleCorrectionTest, testObstacleCorrectionUnreachableFromAnInvalidPosition) {
    loadFromString(" #########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "##########"
                   "######### ", 10, 8);

    setTestUserPosition(1, 1);
    setUserPosition(9, 7);
    EXPECT_POSITION(9, 7);
}
