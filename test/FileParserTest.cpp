#include "gmock/gmock.h"
#include "FileParser.h"

using namespace std;

TEST(FileParserTest, invalidUrlFingerprints) {
    FileParser fileParser;
    EXPECT_EXIT(fileParser.getFingerprints("was geht ab, partypeople?"); exit(EXIT_FAILURE), testing::ExitedWithCode(EXIT_FAILURE), "");
}


TEST(FileParserTest, parseJson) {    

    vector<Fingerprint> fingerprints = FileParser::getFingerprints("res/test_fingerprint.json");
    
    // Because there is only one Fingerprint we can get the first one.
    Fingerprint fingerprint = fingerprints[0];
    Histogram histogram = fingerprint.getHistogram();
    
    // Test the fingerprint id.
    ASSERT_EQ("test-fp", fingerprint.getId());
    
    // Test the histogram values.
    ASSERT_EQ("test-fp", fingerprint.getHistogram().getId());    
    ASSERT_EQ("00:21:d8:93:c2:31", histogram.getHistogram().find("00:21:d8:93:c2:31")->first);
    ASSERT_EQ(-97, histogram.getHistogram().find("00:21:d8:93:c2:31")->second.find(-97)->first);
    // The delta needs to be 0.1 because of the way floats work in C++. 0.6 was turned into 0.59999999999999998.
    ASSERT_NEAR(0.6, histogram.getHistogram().find("00:21:d8:93:c2:31")->second.find(-97)->second, 0.1);
    
    // Test the point values.
    ASSERT_EQ(-99, fingerprint.getPoint().getLongitude());
    ASSERT_EQ(100, fingerprint.getPoint().getLatitude());
    ASSERT_NEAR(0.0, fingerprint.getPoint().getDistance(), 0.0);
}

TEST(FileParserTest, invalidUrlSensorData) {

    EXPECT_EXIT(FileParser::getSensorMeasurements("was geht ab, partypeople?"); exit(EXIT_FAILURE), testing::ExitedWithCode(EXIT_FAILURE), "");
}

TEST(FileParserTest, parseSensorData) {

    vector<SensorMeasurement> measurements = FileParser::getSensorMeasurements("res/test_sensordata.txt");
    
    ASSERT_EQ(2, measurements.size());
    ASSERT_EQ(205536939868000,  measurements[0].timestamp);
    ASSERT_EQ(205536959276000,  measurements[1].timestamp);
    ASSERT_EQ(-0.2860273f, measurements[0].x);
    ASSERT_EQ(-0.313268f, measurements[1].x);
    ASSERT_EQ(3.0237172f, measurements[0].y);
    ASSERT_EQ(3.0645783f, measurements[1].y);
    ASSERT_EQ(9.670447f, measurements[0].z);
    ASSERT_EQ(9.684067f, measurements[1].z);
}

TEST(FileParserTest, parseFingerprintsString) {

    vector<Fingerprint> fingerprints = FileParser::getFingerprintsFromString(
            "[{\"histogram\":{\"00:21:d8:93:c2:31\":{\"-97\":0.6}},\"id\":\"test-fp\",\"point\":{\"divergence\":0.0,\"latitude\":100,\"longitude\":-99}}]");
    
    // Because there is only one Fingerprint we can get the first one.
    Fingerprint fingerprint = fingerprints[0];
    Histogram histogram = fingerprint.getHistogram();
    
    // Test the fingerprint id.
    ASSERT_EQ("test-fp", fingerprint.getId());
    
    // Test the histogram values.
    ASSERT_EQ("test-fp", fingerprint.getHistogram().getId());    
    ASSERT_EQ("00:21:d8:93:c2:31", histogram.getHistogram().find("00:21:d8:93:c2:31")->first);
    ASSERT_EQ(-97, histogram.getHistogram().find("00:21:d8:93:c2:31")->second.find(-97)->first);
    // The delta needs to be 0.1 because of the way floats work in C++. 0.6 was turned into 0.59999999999999998.
    ASSERT_NEAR(0.6, histogram.getHistogram().find("00:21:d8:93:c2:31")->second.find(-97)->second, 0.1);
    
    // Test the point values.
    ASSERT_EQ(-99, fingerprint.getPoint().getLongitude());
    ASSERT_EQ(100, fingerprint.getPoint().getLatitude());
    ASSERT_EQ(0.f, fingerprint.getPoint().getDistance());
}

TEST(FileParserTest, getOccupancyGridMap) {

    
    unsigned int width, height;
    vector<float> pixelValues = FileParser::getOccupancyGridmap("res/test_ogm.png", &width, &height);
    
    ASSERT_EQ(3, width);
    ASSERT_EQ(2, height);
    
    ASSERT_EQ(1.f, pixelValues[0]);
    ASSERT_NEAR(0.57f, pixelValues[1], 0.0014f);
    ASSERT_NEAR(0.f, pixelValues[2], 0.0014f);
    ASSERT_NEAR(0.57f, pixelValues[3], 0.0014f);
    ASSERT_EQ(0.f, pixelValues[4]);
    ASSERT_NEAR(0.87f, pixelValues[5], 0.0006f);
}
