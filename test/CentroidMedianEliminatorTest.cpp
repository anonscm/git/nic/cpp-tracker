#include "CentroidMedianEliminator.h"
#include "CandidateFixtures.h"
#include "gmock/gmock.h"

TEST(CentroidMedianEliminatorTest, testThatNoPointIsEliminatedWhenTheTotalAmountOfPointsIsToSmall) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesTooSmallAmount();
    vector<GeoPoint> expected = cf.createCandidatesTooSmallAmountExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatTheOutlierIsRemovedWhenItIsObviousThatItIsAnOutlierWithVaryingGeoPoints) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesWithAnObviousOutlier();
    vector<GeoPoint> expected = cf.createCandidatesWithAnObviousOutlierExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatTheOutlierIsRemovedWhenItIsObviousThatItIsAnOutlierWithTheOutlierAddedInTheMiddleOfTheMap) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesWithAnObviousOutlierInTheMiddleOfTheMap();
    vector<GeoPoint> expected = cf.createCandidatesWithAnObviousOutlierInTheMiddleOfTheMapExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatGeoPointsPlacedInASquareHaveNoOutliers) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesPlacedInASquare();
    vector<GeoPoint> expected = cf.createCandidatesPlacedInASquareExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatFourGeoPointsPlacedInASquareAndOnePlacedInTheMiddleHaveNoOutliers) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesPlacedInASquareAndOnePlacedInTheMiddle();
    vector<GeoPoint> expected = cf.createCandidatesPlacedInASquareAndOnePlacedInTheMiddleExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatAnOutlierFarAwayFromGeoPointsPlacedInASquareIsDetectedProperly) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesPlacedInASquareAndOnePlacedFarAway();
    vector<GeoPoint> expected = cf.createCandidatesPlacedInASquareAndOnePlacedFarAwayExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testRasterCoordinates) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesRasterCoordinates();
    vector<GeoPoint> expected = cf.createCandidatesRasterCoordinatesExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, test) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesTest();
    vector<GeoPoint> expected = cf.createCandidatesTestExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThat2PointsFarAwayFromAnother2PointsHaveNoOutliers) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesTwoPointsFarAwayFromAnotherTwoPoints();
    vector<GeoPoint> expected = cf.createCandidatesTwoPointsFarAwayFromAnotherTwoPointsExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThat2PointsFarAwayFrom3PointsNearToEachAreDetectedAsOutliers) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOther();
    vector<GeoPoint> expected = cf.createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOtherExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}

TEST(CentroidMedianEliminatorTest, testThatOutliersFromARealMapAreDetected) {
    CentroidMedianEliminator cme = CentroidMedianEliminator(1.5);
    CandidateFixtures cf = CandidateFixtures();

    vector<GeoPoint> geoPointsIn = cf.createCandidatesOutliersFromARealMap();
    vector<GeoPoint> expected = cf.createCandidatesOutliersFromARealMapExpected();
    vector<GeoPoint> geoPointsOut = cme.removeOutliers(geoPointsIn);

    for (int i = 0; i < geoPointsOut.size(); i++) {
        ASSERT_TRUE(expected[i].getLongitudeE6() == geoPointsOut[i].getLongitudeE6());
        ASSERT_TRUE(expected[i].getLatitudeE6() == geoPointsOut[i].getLatitudeE6());
    }
}
