cmake_minimum_required(VERSION 2.8)

enable_testing()

set(PROJECT_NAME_STR tracker_test)
project(${PROJECT_NAME_STR} C CXX)

file(GLOB SRC ${PROJECT_SOURCE_DIR}/*.cpp)

set(GTEST_LANG_CXX11 1)

add_subdirectory(../ext/gmock-1.7.0 gmock)
set(GMOCK_INCLUDE_DIR ../ext/gmock-1.7.0/include)
set(GTEST_INCLUDE_DIR ../ext/gmock-1.7.0/gtest/include)

include_directories(
	${X11_INCLUDE_DIRS}
    ${TRACKER_INCLUDE_DIR}
    ${EIGEN_INCLUDE_DIR}
    ${JSON_SPIRIT_INCLUDE_DIR}
    ${GMOCK_INCLUDE_DIR}
    ${GTEST_INCLUDE_DIR}
	${Boost_INCLUDE_DIRS}
	${LODEPNG_INCLUDE_DIR}
) 

add_executable(${PROJECT_NAME_STR} ${SRC})
add_dependencies(${PROJECT_NAME_STR} tracker)
add_dependencies(${PROJECT_NAME_STR} gmock)

target_link_libraries(
    ${PROJECT_NAME_STR}
    trackerall
	${Boost_LIBRARIES}
    gtest
    gtest_main
    gmock
    gmock_main
	${GCOV}
	${ADDITIONAL_LINK_LIBRARIES}
	${X11_LIBRARIES}
)

add_test(test1 ${PROJECT_NAME_STR})

install (TARGETS ${PROJECT_NAME_STR} RUNTIME DESTINATION bin)

file(GLOB RES ${PROJECT_SOURCE_DIR}/../res/*.*)
install(FILES ${RES} DESTINATION bin/res)
install(DIRECTORY DESTINATION bin/log)
