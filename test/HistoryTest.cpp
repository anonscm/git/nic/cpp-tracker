#include "History.h"
#include "gmock/gmock.h"

TEST(HistoryTest, getAverage) {
    History history(3);
    GeoPoint point;
    
    point.setXY(1, 2);
    history.add(point);
    point.setXY(10, 7);
    history.add(point);
    point.setXY(4, 3);
    history.add(point);
    
    GeoPoint pos = history.getAveragePoint();
    
    ASSERT_NEAR(5.f, pos.getX(), 0.3f);
    ASSERT_NEAR(4.f, pos.getY(), 0.3f); 
}


TEST(HistoryTest, getMedian) {
    History history(3);
    GeoPoint point;
    
    point.setXY(1, 2);
    history.add(point);
    point.setXY(10, 7);
    history.add(point);
    point.setXY(4, 3);
    history.add(point);
    
    GeoPoint pos = history.getMedianPoint();
    
    ASSERT_NEAR(4.f, pos.getX(), 0.3f);
    ASSERT_NEAR(3.f, pos.getY(), 0.3f); 
}

TEST(HistoryTest, getPointsEmptyHistory) {
    History history(3);

    GeoPoint pos = history.getAveragePoint();
    
    ASSERT_NEAR(0, pos.getX(), 0.3f);
    ASSERT_NEAR(0, pos.getY(), 0.3f); 
    
    pos = history.getMedianPoint();
    
    ASSERT_NEAR(0, pos.getX(), 0.3f);
    ASSERT_NEAR(0, pos.getY(), 0.3f); 
}

TEST(HistoryTest, getPointsHistoryNotFull) {
    History history(3);
    GeoPoint point;
    
    point.setXY(1, 2);
    history.add(point);
    point.setXY(4, 3);
    history.add(point);
    
    GeoPoint pos = history.getAveragePoint();
    
    ASSERT_NEAR(2.5f, pos.getX(), 0.3f);
    ASSERT_NEAR(2.5f, pos.getY(), 0.3f); 
    
    pos = history.getMedianPoint();
    
    ASSERT_NEAR(2.5f, pos.getX(), 0.3f);
    ASSERT_NEAR(2.5f, pos.getY(), 0.3f); 
}

TEST(HistoryTest, addPoints) {
    History history(1);
    GeoPoint point;
    
    point.setXY(1, 2);
    history.add(point);
    
    ASSERT_NEAR(1.f, history.getHistory()[0].getX(), 0.3f);
    ASSERT_NEAR(2.f, history.getHistory()[0].getY(), 0.3f); 
    ASSERT_EQ(1, history.getHistory().size());
    
    point.setXY(4, 3);
    history.add(point);
   
    ASSERT_NEAR(4.f, history.getHistory()[0].getX(), 0.3f);
    ASSERT_NEAR(3.f, history.getHistory()[0].getY(), 0.3f); 
    ASSERT_EQ(1, history.getHistory().size());
}