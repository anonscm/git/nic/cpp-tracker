#include "OccupancyGridMap.h"
#include "gmock/gmock.h"

using namespace std;

TEST(OccupancyGridMapTest, testSetResolution) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    V2d resolution = ogm.getResolution();
    
    ASSERT_NEAR(17.8f, resolution.x, 0.01f);
    ASSERT_NEAR(17.75f, resolution.y, 0.01f);
}

TEST(OccupancyGridMapTest, testGetGridPosition) {
     // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8); // (71,89)
    ogm.setBoundingBox(min, max);
    
    GeoPoint realpos(40, 50);
    V2i pixel = ogm.getGridPosition(V2i(realpos.getLatitudeE6(), realpos.getLongitudeE6()));
    
    ASSERT_EQ(2, pixel.x);
    ASSERT_EQ(2, pixel.y);
    
    // positions outside of the bounding box have 0 as value by default
    // therefore it doesn't matter if (i,j) or (lat, long) are outside the bounding box
    GeoPoint realpos2(400, 500);
    V2i pixel2 = ogm.getGridPosition(V2i(realpos2.getLatitudeE6(), realpos2.getLongitudeE6()));
    
    ASSERT_EQ(28, pixel2.x);
    ASSERT_EQ(22, pixel2.y);

}

TEST(OccupancyGridMapTest, testGetRealPosition) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    

    V2i l = ogm.getRealPosition({2, 2});
    
    ASSERT_NEAR(44, l.latitude, 0.01f);
    ASSERT_NEAR(44, l.longitude, 0.01f);
    
    // positions outside of the bounding box have 0 as value by default
    // therefore it doesn't matter if (i,j) or (lat, long) are outside the bounding box
    l = ogm.getRealPosition({200, 2});
    
    ASSERT_NEAR(44, l.latitude, 0.01f);
    ASSERT_NEAR(3568, l.longitude, 0.01f);
    
}

TEST(OccupancyGridMapTest, testGetGridValue) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[18] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
   
    ASSERT_EQ(0.6f, ogm.getGridValue(GeoPoint(62,62))); // (3,3)
    ASSERT_EQ(0.f, ogm.getGridValue(GeoPoint(62,-26))); // (-1,3)
    ASSERT_EQ(0.f, ogm.getGridValue(GeoPoint(62,1984))); // (111,3)
}

TEST(OccupancyGridMapTest, testGetGeoPointPositions) {  
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    OccupancyGridMap::GridPosition gridpos;
    vector<OccupancyGridMap::GridPosition> gridpositions;
    
    gridpos.x = 2;
    gridpos.y = 2;
    gridpositions.push_back(gridpos);
    gridpos.x = 0;
    gridpos.y = 3;
    gridpositions.push_back(gridpos);
    
    vector<GeoPoint> geopos = ogm.gridPositionToGeoPoint(gridpositions);
    
    ASSERT_NEAR(44, geopos[0].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(44, geopos[0].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(62, geopos[1].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(8, geopos[1].getLongitudeE6(), 0.01f);
}

TEST(OccupancyGridMapTest, testPlanPath) {    
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    GeoPoint start(62, 8);
    GeoPoint goal(44, 62); 
    vector<GeoPoint>  path = ogm.planPath(start, goal);
    
    ASSERT_NEAR(62, path[0].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(8, path[0].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[1].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(26, path[1].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(26, path[2].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[2].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[3].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[3].getLongitudeE6(), 0.01f);
    
}

TEST(OccupancyGridMapTest, testGetShortestPath) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    GeoPoint start(62, 8);
    vector<GeoPoint> goals;
    goals.push_back(GeoPoint(8, 62));
    goals.push_back(GeoPoint(26,26));
    goals.push_back(GeoPoint(62,62));
    
    vector<GeoPoint> path = ogm.planShortestPath(start, goals);
    
    ASSERT_NEAR(62, path[0].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(8, path[0].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[1].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(8, path[1].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(26, path[2].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(26, path[2].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(8, path[3].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[3].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(8, path[4].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[4].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(26, path[5].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[5].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(44, path[6].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[6].getLongitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[7].getLatitudeE6(), 0.01f);
    ASSERT_NEAR(62, path[7].getLongitudeE6(), 0.01f);
}

TEST(OccupancyGridMapTest, testPathPlanningWithBrokenMap) {
    // test grid map
    int width = 2;
    int height = 0;
    vector<float> pixelValues;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    GeoPoint start(62, 8);
    GeoPoint goal(44, 62); 
    vector<GeoPoint>  path = ogm.planPath(start, goal);
    
    ASSERT_TRUE(path.empty());
}

TEST(OccupancyGridMapTest, testPathPlanningWithoutBoundingBox) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint start(62, 8);
    GeoPoint goal(44, 62); 
    vector<GeoPoint>  path = ogm.planPath(start, goal);
    
    ASSERT_TRUE(path.empty());
}


TEST(OccupancyGridMapTest, testGetShortestPathWithBrokenMap) {
    // test grid map
    int width = 5;
    int height = 0;
    vector<float> pixelValues;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);
    
    GeoPoint start(62, 8);
    vector<GeoPoint> goals;

    goals.push_back(GeoPoint(8, 62));
    goals.push_back(GeoPoint(26,26));
    goals.push_back(GeoPoint(62,62));
    
    vector<GeoPoint> path = ogm.planShortestPath(start, goals);
    
    ASSERT_TRUE(path.empty());
    
}

TEST(OccupancyGridMapTest, testGetShortestPathWithoutBoundingBox) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint start(62, 8);
    vector<GeoPoint> goals;
    goals.push_back(GeoPoint(8, 62));
    goals.push_back(GeoPoint(26,26));
    goals.push_back(GeoPoint(62,62));
    
    vector<GeoPoint> path = ogm.planShortestPath(start, goals);
    
    ASSERT_TRUE(path.empty());
    
}

TEST(OccupancyGridMapTest, testEmptyGridMap) {
    // test grid map
    int width = 0;
    int height = 0;
    vector<float> pixelValues;

    OccupancyGridMap ogm(pixelValues, width, height);
    
    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    
    ogm.setBoundingBox(min, max);
    
    ASSERT_EQ(1.f, ogm.getGridValue(GeoPoint(40,50)));
    
}

TEST(OccupancyGridMapTest, testNoBoundingBox) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);

    OccupancyGridMap ogm(pixelValues, width, height);
    
    ASSERT_EQ(1.f, ogm.getGridValue(GeoPoint(40,50)));
}

TEST(OccupancyGridMapTest, testNoConstructorInput) {
    // test grid map
   OccupancyGridMap* ogm = new OccupancyGridMap();
    
    ASSERT_EQ(1.f, ogm->getGridValue(GeoPoint(40,50)));
}

TEST(OccupancyGridMapTest, testNotBlackAccessiblePixels) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);


    std::vector<V2i> accessiblePixels = ogm.getAccessiblePixels();
    std::vector<std::vector<float>> gridMap = ogm.getGridMap();

    int size = accessiblePixels.size();
    ASSERT_EQ(5*4 - 2, size); // Only 2 pixels were set to black (0) and are inaccessible.
    
    int counter = 0;
    for (int i=0; i<size; i++) {
        if (gridMap[accessiblePixels[i].x][accessiblePixels[i].y] != 0) {
            counter++;
        }
    }

    ASSERT_EQ(size, counter);
}

TEST(OccupancyGridMapTest, testNumberOfAccessiblePixelsEqualsNumberOfNotBlackPixelsInGridMap) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);


    std::vector<V2i> accessiblePixels = ogm.getAccessiblePixels();
    std::vector<std::vector<float>> gridMap = ogm.getGridMap();

    int size = accessiblePixels.size();
    int dimX = gridMap.size();
    int dimY = gridMap[0].size();
    int counter1 = 0;
    int counter2 = 0;

    for (int i=0; i<size; i++) {
        if (gridMap[accessiblePixels[i].x][accessiblePixels[i].y] != 0) {
            counter1++;
        }
    }

    for (int i = 0; i < dimX; i++) {
        for (int j = 0; j < dimY; j++) {
            if (gridMap[i][j] != 0) {
                counter2++;
            }
        }
    }

    ASSERT_EQ(counter1, counter2);
}

TEST(OccupancyGridMapTest, testRandomDistributedPositionInPixelIsInPixel) {
    // test grid map
    int width = 5;
    int height = 4;
    vector<float> pixelValues(width*height, 1.f);
    pixelValues[0] = 0.f; 
    pixelValues[5] = 0.f;
    pixelValues[12] = 0.2f;
    pixelValues[17] = 0.6f;

    OccupancyGridMap ogm(pixelValues, width, height);

    GeoPoint min, max; 
    min.setXY(0, 0);
    max.setXY(10, 8);
    ogm.setBoundingBox(min, max);

    int mismatch = 0;
    int size = 1000;
    
    int NW = 0;
    int NE = 0;
    int SE = 0;
    int SW = 0;

    int errorCountInBounds = 0;

    for (int x=0; x<width; x++) {
        for (int y=0; y<height; y++) {
            for (int k=0; k<size; k++) {
                V2i position = ogm.getRandomDistributedPositionInPixel({x, y});
                V2i pixel = ogm.getGridPosition(position);

                V2i check = ogm.getRealPosition(pixel);
                if (pixel.x != x || pixel.y != y) {
                    V2d resolution = ogm.getResolution();
                    cout << "mismatch " << x << "/" << y << " != " << pixel.x << "/" << pixel.y << endl;
                    cout << "lon/lat " << position.longitude << "/" << position.latitude << " != " << check.longitude << "/" << check.latitude << endl;
                    cout << "res " << resolution.x << " " << resolution.y << " " << endl;
                    mismatch++;

                    if (x == (width-1) || y == (height-1) || x==0 || y==0) {
                        errorCountInBounds++;
                    }
                } else {
                    // V2d resolution = ogm.getResolution();
                    // cout << "match " << x << " " << y << " " << x2 << " " << y2 << " " << longitude << " " << latitude 
                    // << " " << lonReal << " " << latReal << " " << resolution.x << " " << resolution.y << " " << min.getLongitudeE6() << " " <<  min.getLatitudeE6() << endl;
                }
                
                // In which quadrant of the pixel is this point?
                if (position.latitude >= check.latitude && position.longitude >= check.longitude) {
                    SE++;
                }
                if (position.latitude >= check.latitude && position.longitude < check.longitude) {
                    SW++;
                }
                if (position.latitude < check.latitude && position.longitude >= check.longitude) {
                    NE++;
                }
                if (position.latitude < check.latitude && position.longitude < check.longitude) {
                    NW++;
                }
            }
        }
    }
    
    // printf("NW=%d, NE=%d, SE=%d, SW=%d\n", NW, NE, SE, SW);   
    // cout << "errorCountInBounds: " << errorCountInBounds << endl;

    ASSERT_EQ(0, mismatch);
}