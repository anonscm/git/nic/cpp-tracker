#include "TrackerConfig.h"

#include <vector>

#include "gmock/gmock.h"

TEST(TrackerConfigTest, defaults) {
    TrackerConfig* config = TrackerConfig::getInstance();

    ASSERT_EQ(config->getInt(LOCALISATION_MODE), 2);
    ASSERT_EQ(config->getDouble(LOCALISATION_MODE), 2.0);
    ASSERT_EQ(config->getString(LOCALISATION_MODE), "2");
    ASSERT_EQ(config->getBool(LOCALISATION_MODE), true);
}

TEST(TrackerConfigTest, undefinedValues) {
	TrackerConfig* config = TrackerConfig::getInstance();

    ASSERT_EQ(config->getBool("nonexistent_config"), false);
    ASSERT_EQ(config->getInt("nonexistent_config"), 0);
    ASSERT_EQ(config->getString("nonexistent_config"), "");
    ASSERT_EQ(config->getDouble("nonexistent_config"), 0.0);
}

TEST(TrackerConfigTest, setValues) {
	TrackerConfig* config = TrackerConfig::getInstance();

	ASSERT_EQ(config->getInt(LOCALISATION_MODE), 2);
	config->setValue(LOCALISATION_MODE, "1.2");
	ASSERT_EQ(config->getInt(LOCALISATION_MODE), 1);
	ASSERT_EQ(config->getDouble(LOCALISATION_MODE), 1.2);
	ASSERT_EQ(config->getString(LOCALISATION_MODE), "1.2");

	// setting a value we don't know does nothing
	ASSERT_EQ(config->getInt("undefined"), 0);
	config->setValue("undefined", "42");
	ASSERT_EQ(config->getInt("undefined"), 0);

	// setting a value out-of-bounds: check that getter still returns min/max

	config->setValue(PCT_RANDOM_PARTICLES, "0");
	ASSERT_EQ(config->getDouble(PCT_RANDOM_PARTICLES), 0.0);
	config->setValue(PCT_RANDOM_PARTICLES, "-1");
	ASSERT_EQ(config->getDouble(PCT_RANDOM_PARTICLES), 0.0);
	config->setValue(PCT_RANDOM_PARTICLES, "100");
	ASSERT_EQ(config->getDouble(PCT_RANDOM_PARTICLES), 100.0);
	config->setValue(PCT_RANDOM_PARTICLES, "101");
	ASSERT_EQ(config->getDouble(PCT_RANDOM_PARTICLES), 100.0);
}

TEST(TrackerConfigTest, configValues) {
	TrackerConfig* config = TrackerConfig::getInstance();

	std::vector<TrackerConfigParameter> params = config->getConfig();

	ASSERT_TRUE(params.size() > 0);
}