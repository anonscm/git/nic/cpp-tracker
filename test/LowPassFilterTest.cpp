#include <cmath>

#include "LowPassFilter.h"
#include "gmock/gmock.h"

TEST(LowPassFilterTest, filterRawValuesForOneSource) {
    LowPassFilter lowPass;
    float weight = 0.3f;

    SensorMeasurement tmp = SensorMeasurement(5.f, 3.f, 8.f);

    // initial call - expect input values back
    SensorMeasurement filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(5.f, filtered.x, 0.0001f);
    ASSERT_NEAR(3.f, filtered.y, 0.0001f);
    ASSERT_NEAR(8.f, filtered.z, 0.0001f);

    // 2nd call - expect changed data
    tmp = SensorMeasurement(7.f, 9.f, 1.f);

    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(5.6f, filtered.x, 0.0001f);
    ASSERT_NEAR(4.8f, filtered.y, 0.0001f);
    ASSERT_NEAR(5.9f, filtered.z, 0.0001f);

    // 3rd call with disabled filter, expect input values back
    weight = 1.f;
    tmp = SensorMeasurement(6.f, 4.f, 2.f);
    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(6.f, filtered.x, 0.0001f);
    ASSERT_NEAR(4.f, filtered.y, 0.0001f);
    ASSERT_NEAR(2.f, filtered.z, 0.0001f);

    // 4th call, disregard values, expect result from last call again
    weight = 0.f;
    tmp = SensorMeasurement(1.f, 2.f, 3.f);
    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(6.f, filtered.x, 0.0001f);
    ASSERT_NEAR(4.f, filtered.y, 0.0001f);
    ASSERT_NEAR(2.f, filtered.z, 0.0001f);

    // 5th call, expect changed values
    weight = .5f;
    tmp = SensorMeasurement(2.f, 3.f, 4.f);
    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(4.f, filtered.x, 0.0001f);  // 6*.5 + 2*.5;
    ASSERT_NEAR(3.5f, filtered.y, 0.0001f); // 4*.5 + 3*.5
    ASSERT_NEAR(3.f, filtered.z, 0.0001f);  // 2*.5 + 4*.5  
}

TEST(LowPassFilterTest, filterRawValuesForMultipleSources) {
    LowPassFilter lowPass;
    float weight = 0.5f;

    // 1st calls for each sensor type - expect input values back
    SensorMeasurement tmp = SensorMeasurement(5.f, 3.f, 8.f);
    SensorMeasurement filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(5.f, filtered.x, 0.0001f);
    ASSERT_NEAR(3.f, filtered.y, 0.0001f);
    ASSERT_NEAR(8.f, filtered.z, 0.0001f);

    tmp = SensorMeasurement(7.f, 9.f, 1.f);
    filtered = lowPass.filterRaw(weight, tmp, MAGNETOMETER_DATA);
    ASSERT_NEAR(7.f, filtered.x, 0.0001f);
    ASSERT_NEAR(9.f, filtered.y, 0.0001f);
    ASSERT_NEAR(1.f, filtered.z, 0.0001f);

    // 2nd calls - expect changed values
    tmp = SensorMeasurement(6.f, 4.f, 2.f);
    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(5.5f, filtered.x, 0.0001f); // 5*.5 + 6*.5
    ASSERT_NEAR(3.5f, filtered.y, 0.0001f); // 3*.5 + 4*.5
    ASSERT_NEAR(5.0f, filtered.z, 0.0001f); // 8*.5 + 2*.5 

    tmp = SensorMeasurement(8.f, 6.f, 4.f);
    filtered = lowPass.filterRaw(weight, tmp, MAGNETOMETER_DATA);
    ASSERT_NEAR(7.5f, filtered.x, 0.0001f); // 7*.5 + 8*.5
    ASSERT_NEAR(7.5f, filtered.y, 0.0001f); // 9*.5 + 6*.5
    ASSERT_NEAR(2.5f, filtered.z, 0.0001f); // 1*.5 + 4*.5
}

TEST(LowPassFilterTest, filterCompassValue) {
    LowPassFilter lowPass;
    float weight = 0.5f;
    
    float tmp = 5.59f; // 320deg  
    float filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(5.59f, filtered, 0.01f);

    tmp = 0.79f; // 45deg
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(0.0484f, filtered, 0.01f);

    weight = 0.2f;
    tmp = 0.44f; // 25deg

    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(0.122f, filtered, 0.01f);

    weight = 0.f;
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(0.122f, filtered, 0.01f);

    weight = 1.f;
    tmp = 0.17f; // 10deg
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(tmp, filtered, 0.01f);

    weight = 0.3f;
    tmp = 5.24f;
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(6.119f, filtered, 0.01f);
}

TEST(LowPassFilterTest, compassFilterOff) {
    LowPassFilter lowPass;
    float weight = 1.f;
    
    float tmp = 5.59f; // 320deg

    float filtered = lowPass.filterCompass(weight, tmp);
    
    ASSERT_NEAR(tmp, filtered, 0.00001f);
    
    tmp = 0.79f; // 45deg
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(tmp, filtered, 0.00001f);
    
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(tmp, filtered, 0.00001f);
    
    filtered = lowPass.filterCompass(weight, tmp);
    ASSERT_NEAR(tmp, filtered, 0.00001f);
}

/*
TEST(LowPassFilterTest, filterRawValuesWithEmptyBuffer) {
    LowPassFilter lowPass;
    vector<SensorMeasurement> buffer;
    float weight = 0.3f;
    
    // New, empty buffer:
    SensorMeasurement filtered = lowPass.filterRaw(weight, buffer);
    ASSERT_NEAR(FLT_MAX, filtered.x, 0.0001f);
    ASSERT_NEAR(FLT_MAX, filtered.y, 0.0001f);
    ASSERT_NEAR(FLT_MAX, filtered.z, 0.0001f);

    SensorMeasurement tmp = SensorMeasurement(5.f, 3.f, 8.f);
   
    buffer.push_back(tmp);
    // Normal usage, with one value in the buffer:
    filtered = lowPass.filterRaw(weight, tmp, ACCELEROMETER_DATA);
    ASSERT_NEAR(5.f, filtered.x, 0.0001f);
    ASSERT_NEAR(3.f, filtered.y, 0.0001f);
    ASSERT_NEAR(8.f, filtered.z, 0.0001f);

    buffer.clear();
    
    // Now the buffer is empty again, and the old value shall be returned again:
    filtered = lowPass.filterRaw(weight, buffer);
    ASSERT_NEAR(5.f, filtered.x, 0.0001f);
    ASSERT_NEAR(3.f, filtered.y, 0.0001f);
    ASSERT_NEAR(8.f, filtered.z, 0.0001f);
}
*/
