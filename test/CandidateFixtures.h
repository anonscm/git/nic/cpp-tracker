#ifndef CANDIDATEFIXTURES_H
#define	CANDIDATEFIXTURES_H

#include <vector>
#include "GeoPoint.h"

using namespace std;

/*
 * For testing the outlier detection
 */

class CandidateFixtures {
public:
    CandidateFixtures();
    CandidateFixtures(const CandidateFixtures& orig);
    virtual ~CandidateFixtures();
    static vector<GeoPoint> createCandidatesTooSmallAmount();
    static vector<GeoPoint> createCandidatesTooSmallAmountExpected();
    static vector<GeoPoint> createCandidatesWithAnObviousOutlier();
    static vector<GeoPoint> createCandidatesWithAnObviousOutlierExpected();
    static vector<GeoPoint> createCandidatesWithAnObviousOutlierInTheMiddleOfTheMap();
    static vector<GeoPoint> createCandidatesWithAnObviousOutlierInTheMiddleOfTheMapExpected();
    static vector<GeoPoint> createCandidatesPlacedInASquare();
    static vector<GeoPoint> createCandidatesPlacedInASquareExpected();
    static vector<GeoPoint> createCandidatesPlacedInASquareAndOnePlacedInTheMiddle();
    static vector<GeoPoint> createCandidatesPlacedInASquareAndOnePlacedInTheMiddleExpected();
    static vector<GeoPoint> createCandidatesPlacedInASquareAndOnePlacedFarAway();
    static vector<GeoPoint> createCandidatesPlacedInASquareAndOnePlacedFarAwayExpected();
    static vector<GeoPoint> createCandidatesRasterCoordinates();
    static vector<GeoPoint> createCandidatesRasterCoordinatesExpected();
    static vector<GeoPoint> createCandidatesTest();
    static vector<GeoPoint> createCandidatesTestExpected();
    static vector<GeoPoint> createCandidatesTwoPointsFarAwayFromAnotherTwoPoints();
    static vector<GeoPoint> createCandidatesTwoPointsFarAwayFromAnotherTwoPointsExpected();
    static vector<GeoPoint> createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOther();
    static vector<GeoPoint> createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOtherExpected();
    static vector<GeoPoint> createCandidatesOutliersFromARealMap();
    static vector<GeoPoint> createCandidatesOutliersFromARealMapExpected();
    
    
    
private:
    
    static GeoPoint makeGeoPoint(const double x, const double y);

};

#endif	/* CANDIDATEFIXTURES_H */

