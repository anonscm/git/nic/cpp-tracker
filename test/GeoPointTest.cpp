#include "GeoPoint.h"
#include "gmock/gmock.h"

const double delta = 0.1;

TEST(GeoPointTest, GeoPointConstructorWithGeoPoint) {
    int longitude = 12345;
    int latitude = 54321;
    GeoPoint geoPoint(latitude, longitude);
    GeoPoint actualGeoPoint(geoPoint);

    ASSERT_EQ(latitude, actualGeoPoint.getLatitudeE6());
    ASSERT_EQ(longitude, actualGeoPoint.getLongitudeE6());
}

TEST(GeoPointTest, GetXY_LatLong0) {
    GeoPoint geoPoint(0, 0);

    double expectedResultX = 0;
    double expectedYResult = LATITUDE_LINE_DISTANCE * 0;

    ASSERT_EQ(expectedResultX, geoPoint.getX());
    ASSERT_EQ(expectedYResult, geoPoint.getY());
}

TEST(GeoPointTest, GetXY_LatLong1) {
    GeoPoint geoPointDecimal(1.0, 1.0);
    GeoPoint geoPointE6(1000000, 1000000);

    double expectedResultX = 111302.616;
    double expectedResultY = LATITUDE_LINE_DISTANCE * 1;

    ASSERT_NEAR(expectedResultX, geoPointDecimal.getX(), delta);
    ASSERT_NEAR(expectedResultX, geoPointE6.getX(), delta);
    ASSERT_NEAR(expectedResultY, geoPointDecimal.getY(), delta);
    ASSERT_NEAR(expectedResultY, geoPointE6.getY(), delta);
}

TEST(GeoPointTest, GetXY_LatLong15) {
    GeoPoint geoPointDecimal(15.0, 15.0);
    GeoPoint geoPointE6(15000000, 15000000);

    double expectedResultX = 107550.455 * 15;
    double expectedResultY = LATITUDE_LINE_DISTANCE * 15;

    ASSERT_NEAR(expectedResultX, geoPointDecimal.getX(), delta);
    ASSERT_NEAR(expectedResultX, geoPointE6.getX(), delta);
    ASSERT_NEAR(expectedResultY, geoPointDecimal.getY(), delta);
    ASSERT_NEAR(expectedResultY, geoPointE6.getY(), delta);
}

TEST(GeoPointTest, GetXY_BonnCoordinates) {
    double bonnLatitude = 50.733992;
    double bonnLongitude = 7.099814;

    GeoPoint geoPoint(bonnLatitude, bonnLongitude);

    double expectedResultX = 70598.276 * bonnLongitude;
    double expectedResultY = LATITUDE_LINE_DISTANCE * bonnLatitude;

    ASSERT_NEAR(expectedResultX, geoPoint.getX(), delta);
    ASSERT_NEAR(expectedResultY, geoPoint.getY(), delta);
}

TEST(GeoPointTest, SetXY_BonnCoordinates) {
    double bonnLatitude = 50.733992;
    double bonnLongitude = 7.099814;

    GeoPoint geoPoint(bonnLatitude, bonnLongitude);

    geoPoint.setXY(geoPoint.getX(), geoPoint.getY());

    double expectedLatitude = (int) (bonnLatitude * 1E6);
    double expectedLongitude = (int) (bonnLongitude * 1E6);

    ASSERT_EQ(expectedLatitude, geoPoint.getLatitudeE6());
    ASSERT_EQ(expectedLongitude, geoPoint.getLongitudeE6());
}

TEST(GeoPointTest, SetXY_ChangingValues) {
    GeoPoint geoPoint(0, 0);

    geoPoint.setXY(1234000, 4321000);

    int expectedLatitude = 38814282;
    int expectedLongitude = 14208016;

    ASSERT_EQ(expectedLatitude, geoPoint.getLatitudeE6());
    ASSERT_EQ(expectedLongitude, geoPoint.getLongitudeE6());
}

TEST(GeoPointTest, DistanceCalculation_NormalValues) {
    GeoPoint pointA;
    GeoPoint pointB;

    pointA.setXY(10, 6);
    pointB.setXY(7, 10);

    double expected = 5.0;

    double result = pointA.calculateEuclideanDistanceTo(pointB);

    ASSERT_NEAR(expected, result, delta);
}

TEST(GeoPointTest, DistanceCalculation_Zeroes) {
    GeoPoint pointA;
    GeoPoint pointB;

    pointA.setXY(0, 0);
    pointB.setXY(0, 0);

    double expected = 0.0;

    double result = pointA.calculateEuclideanDistanceTo(pointB);

    ASSERT_NEAR(expected, result, delta);
}

TEST(GeoPointTest, Equals) {
    GeoPoint pointA;
    pointA.setXY(100, 200);

    GeoPoint pointB;

    // Identical Points
    pointB.setXY(100, 200);
    ASSERT_TRUE(pointA.equals(pointB));
    ASSERT_TRUE(pointB.equals(pointA));

    // X is different
    pointB.setXY(101, 200);
    ASSERT_FALSE(pointA.equals(pointB));
    ASSERT_FALSE(pointB.equals(pointA));

    // Y is different
    pointB.setXY(100, 201);
    ASSERT_FALSE(pointA.equals(pointB));
    ASSERT_FALSE(pointB.equals(pointA));

    // Divergence is different.
    pointB.setXY(100, 200);
    pointB.setDistance(1);
    ASSERT_FALSE(pointA.equals(pointB));
    ASSERT_FALSE(pointB.equals(pointA));

    // Identical Points
    pointA.setDistance(1);
    ASSERT_TRUE(pointA.equals(pointB));
    ASSERT_TRUE(pointB.equals(pointA));
}

TEST(GeoPointTest, CompareTo) {
    GeoPoint pointA(1, 1);
    GeoPoint pointB(0, 0);

    // Note: if the divergence is the same then the result is dependent on the coordinates - but should really
    //       be treated as undefined. We don't care which one comes first in this case! But just don't want the
    //       result to be 0, because that would be inconsistent with equals().
    ASSERT_EQ(1, pointA.compareTo(pointB));

    pointA.setDistance(42);
    pointB.setDistance(42);
    ASSERT_EQ(1, pointA.compareTo(pointB));
    ASSERT_EQ(-1, pointB.compareTo(pointA));

    pointA.setDistance(41);
    ASSERT_EQ(-1, pointA.compareTo(pointB));
    ASSERT_EQ(1, pointB.compareTo(pointA));

    // Compare pointA with itself.
    ASSERT_EQ(0, pointA.compareTo(pointA));
}

TEST(GeoPointTest, Compare) {
    GeoPoint pointA(1, 1);
    GeoPoint pointB(0, 0);

    // Note: if the divergence is the same then the result is dependent on the coordinates - but should really
    //       be treated as undefined. We don't care which one comes first in this case! But just don't want the
    //       result to be 0, because that would be inconsistent with equals().
    ASSERT_EQ(0, GeoPoint::compare(pointA, pointB));

    pointA.setDistance(42);
    pointB.setDistance(42);
    ASSERT_EQ(0, GeoPoint::compare(pointA, pointB));
    ASSERT_EQ(0, GeoPoint::compare(pointB, pointA));

    pointA.setDistance(41);
    ASSERT_EQ(1, GeoPoint::compare(pointA, pointB));
    ASSERT_EQ(0, GeoPoint::compare(pointB, pointA));

    // Compare pointA with itself.
    ASSERT_EQ(0, GeoPoint::compare(pointA, pointA));
}

TEST(GeoPointTest, testHaversineDistance) {
    GeoPoint p1(50.713858, 7.057449);
    GeoPoint p2(50.7248242, 7.0559307);
    double distance = p1.calculateGreatCircleDistanceTo(p2);
    ASSERT_NEAR(1224.06, distance, 0.01);

    GeoPoint p3(36.12, -86.67);
    GeoPoint p4(33.94, -118.40);
    distance = p3.calculateGreatCircleDistanceTo(p4);
    ASSERT_NEAR(2886444.44, distance, 0.1);
}
