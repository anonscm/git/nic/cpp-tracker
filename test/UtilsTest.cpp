#include "gmock/gmock.h"
#include "Utils.h"
#include "TrackerLog.h"

#define EXCPECTED_FP_PRECISION 0.01f

TEST(UtilsTest, greyscaleNormalized) {
    float greyscaleWhite = Utils::greyscaleNormalized(255, 255, 255);
    float greyscaleBlack = Utils::greyscaleNormalized(0, 0, 0);
    float greyscaleGrey = Utils::greyscaleNormalized(127, 127, 127);
    float greyscaleLighterGray = Utils::greyscaleNormalized(60, 12, 190);

    ASSERT_EQ(1.0f, greyscaleWhite);
    ASSERT_EQ(0.0f, greyscaleBlack);
    ASSERT_NEAR(0.5f, greyscaleGrey, EXCPECTED_FP_PRECISION);
    ASSERT_NEAR(74.0f / 100.0f, greyscaleLighterGray, EXCPECTED_FP_PRECISION);
}

TEST(UtilsTest, greyscale) {
    unsigned char greyscaleWhite = Utils::greyscale(255, 255, 255);
    unsigned char greyscaleBlack = Utils::greyscale(0, 0, 0);
    unsigned char greyscaleGrey = Utils::greyscale(127, 127, 127);
    unsigned char greyscaleLighterGray = Utils::greyscale(60, 12, 190);

    ASSERT_EQ(255, greyscaleWhite);
    ASSERT_EQ(0, greyscaleBlack);
    ASSERT_EQ(127, greyscaleGrey);
    ASSERT_EQ(190, greyscaleLighterGray);
}

/*
TEST(UtilsTest, logger) {

    TrackerLog::log(NVM, "nothing to see here, move along");
    TrackerLog::log(FYI, "thought you should know");
    TrackerLog::log(WTF, "this shouldn't happen");
    TrackerLog::log(OMG, "abandon all hope. the world will end now.");
}
*/
