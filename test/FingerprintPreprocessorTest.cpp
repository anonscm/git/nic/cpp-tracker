#include "FingerprintPreprocessor.h"
#include "FileParser.h"
#include "Fingerprint.h"
#include "QuadraticFormDistance.h"

#include "gmock/gmock.h"
#include <vector>
#include <algorithm>


using namespace std;

// Test whether FingerprintPreprocessor::excludeFingerprintsByMean returns the requested/correct number of fingerprints.
TEST(FingerprintPreprocessorTest, testSizeOfFP) {

    vector<Fingerprint> histograms = FileParser::getFingerprints("res/fpPreprocesor_histograms.json");
    vector<Fingerprint> sourcefingerprints = FileParser::getFingerprints("res/fpPreprocesor_fingerprints.json");

    FingerprintPreprocessor p(sourcefingerprints);

    p.excludeFingerprintsByMean(histograms[0].getHistogram(), 30);
    vector<Fingerprint> fingerprints = p.getProcessedFingerprints();
    ASSERT_EQ(fingerprints.size(), 30);

    p.excludeFingerprintsByMean(histograms[0].getHistogram(), 200);
    fingerprints = p.getProcessedFingerprints();
    ASSERT_EQ(fingerprints.size(), 100); // There are only 100 FPs in the file, so that is the maximum that we can get.
}


// This test calculates the closest fingerprints using the QFD and using the FingerprintPreprocessor.
// Both sets should be similar. Esp. the very closest ones must be included in the estimate.
TEST(FingerprintPreprocessorTest, testThatSimilarFingerprintsAreNotExcluded) {

    Histogram histogram = FileParser::getFingerprints("res/fpPreprocesor_histograms.json")[0].getHistogram();
    vector<Fingerprint> fingerprints = FileParser::getFingerprints("res/fpPreprocesor_fingerprints.json");

    // Calculate the estimated best 40 fingerprints (of 100):
    FingerprintPreprocessor p(fingerprints);
    p.excludeFingerprintsByMean(histogram, 40);
    vector<Fingerprint> fingerprintCandidates = p.getProcessedFingerprints();
    /*
    for (int i=0; i<fingerprintCandidates.size(); ++i) {
        printf("Candidates: %s\n", fingerprintCandidates[i].getId().c_str());
    }
    */

    // Calculate the "real"/expensive distances of all fingerprints:
    QuadraticFormDistance qfd;
    std::map<double, string> closestFingerprints; // distance -> ID
    for (int i=0; i<fingerprints.size(); i++) {
        double dist = qfd.compareHistograms(histogram, fingerprints[i].getHistogram());
        closestFingerprints.insert(pair<double, string>(dist, fingerprints[i].getId()));
    }

    // Now we want the best 15 (<- arbitrary guess and much too low!) fingerprints to be included in our estimate:
    int count = 15;
    for (map<double, string>::iterator it = closestFingerprints.begin(); it != closestFingerprints.end(); ++it) {
        // printf("----> %f = %s\n", it->first, it->second.c_str());
        if (--count == 0) break;
        
        string id = it->second;
        bool found = false;
        for (int i=0; i<fingerprintCandidates.size(); ++i) {
            if (fingerprintCandidates[i].getId() == id) {
                found = true;
                // printf("Found at fingerprintCandidates[%d]\n", i);
            }
        }
        if (!found) {
            // printf("Not found!\n");
        }
        
        ASSERT_TRUE(found);
    }
    
}


// TODO: where does the list of knownFingerprints come from?
TEST(FingerprintPreprocessorTest, testBestFP) {

    vector<Fingerprint> histograms = FileParser::getFingerprints("res/fpPreprocesor_histograms.json");
    vector<Fingerprint> sourcefingerprints = FileParser::getFingerprints("res/fpPreprocesor_fingerprints.json");

    FingerprintPreprocessor p(sourcefingerprints);
    p.excludeFingerprintsByMean(histograms[0].getHistogram(), 30);

    vector<Fingerprint> fingerprints = p.getProcessedFingerprints();
   
    string knownFingerprints [30] = {
        "FP-66",
        "FP-52",
        "FP-67",
        "FP-98",
        "FP-65",
        "FP-61",
        "FP-50",
        "FP-60",
        "FP-82",
        "FP-56",
        "FP-62",
        "FP-57",
        "FP-70",
        "FP-77",
        "FP-81",
        "FP-48",
        "FP-64",
        "FP-51",
        "FP-63",
        "FP-86",
        "FP-54",
        "FP-58",
        "FP-84",
        "FP-59",
        "FP-97",
        "FP-47",
        "FP-80",
        "FP-96",
        "FP-83",
        "FP-85"
    };

    ASSERT_EQ(sizeof(knownFingerprints)/sizeof(knownFingerprints[0]), fingerprints.size());

    set<string> names;
    for (int i=0; i<fingerprints.size(); i++) {
        // cout << fingerprints[i].getId() << endl;
        ASSERT_EQ(fingerprints[i].getId(), knownFingerprints[i]);
        names.insert(fingerprints[i].getId());
    }

    ASSERT_EQ(names.size(), fingerprints.size());
}


// Test whether FingerprintPreprocessor precalculates exactly one "FingerprintWithMean" for each source-FP.
TEST(FingerprintPreprocessorTest, testFingerprintWithMeanSize) {

    vector<Fingerprint> histograms = FileParser::getFingerprints("res/fpPreprocesor_histograms.json");
    vector<Fingerprint> sourcefingerprints = FileParser::getFingerprints("res/fpPreprocesor_fingerprints.json");

    FingerprintPreprocessor p(sourcefingerprints);

    vector<FingerprintWithMean> fingerprintWithMean = p.getFingerprintsWithMeans();

    ASSERT_EQ(fingerprintWithMean.size(), sourcefingerprints.size());
}


TEST(FingerprintPreprocessorTest, testGetMeanOfTransmitterValues) {

    vector<Fingerprint> sourcefingerprints;
    FingerprintPreprocessor p(sourcefingerprints);
    map<int, float> transmitterValues;

    ASSERT_EQ(INT_MAX, p.getMeanOfTransmitterValues(transmitterValues));
    
    transmitterValues.insert(make_pair(-20, 1.0));
    ASSERT_EQ(-20, p.getMeanOfTransmitterValues(transmitterValues));

    transmitterValues.clear();
    transmitterValues.insert(make_pair(-20, 0.5));
    transmitterValues.insert(make_pair(-30, 0.5));
    ASSERT_EQ(-25, p.getMeanOfTransmitterValues(transmitterValues));

    transmitterValues.clear();
    transmitterValues.insert(make_pair(-10, 0.2));
    transmitterValues.insert(make_pair(-20, 0.8));
    ASSERT_EQ(-18, p.getMeanOfTransmitterValues(transmitterValues));

    transmitterValues.clear();
    transmitterValues.insert(make_pair(-10, 0.1));
    transmitterValues.insert(make_pair(-20, 0.5));
    transmitterValues.insert(make_pair(-30, 0.4));
    ASSERT_EQ(-23, p.getMeanOfTransmitterValues(transmitterValues));
}
