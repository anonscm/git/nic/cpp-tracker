#include "CandidateFixtures.h"

CandidateFixtures::CandidateFixtures() {
}

CandidateFixtures::CandidateFixtures(const CandidateFixtures& orig) {
}

CandidateFixtures::~CandidateFixtures() {
}

vector<GeoPoint> CandidateFixtures::createCandidatesTooSmallAmount(){
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(5,4));
    geoPointsIn.push_back(makeGeoPoint(16,15));
    return geoPointsIn;
    
};
vector<GeoPoint> CandidateFixtures::createCandidatesTooSmallAmountExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(5, 4));
    expected.push_back(makeGeoPoint(16, 15));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesWithAnObviousOutlier() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(5, 4));
    geoPointsIn.push_back(makeGeoPoint(7, 6));
    geoPointsIn.push_back(makeGeoPoint(9, 3));
    geoPointsIn.push_back(makeGeoPoint(16, 15));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesWithAnObviousOutlierExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(5, 4));
    expected.push_back(makeGeoPoint(7, 6));
    expected.push_back(makeGeoPoint(9, 3));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesWithAnObviousOutlierInTheMiddleOfTheMap() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(5, 4));
    geoPointsIn.push_back(makeGeoPoint(9, 3));
    geoPointsIn.push_back(makeGeoPoint(16, 15));
    geoPointsIn.push_back(makeGeoPoint(7, 6));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesWithAnObviousOutlierInTheMiddleOfTheMapExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(5, 4));
    expected.push_back(makeGeoPoint(9, 3));
    expected.push_back(makeGeoPoint(7, 6));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquare() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(0, 0));
    geoPointsIn.push_back(makeGeoPoint(0, 2));
    geoPointsIn.push_back(makeGeoPoint(2, 0));
    geoPointsIn.push_back(makeGeoPoint(2, 2));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquareExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(0, 0));
    expected.push_back(makeGeoPoint(0, 2));
    expected.push_back(makeGeoPoint(2, 0));
    expected.push_back(makeGeoPoint(2, 2));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquareAndOnePlacedInTheMiddle() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(0, 0));
    geoPointsIn.push_back(makeGeoPoint(0, 2));
    geoPointsIn.push_back(makeGeoPoint(2, 0));
    geoPointsIn.push_back(makeGeoPoint(2, 2));
    geoPointsIn.push_back(makeGeoPoint(1, 1));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquareAndOnePlacedInTheMiddleExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(0, 0));
    expected.push_back(makeGeoPoint(0, 2));
    expected.push_back(makeGeoPoint(2, 0));
    expected.push_back(makeGeoPoint(2, 2));
    expected.push_back(makeGeoPoint(1, 1));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquareAndOnePlacedFarAway() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(0, 0));
    geoPointsIn.push_back(makeGeoPoint(0, 2));
    geoPointsIn.push_back(makeGeoPoint(2, 0));
    geoPointsIn.push_back(makeGeoPoint(2, 2));
    geoPointsIn.push_back(makeGeoPoint(15, 15));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesPlacedInASquareAndOnePlacedFarAwayExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(0, 0));
    expected.push_back(makeGeoPoint(0, 2));
    expected.push_back(makeGeoPoint(2, 0));
    expected.push_back(makeGeoPoint(2, 2));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesRasterCoordinates() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(2, 2));
    geoPointsIn.push_back(makeGeoPoint(2, 4));
    geoPointsIn.push_back(makeGeoPoint(4, 2));
    geoPointsIn.push_back(makeGeoPoint(4, 4));
    geoPointsIn.push_back(makeGeoPoint(6, 2));
    geoPointsIn.push_back(makeGeoPoint(6, 4));
    geoPointsIn.push_back(makeGeoPoint(6, 6));
    geoPointsIn.push_back(makeGeoPoint(2, 6));
    geoPointsIn.push_back(makeGeoPoint(4, 6));
    geoPointsIn.push_back(makeGeoPoint(8, 8));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesRasterCoordinatesExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(2, 2));
    expected.push_back(makeGeoPoint(2, 4));
    expected.push_back(makeGeoPoint(4, 2));
    expected.push_back(makeGeoPoint(4, 4));
    expected.push_back(makeGeoPoint(6, 2));
    expected.push_back(makeGeoPoint(6, 4));
    expected.push_back(makeGeoPoint(6, 6));
    expected.push_back(makeGeoPoint(2, 6));
    expected.push_back(makeGeoPoint(4, 6));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTest() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(11, 2));
    geoPointsIn.push_back(makeGeoPoint(14, 6));
    geoPointsIn.push_back(makeGeoPoint(17, 2));
    geoPointsIn.push_back(makeGeoPoint(14, 3));
    geoPointsIn.push_back(makeGeoPoint(14, 10));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTestExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(11, 2));
    expected.push_back(makeGeoPoint(14, 6));
    expected.push_back(makeGeoPoint(17, 2));
    expected.push_back(makeGeoPoint(14, 3));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTwoPointsFarAwayFromAnotherTwoPoints() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(2, 1));
    geoPointsIn.push_back(makeGeoPoint(4, 1));
    geoPointsIn.push_back(makeGeoPoint(6, 15));
    geoPointsIn.push_back(makeGeoPoint(8, 15));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTwoPointsFarAwayFromAnotherTwoPointsExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(2, 1));
    expected.push_back(makeGeoPoint(4, 1));
    expected.push_back(makeGeoPoint(6, 15));
    expected.push_back(makeGeoPoint(8, 15));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOther() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(10, 1));
    geoPointsIn.push_back(makeGeoPoint(11, 1));
    geoPointsIn.push_back(makeGeoPoint(11, 2));
    geoPointsIn.push_back(makeGeoPoint(1, 15));
    geoPointsIn.push_back(makeGeoPoint(20, 20));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesTwoPointsFarAwayFromThreePointsNearToEachOtherExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(10, 1));
    expected.push_back(makeGeoPoint(11, 1));
    expected.push_back(makeGeoPoint(11, 2));
    return expected;
};
vector<GeoPoint> CandidateFixtures::createCandidatesOutliersFromARealMap() {
    vector<GeoPoint> geoPointsIn;
    geoPointsIn.push_back(makeGeoPoint(50722124, 7061466));
    geoPointsIn.push_back(makeGeoPoint(50722124, 7061587));
    geoPointsIn.push_back(makeGeoPoint(50722033, 7061589));
    geoPointsIn.push_back(makeGeoPoint(50722032, 7061460));
    geoPointsIn.push_back(makeGeoPoint(50722087, 7061526));
    geoPointsIn.push_back(makeGeoPoint(50721550, 7061636));
    geoPointsIn.push_back(makeGeoPoint(50721494, 7061554));
    return geoPointsIn;
};
vector<GeoPoint> CandidateFixtures::createCandidatesOutliersFromARealMapExpected() {
    vector<GeoPoint> expected;
    expected.push_back(makeGeoPoint(50722124, 7061466));
    expected.push_back(makeGeoPoint(50722124, 7061587));
    expected.push_back(makeGeoPoint(50722033, 7061589));
    expected.push_back(makeGeoPoint(50722032, 7061460));
    expected.push_back(makeGeoPoint(50722087, 7061526));
    return expected;
};

GeoPoint CandidateFixtures::makeGeoPoint(const double x, const double y){
    GeoPoint nicGeoPoint = GeoPoint(0.0,0.0);
    nicGeoPoint.setXY(x,y);
    
    return nicGeoPoint;
};
