#include "QuadraticFormDistance.h"
#include "gmock/gmock.h"

using namespace std;

TEST(QuadraticFormDistanceTest, compareHistograms_bothEmpty) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    Histogram q;

    double qfDist = quadraticFormDistance.compareHistograms(p, q);

    ASSERT_NEAR(0.f, qfDist, 0.0000001f);
}

TEST(QuadraticFormDistanceTest, compareHistograms_oneEmpty) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-30, 25.f));
    tmpHist.insert(make_pair(-40, 30.f));
    p.insert("00:01:02:03:04:05", tmpHist);
    Histogram q;

    double qfDist = quadraticFormDistance.compareHistograms(p, q);

    ASSERT_NEAR(0.f, qfDist, 0.0000001f);
}

TEST(QuadraticFormDistanceTest, compareHistograms_identical) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-30, 25.f));
    tmpHist.insert(make_pair(-40, 30.f));
    p.insert("00:01:02:03:04:05", tmpHist);

    double qfDist = quadraticFormDistance.compareHistograms(p, p);

    ASSERT_NEAR(0.f, qfDist, 0.0000001f);
}

TEST(QuadraticFormDistanceTest, compareHistograms_simple) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-20, 25.f));
    tmpHist.insert(make_pair(-40, 30.f));
    p.insert("02:00:00:00:00:01", tmpHist);

    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    tmpHist2.insert(make_pair(-80, 10.f));
    q.insert("02:00:00:00:00:01", tmpHist2);

    double qfDist = quadraticFormDistance.compareHistograms(p, q);

    ASSERT_NEAR(23.6f, qfDist, 0.1f);
}

TEST(QuadraticFormDistanceTest, compareHistograms_multiple) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-20, 25.f));
    tmpHist.insert(make_pair(-40, 30.f));
    p.insert("02:00:00:00:00:01", tmpHist);
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-50, 10.0f));
    tmpHist2.insert(make_pair(-70, 20.0f));
    p.insert("00:01:02:03:04:05", tmpHist2);

    Histogram q;
    map<int, float> tmpHist3;
    tmpHist3.insert(make_pair(-20, 10.f));
    tmpHist3.insert(make_pair(-40, 20.f));
    tmpHist3.insert(make_pair(-80, 10.f));
    q.insert("02:00:00:00:00:01", tmpHist3);
    map<int, float> tmpHist4;
    tmpHist4.insert(make_pair(-50, 10.0f));
    q.insert("00:01:02:03:04:05", tmpHist4);

    double qfDist = quadraticFormDistance.compareHistograms(p, q);

    ASSERT_NEAR(21.8f, qfDist, 0.1f);
}

TEST(QuadraticFormDistanceTest, compareHistograms_3Histograms) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-20, 25.f));
    tmpHist.insert(make_pair(-40, 30.f));
    p.insert("02:00:00:00:00:01", tmpHist);

    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    tmpHist2.insert(make_pair(-80, 10.f));
    q.insert("02:00:00:00:00:01", tmpHist2);

    Histogram r;
    map<int, float> tmpHist3;
    tmpHist3.insert(make_pair(-50, 50.f));
    tmpHist3.insert(make_pair(-60, 20.f));
    tmpHist3.insert(make_pair(-70, 30.f));
    r.insert("02:00:00:00:00:01", tmpHist3);

    double qfDist = quadraticFormDistance.compareHistograms(p, q);
    ASSERT_NEAR(23.6f, qfDist, 0.1f);

    double qfDist2 = quadraticFormDistance.compareHistograms(p, r);
    ASSERT_NEAR(74.5f, qfDist2, 0.1f);

    double qfDist3 = quadraticFormDistance.compareHistograms(q, r);
    ASSERT_NEAR(68.3f, qfDist3, 0.1f);

    // These proportions are pretty obvious:
    ASSERT_TRUE(qfDist < qfDist2);
    ASSERT_TRUE(qfDist < qfDist3);
    ASSERT_TRUE(qfDist3 < qfDist2);
}

TEST(QuadraticFormDistanceTest, compareHistograms_logarithmicScale) {
    QuadraticFormDistance quadraticFormDistance;
    quadraticFormDistance.setUseLogScale(false);

    quadraticFormDistance.setUseLogScale(true);
    
    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-10, 10.f));
    tmpHist.insert(make_pair(-40, 10.f));
    p.insert("02:00:00:00:00:01", tmpHist);

    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    q.insert("02:00:00:00:00:01", tmpHist2);

    double qfDist = quadraticFormDistance.compareHistograms(p, q);

    ASSERT_NEAR(19.59f, qfDist, 0.1f);
}

TEST(QuadraticFormDistanceTest, compareHistogramsNoMatch) {
    QuadraticFormDistance quadraticFormDistance;
    
    Histogram p;
    map<int, float> tmpHist;
    tmpHist.insert(make_pair(-10, 10.f));
    tmpHist.insert(make_pair(-40, 10.f));
    p.insert("00:01:02:03:04:05", tmpHist);

    Histogram q;
    map<int, float> tmpHist2;
    tmpHist2.insert(make_pair(-20, 10.f));
    tmpHist2.insert(make_pair(-40, 20.f));
    q.insert("02:00:00:00:00:01", tmpHist2);
    
    double qfDist = quadraticFormDistance.compareHistograms(p, q);
    
    ASSERT_EQ(0.f, qfDist);
}