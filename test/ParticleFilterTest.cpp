#include "ParticleFilter.h"
#include "gmock/gmock.h"

using namespace std;

class ParticleFilterTestable : public ParticleFilter {
public:
    
        float m_nextRandomValue = 0.5f;
        float m_nextGaussianValue = 0.1f;
        int m_times = 0;

        ParticleFilterTestable() : ParticleFilter()  {
        }

        ParticleFilterTestable(float nextRandomValue) : ParticleFilter() {
            m_nextRandomValue = nextRandomValue;
        }

private:
    
        /**
         * There is the if in here to make this that a not-0 value is returned.
         * Added for {@link #testThatResamplingDoesNotCrashWhenNextRandomIsFirst0}
         */
         float nextRandom() {
            if (m_times == 0) {
                m_times++;
                return m_nextRandomValue;
            } else {
                return 0.1f;
            }
        }

        void setNextRandom(float nextRandomValue) {
            m_nextRandomValue = nextRandomValue;
        }
        
        float nextGaussian() {
            return m_nextGaussianValue;
        }

};


TEST(ParticleFilterTest, testInitialization) {
    vector<float> pixelValues;
    OccupancyGridMap ogm(pixelValues, 0, 0); // no well defined occupancy grid map leads to free space everywhere
    ParticleFilterTestable pf;
    pf.setGridMap(&ogm);
    pf.setScale(1.f);
    pf.setNumParticles(6);
    pf.initialize(0, 0);

    ASSERT_TRUE(6 == pf.m_particles.size());
    ASSERT_NEAR(0.17f, pf.m_particles[0].importanceWeight, 0.01f);
    ASSERT_NEAR(100.f, pf.m_particles[0].x, 0.01f);
    ASSERT_NEAR(100.f, pf.m_particles[0].y, 0.01f);
}

TEST(ParticleFilterTest, testResampling) {
    ParticleFilterTestable pf;
    pf.setNumParticles(4);
    pf.setScale(1.f);

    Particle tmp(1.f, 2.f, 0.25f);
    pf.m_particles.push_back(tmp);

    tmp.x = 3.f;
    tmp.y = 4.f;
    tmp.importanceWeight = 0.01f;
    pf.m_particles.push_back(tmp);

    tmp.x = 5.f;
    tmp.y = 6.f;
    tmp.importanceWeight = 0.73f;
    pf.m_particles.push_back(tmp);

    tmp.x = 7.f;
    tmp.y = 8.f;
    tmp.importanceWeight = 0.01f;
    pf.m_particles.push_back(tmp);

    pf.resample();

    ASSERT_NEAR(11.f, pf.m_particles[0].x, 0.01f);
    ASSERT_NEAR(15.f, pf.m_particles[1].x, 0.01f);
    ASSERT_NEAR(15.f, pf.m_particles[2].x, 0.01f);
    ASSERT_NEAR(15.f, pf.m_particles[3].x, 0.01f);
}

TEST(ParticleFilterTest, testUpdateAction) {
    float deltaX = 2.f;
    float deltaY = 0.5f;

    vector<float> pixelValues;
    OccupancyGridMap ogm(pixelValues, 0, 0); // no well defined occupancy grid map leads to free space everywhere
    ParticleFilterTestable pf;
    pf.setGridMap(&ogm);
    pf.setNumParticles(2);

    Particle tmp(1.f, 2.f, 0.6f);
    pf.m_particles.push_back(tmp);

    tmp.x = 4.f;
    tmp.y = 5.f;
    tmp.importanceWeight = 0.4f;
    pf.m_particles.push_back(tmp);

    pf.setScale(1.f);

    pf.updateWithDeadReckoning(deltaX, deltaY);

    ASSERT_NEAR(13.f, pf.m_particles[0].x, 0.01f);
    ASSERT_NEAR(12.5f, pf.m_particles[0].y, 0.01f);
    ASSERT_NEAR(16.f, pf.m_particles[1].x, 0.01f);
    ASSERT_NEAR(15.5f, pf.m_particles[1].y, 0.01f);
}

TEST(ParticleFilterTest, testUpdateSensor) {
    double wifiX = 1.f;
    double wifiY = 5.f;

    ParticleFilterTestable pf;
    pf.setNumParticles(2);

    Particle tmp(2.f, 4.f, 0.5f);
    pf.m_particles.push_back(tmp);

    tmp.x = 20.f;
    tmp.y = 40.f;
    tmp.importanceWeight = 0.5f;
    pf.m_particles.push_back(tmp);

    pf.updateWithWifiPosition(wifiX, wifiY);

    ASSERT_NEAR(0.966f, pf.m_particles[0].importanceWeight, 0.01f);
    ASSERT_NEAR(0.034f, pf.m_particles[1].importanceWeight, 0.01f);
}

TEST(ParticleFilterTest, testCalculatePosition) {
    ParticleFilterTestable pf;
    pf.setNumParticles(4);

    Particle tmp(1.f, 2.f, 0.25f);
    pf.m_particles.push_back(tmp);

    tmp.x = 3.f;
    tmp.y = 4.f;
    tmp.importanceWeight = 0.01f;
    pf.m_particles.push_back(tmp);

    tmp.x = 5.f;
    tmp.y = 6.f;
    tmp.importanceWeight = 0.73f;
    pf.m_particles.push_back(tmp);

    tmp.x = 7.f;
    tmp.y = 8.f;
    tmp.importanceWeight = 0.01f;
    pf.m_particles.push_back(tmp);

    GeoPoint pos = pf.calculatePosition();

    ASSERT_NEAR(4.f, pos.getX(), 0.2f);
    ASSERT_NEAR(5.f, pos.getY(), 0.2f);
}

TEST(ParticleFilterTest, testUpdateDivisionBy0) {
    ParticleFilterTestable pf;
    
    Particle tmp(0,0,0);
    pf.m_particles.push_back(tmp);
    pf.updateWithWifiPosition(0, 0);
    
    // see if function terminates without crashing
    ASSERT_TRUE(pf.m_particles[0].importanceWeight >= 0.f);
}

TEST(ParticleFilterTest, testNormalizeWeights) {
    ParticleFilter pf;
    
    Particle tmp(1.f, 2.f, 0.45f);
    pf.m_particles.push_back(tmp);

    tmp.x = 3.f;
    tmp.y = 4.f;
    tmp.importanceWeight = 0.05f;
    pf.m_particles.push_back(tmp);

    tmp.x = 5.f;
    tmp.y = 6.f;
    tmp.importanceWeight = 0.73f;
    pf.m_particles.push_back(tmp);
    
    pf.normalizeWeights(1.23f);
    
    ASSERT_NEAR(0.37f, pf.m_particles[0].importanceWeight, 0.05f);
    ASSERT_NEAR(0.04f, pf.m_particles[1].importanceWeight, 0.05f);
    ASSERT_NEAR(0.59f, pf.m_particles[2].importanceWeight, 0.05f);
}

TEST(ParticleFilterTest, testZeroParticleCluster) {
    ParticleFilter pf;
    pf.m_clusterRadius = 5; //cm
    
    vector<Particle> biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(0, biggestCluster.size());
}

TEST(ParticleFilterTest, testOneParticleCluster) {
    ParticleFilter pf;
    pf.setNumParticles(1);
    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    
    vector<Particle> biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(1, biggestCluster.size());

    ASSERT_EQ(1, biggestCluster[0].y);
    ASSERT_EQ(1, biggestCluster[0].x);
}

TEST(ParticleFilterTest, testTwoCluster) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));


    pf.m_particles.push_back(Particle(10,10,1));
    pf.m_particles.push_back(Particle(10,11,1));
    pf.m_particles.push_back(Particle(10,12,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_TRUE(3 == biggestCluster.size());

    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(10, biggestCluster[i].x);
        ASSERT_TRUE( (10 == biggestCluster[i].y) || (11 == biggestCluster[i].y) || (12 == biggestCluster[i].y) );
    }

}


TEST(ParticleFilterTest, testThreeCluster) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));


    pf.m_particles.push_back(Particle(10,10,1));
    pf.m_particles.push_back(Particle(10,11,1));
    pf.m_particles.push_back(Particle(10,12,1));

    pf.m_particles.push_back(Particle(10,20,1));
    pf.m_particles.push_back(Particle(10,21,1));
    pf.m_particles.push_back(Particle(10,22,1));
    pf.m_particles.push_back(Particle(10,23,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(4, biggestCluster.size());


    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(10, biggestCluster[i].x);
        ASSERT_TRUE( (20 == biggestCluster[i].y) || (21 == biggestCluster[i].y) || (22 == biggestCluster[i].y) || (23 == biggestCluster[i].y));
    }

}


TEST(ParticleFilterTest, testOneCluster) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(2, biggestCluster.size());

    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(1, biggestCluster[i].x);
        ASSERT_TRUE( (1 == biggestCluster[i].y) || (2 == biggestCluster[i].y) );
    }

}


TEST(ParticleFilterTest, testTwoBiggestClusterCheckX) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));

    pf.m_particles.push_back(Particle(10,10,1));
    pf.m_particles.push_back(Particle(10,11,1));
    pf.m_particles.push_back(Particle(10,12,1));

    pf.m_particles.push_back(Particle(10,20,1));
    pf.m_particles.push_back(Particle(10,21,1));
    pf.m_particles.push_back(Particle(10,22,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(3, biggestCluster.size());


    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(10, biggestCluster[i].x);
    }
}

TEST(ParticleFilterTest, testTwoBiggestClusterCheckXY) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));


    pf.m_particles.push_back(Particle(10,10,1));
    pf.m_particles.push_back(Particle(10,11,1));
    pf.m_particles.push_back(Particle(10,12,1));

    pf.m_particles.push_back(Particle(10,20,1));
    pf.m_particles.push_back(Particle(10,21,1));
    pf.m_particles.push_back(Particle(10,22,1));
    pf.m_particles.push_back(Particle(10,23,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(4, biggestCluster.size());


    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(10, biggestCluster[i].x);
        ASSERT_TRUE( (20 == biggestCluster[i].y) || (21 == biggestCluster[i].y) || (22 == biggestCluster[i].y) || (23 == biggestCluster[i].y));
    }
}


TEST(ParticleFilterTest, testTwoBiggestClusterCheckDistance) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,6,1));
    pf.m_particles.push_back(Particle(1,11,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(1, biggestCluster.size());
}


TEST(ParticleFilterTest, testTwoBiggestClusterCheckDistanceRandomParticles) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5; //cm

    pf.m_particles.push_back(Particle(1,1,1));
    pf.m_particles.push_back(Particle(1,2,1));

    pf.m_particles.push_back(Particle(1,7,1));
    pf.m_particles.push_back(Particle(1,7,1));
    pf.m_particles.push_back(Particle(1,8,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(3, biggestCluster.size());

    pf.m_clusterRadius = 5.1; //cm

    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(5, biggestCluster.size());
}

TEST(ParticleFilterTest, testOrderClusterInsertion) {
    ParticleFilter pf;

    pf.m_clusterRadius = 5;  //cm

    pf.m_particles.push_back(Particle(0,0,1));
    pf.m_particles.push_back(Particle(0,3,1));
    pf.m_particles.push_back(Particle(0,-2,1));
    pf.m_particles.push_back(Particle(0,4,1)); 
    pf.m_particles.push_back(Particle(0,-3,1));

    pf.m_particles.push_back(Particle(0,20,1));
    pf.m_particles.push_back(Particle(0,24,1));
    pf.m_particles.push_back(Particle(0,28,1));
    pf.m_particles.push_back(Particle(0,32,1));

    vector<Particle> biggestCluster;
    biggestCluster = pf.getBiggestCluster();

    ASSERT_EQ(5, biggestCluster.size());

    for(int i=0; i<biggestCluster.size(); i++) {
        ASSERT_EQ(0, biggestCluster[i].x);
        ASSERT_TRUE( (0 == biggestCluster[i].y) || (3 == biggestCluster[i].y) || 
                     (-2 == biggestCluster[i].y) || (4 == biggestCluster[i].y) || (-3 == biggestCluster[i].y));
    }
}