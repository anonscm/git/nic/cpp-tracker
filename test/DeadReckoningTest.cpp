#include <cmath>

#include "DeadReckoning.h"
#include "gmock/gmock.h"

using namespace std;

// We use this derived class to be able to override some methods which we use to "inject" test-data.
class DeadReckoningTestable : public DeadReckoning {
public:

    float m_azimuth;
    bool m_moving;
    
    // Just a proxy to call the protected method.
    float calculateAzimuthPublic() {
        return DeadReckoning::calculateAzimuth();
    }
    
    // Just a proxy to call the protected method.
    bool detectMovementPublic() {
        return DeadReckoning::detectMovement();
    }
    
private:

    // Return a constant mock-value.
    float calculateAzimuth() {
        return m_azimuth;
    }
    
    // Return a constant mock-value.
    bool detectMovement() const {
        return m_moving;
    }
    
};

TEST(DeadReckoningTest, delta_noMovement) {
    DeadReckoningTestable deadReckoning;
    deadReckoning.setDelta(10.f, 12.f);
    deadReckoning.m_moving = false;

    V2d delta = deadReckoning.getCurrentDelta();
    double x = delta.x;
    double y = delta.y;
    deadReckoning.calculateDelta();
    
    ASSERT_NEAR(x, deadReckoning.getCurrentDelta().x, 0.0);
    ASSERT_NEAR(y, deadReckoning.getCurrentDelta().y, 0.0);
}

TEST(DeadReckoningTest, delta_noAzimuth) {
    DeadReckoningTestable deadReckoning;
    deadReckoning.setDelta(10.f, 12.f);
    deadReckoning.m_azimuth = -1; // Might be passed a 0 or something instead of a NULL.

    V2d delta = deadReckoning.getCurrentDelta();
    double x = delta.x;
    double y = delta.y;
    deadReckoning.calculateDelta();
    
    ASSERT_NEAR(x, deadReckoning.getCurrentDelta().x, 0.0);
    ASSERT_NEAR(y, deadReckoning.getCurrentDelta().y, 0.0);
}

TEST(DeadReckoningTest, calculateDelta) {
    DeadReckoningTestable deadReckoning;
    deadReckoning.m_moving = true;
    deadReckoning.m_azimuth = 1.57f;
    deadReckoning.setBaseAngle(90);

    deadReckoning.calculateDelta();

    ASSERT_NEAR(0.0, deadReckoning.getCurrentDelta().x, 0.1);
    ASSERT_NEAR(75, deadReckoning.getCurrentDelta().y, 0.1);
}

TEST(DeadReckoningTest, clearDelta) {
    DeadReckoning deadReckoning;
    deadReckoning.setDelta(10.f, 12.f);
    GeoPoint tmp = deadReckoning.getDelta();
    
    ASSERT_NEAR(0.0, deadReckoning.getCurrentDelta().x, 0.0);
    ASSERT_NEAR(0.0, deadReckoning.getCurrentDelta().y, 0.0);
}

TEST(DeadReckoningTest, calculateDelta_noBaseAngle) {    
    DeadReckoningTestable deadReckoning;
    GeoPoint geoPoint;
    
    deadReckoning.m_moving = true;
    deadReckoning.setBaseAngle(0);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(0);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(0.0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0, geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(45);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(90);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(0, geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(180);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(-75.0, geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(270);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(-75.0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(0, geoPoint.getY(), 0.1);
}

TEST(DeadReckoningTest, calculateDelta_baseAnglePlus45) {
    DeadReckoningTestable deadReckoning;
    GeoPoint geoPoint;
    
    deadReckoning.m_moving = true;
    deadReckoning.setBaseAngle(45);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(0);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(45);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0, geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(90);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(180);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(270);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getY(), 0.1);
}

TEST(DeadReckoningTest, calculateDelta_baseAngleMinus45) {  
    DeadReckoningTestable deadReckoning;
    GeoPoint geoPoint;
    
    deadReckoning.m_moving = true;
    deadReckoning.setBaseAngle(-45);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(0);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(45);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0, geoPoint.getX(), 0.1);
    ASSERT_NEAR(0, geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(90);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(180);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getY(), 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(270);
    deadReckoning.calculateDelta();
    geoPoint = deadReckoning.getDelta();    
    ASSERT_NEAR(-75.0 / sqrt(2), geoPoint.getX(), 0.1);
    ASSERT_NEAR(75.0 / sqrt(2), geoPoint.getY(), 0.1);
}

TEST(DeadReckoningTest, addMultipleDeltasBeforeResetting) {  
    DeadReckoningTestable deadReckoning;
    GeoPoint geoPoint;
    
    deadReckoning.m_moving = true;
    deadReckoning.setBaseAngle(0);
    
    ASSERT_NEAR(0, deadReckoning.getCurrentDelta().x, 0.1);
    ASSERT_NEAR(0, deadReckoning.getCurrentDelta().y, 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(90);
    deadReckoning.calculateDelta();   
    ASSERT_NEAR(75, deadReckoning.getCurrentDelta().x, 0.1);
    ASSERT_NEAR(0, deadReckoning.getCurrentDelta().y, 0.1);
    
    deadReckoning.m_azimuth = deadReckoning.toRadians(45);
    deadReckoning.calculateDelta();   
    ASSERT_NEAR(128.025, deadReckoning.getCurrentDelta().x, 0.1);
    ASSERT_NEAR(53.025, deadReckoning.getCurrentDelta().y, 0.1);
    
    geoPoint = deadReckoning.getDelta();   
    ASSERT_NEAR(128.025f, geoPoint.getX(), 0.1);
    ASSERT_NEAR(53.025f, geoPoint.getY(), 0.1);
       
    ASSERT_NEAR(0, deadReckoning.getCurrentDelta().x, 0.1);
    ASSERT_NEAR(0, deadReckoning.getCurrentDelta().y, 0.1);
}

TEST(DeadReckoningTest, calculateAzimuthAndMovement) {
    DeadReckoningTestable deadReckoning;
    deadReckoning.setBaseAngle(90);
    
    SensorMeasurement acc(1.f, 2.f, 4.f);
    SensorMeasurement lin_acc(1.7f, 0.f, 0.f);
    SensorMeasurement mag(4.f, 2.f, 1.f);
    
    deadReckoning.addSensorData(acc, lin_acc, mag);
    
    ASSERT_TRUE(deadReckoning.detectMovementPublic());
    ASSERT_NEAR(4.97f, deadReckoning.calculateAzimuthPublic(), 0.1);
    
}

/*
TEST(DeadReckoningTest, addDataToBuffers) {
    DeadReckoning deadReckoning;
    vector<SensorMeasurement> bufferRawAcc;
    vector<SensorMeasurement> bufferRawMag;
    
    deadReckoning.setBufferSize(1);
    
    SensorMeasurement acc(1.f, 2.f, 4.f);
    SensorMeasurement acc2(1.7f, 0.f, 0.f);
    SensorMeasurement mag(5.f, 2.f, 4.f);
    SensorMeasurement mag2(7.f, 9.f, 8.f);
    float compass, compass2; 
    
    deadReckoning.addRawMeasurement(bufferRawAcc, acc);
    ASSERT_NEAR(1.f, bufferRawAcc.back().x, 0.1f);
    ASSERT_NEAR(2.f, bufferRawAcc.back().y, 0.1f);
    ASSERT_NEAR(4.f, bufferRawAcc.back().z, 0.1f);
    
    deadReckoning.addRawMeasurement(bufferRawAcc, acc2);
    ASSERT_NEAR(1.7f, bufferRawAcc.back().x, 0.1f);
    ASSERT_NEAR(0.f, bufferRawAcc.back().y, 0.1f);
    ASSERT_NEAR(0.f, bufferRawAcc.back().z, 0.1f);
    

    deadReckoning.addRawMeasurement(bufferRawMag, mag);
    ASSERT_NEAR(5.f, bufferRawMag.back().x, 0.1f);
    ASSERT_NEAR(2.f, bufferRawMag.back().y, 0.1f);
    ASSERT_NEAR(4.f, bufferRawMag.back().z, 0.1f);
    
    deadReckoning.addRawMeasurement(bufferRawMag, mag2);
    ASSERT_NEAR(7.f, bufferRawMag.back().x, 0.1f);
    ASSERT_NEAR(9.f, bufferRawMag.back().y, 0.1f);
    ASSERT_NEAR(8.f, bufferRawMag.back().z, 0.1f);

    compass = 1.f;
    compass2 = 7.f;
    deadReckoning.addCompassMeasurement(compass);
    ASSERT_NEAR(1.f, deadReckoning.getCompassBuffer().back(), 0.1f);
    
    deadReckoning.addCompassMeasurement(compass2);
    ASSERT_NEAR(7.f, deadReckoning.getCompassBuffer().back(), 0.1f);
}	
*/