#!/bin/bash

if [ $# -ne 2 ]; then
    echo "Usage: build-android.sh android_ndk_make android_abi"
    exit 1
fi

src_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ndk_prebuilt_make=$1
libtracker=libtrackerall.a
abi=$2
build_dir=$src_dir/build-android
build_dir_abi=$build_dir/build/$abi
dist_dir=$build_dir/dist
dist_dir_abi=$build_dir/dist/lib/$abi
install_dir=$build_dir/stage
libtracker_abi_dir=$install_dir/libs/$abi
cmake_android_toolchain=$src_dir/cmake-modules/android.toolchain.cmake
cmake_genenerate_makefiles="Unix Makefiles"

mkdir -p $build_dir
mkdir -p $build_dir_abi
mkdir -p $dist_dir
mkdir -p $dist_dir_abi
mkdir -p $install_dir

cd $build_dir_abi
cmake -G"$cmake_genenerate_makefiles" -DANDROID_NDK=$ANDROID_NDK -DLIBRARY_OUTPUT_PATH_ROOT=$install_dir -DANDROID_ABI=$abi -DCMAKE_BUILD_TOOL=$ndk_prebuilt_make -DCMAKE_TOOLCHAIN_FILE=$cmake_android_toolchain $src_dir
$ndk_prebuilt_make install
cp -f $libtracker_abi_dir/$libtracker $dist_dir_abi/$libtracker

cp -rf $build_dir_abi/dist/include $dist_dir

cd -

