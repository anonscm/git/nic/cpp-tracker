#!/bin/bash
mkdir -p build
cd build
set -e
cmake -DBUILD_PERFTEST=true -DUSE_GDB=false -DBUILD_TEST=false -DBUILD_JNI=false -G "Unix Makefiles" ../
make install
cd -
