#define SIMPLE_TEST 1

#if SIMPLE_TEST

#include <iostream>
#include <sys/time.h>

#include "FileParser.h"
#include "Fingerprint.h"
#include "UserLocator.h"
#include "Histogram.h"

#define DEBUG_TIMER
#include "DebugTimer.h"

#ifdef DEBUG_TIMER
DebugTimer DebugTimers[DebugTimer_Count] = { 0 };
#endif

using namespace std;

class Timer
{
public:
    Timer() { gettimeofday(&_tv, 0); _hits = 0; }
    ~Timer() { if (_tv.tv_sec > 0) elapsed(); }

    void elapsed() {
        struct timeval now;
        gettimeofday(&now, 0);
        
        double start = _tv.tv_sec + (_tv.tv_usec / 1000000.0);
        double end = now.tv_sec + (now.tv_usec / 1000000.0);
        double elapsed = end - start;
        double h = elapsed / _hits;

        cout << "elapsed: " << elapsed << " seconds, " << _hits << " calls, " << h << "s/call" << endl;
        _tv = {0,0};
        _hits = 0;
    }

    inline void count() { ++_hits; }

private:
    struct timeval _tv;
    int _hits;
};

#define MAPSIZE 5
typedef struct { int strength; float probability; } sp;

typedef struct {
    string id;
    sp map[MAPSIZE];
} histData;

histData data[] = {
    { "c2:ce:46:52:bb:4e", { {-78, 0.42857143}, {-74, 0.5714286} } },
    { "1c:1d:86:b7:04:e1", { {-61, 0.14285715}, {-56, 0.71428573}, {-53, 0.14285715} } },
    { "1c:1d:86:b7:04:e2", { {-59, 0.42857143}, {-53, 0.14285715}, {-52, 0.42857143} } },
    { "1c:1d:86:b7:04:e0", { {-57, 0.42857143}, {-52, 0.2857143}, {-50, 0.2857143} } },
    { "80:1f:02:57:50:0c", { {-85, 1.0} } },
    { "80:1f:02:de:8b:06", { {-77, 1.0} } },
    { "1c:1d:86:b7:04:e3", { {-60, 0.14285715}, {-53, 0.85714287} } },
    { "1c:1d:86:b7:04:e4", { {-58, 0.71428573}, {-52, 0.2857143} } },
    { "80:1f:02:de:82:42", { {-67, 1.0} } },
    { "1c:1d:86:b7:09:c0", { {-85, 0.2857143}, {-84, 0.42857143}, {-80, 0.2857143} } },
    { "1c:1d:86:b7:09:c1", { {-88, 0.42857143}, {-81, 0.14285715}, {-79, 0.42857143} } },
    { "1c:1d:86:cf:58:20", { {-89, 1.0} } },
    { "1c:1d:86:b7:09:c4", { {-85, 0.14285715}, {-84, 0.2857143}, {-81, 0.14285715}, {-80, 0.2857143}, {-79, 0.14285715} } },
    { "1c:1d:86:b7:09:c2", { {-84, 0.85714287}, {-83, 0.14285715} } },
    { "1c:1d:86:b7:09:c3", { {-84, 0.71428573}, {-82, 0.2857143} } },
    { "1c:1d:86:cf:58:24", { {-90, 1.0} } },
    { "1c:1d:86:cf:58:23", { {-88, 1.0} } },
    { "" }
};

int main(int argc, char **argv)
{
	cout << "tracker perftest start" << endl;

    // get fingerprints from json
    FileParser fileParser;
    vector<Fingerprint> fingerprints = fileParser.getFingerprints("build/dist/bin/res/fp-2014-12-02-17-53-54.json");
    cout << "got " << fingerprints.size() << " fingerprints" << endl;

    // initialize locator
    UserLocator ul(LOCALISATION_PARTICLEFILTER);
    ul.setFingerprints(fingerprints);

    // create histogram data
    Histogram hist;
    for (histData* hd = data; hd->id.length() > 0; ++hd)
    {
        map<int, float> tmp;
        for (int i=0; i<MAPSIZE; ++i)
        {
            int str = hd->map[i].strength;
            if (str != 0) 
            {
                tmp.insert(make_pair(str, hd->map[i].probability));
            }
        }
        if (tmp.size() > 0)
        {
            hist.insert(hd->id, tmp);
        }
    }
    
    // run timing loop
    Timer t;
    vector<GeoPoint> distanceSet;
    size_t count = 0;
    for (int i=0; i<100; ++i)
    {
        distanceSet = ul.calculateDistances(hist);
        count += distanceSet.size();
        
        t.count();
    }

    t.elapsed();

    cout << "got " << distanceSet.size() << " points" << endl;
    cout << count << endl;
	cout << "tracker perftest end" << endl;
}
#else

#include <iostream>
#include <sys/time.h>

#include "FileParser.h"
#include "Fingerprint.h"
#include "FingerprintPreprocessor.h"
#include "UserLocator.h"
#include "Histogram.h"
#include "DebugTimer.h"

#include <algorithm>

/*#define fingerprintsPath "/home/dseca/Schreibtisch/penny/"
#define fingerprintsFilePath fingerprintsPath "fingerprints_data.json"

#define histogramsPath "/home/dseca/Schreibtisch/penny/"
#define histogramsFilePath histogramsPath "histogramLog_wifi-2015-06-16-16-43-10.json"*/


#define fingerprintsPath "/home/dseca/Schreibtisch/trackerAccuracyPreprocessor/fingerprints/tarent/wifi/"
                         //"/home/dseca/Schreibtisch/trackerAccuracyPreprocessor/fingerprints/tarent/btle/"
#define fingerprintsFilePath fingerprintsPath "fingerprints_data.json"
                                              //"fingerprints_btle_data.json"

#define histogramsPath "/home/dseca/Schreibtisch/trackerAccuracyPreprocessor/Pos1.Compare.Accuracy_differences/measures/"
#define histogramsFilePath histogramsPath "histogramLog_wifi-2015-06-19-08-40-57.json"


#define positionsPath "/home/dseca/Schreibtisch/trackerAccuracyPreprocessor/Pos1.Compare.Accuracy_differences/measures/"
#define positionsFilePath positionsPath "positions.json"

using namespace std;

#ifdef DEBUG_TIMER
DebugTimer DebugTimers[DebugTimer_Count] = { 0 };
#endif

class Timer
{
public:
    Timer() { 
        gettimeofday(&_tv, 0); 
        _hits = 0; 
    }
    ~Timer() { if (_tv.tv_sec > 0) elapsed(); }

    void elapsed() {
        struct timeval now;
        gettimeofday(&now, 0);
        
        double start = _tv.tv_sec + (_tv.tv_usec / 1000000.0);
        double end = now.tv_sec + (now.tv_usec / 1000000.0);
        double elapsed = end - start;
        double h = elapsed / _hits;

        cout << "elapsed: " << elapsed << " seconds, " << _hits << " calls, " << h << "s/call" << endl;
        
        _tv = {0,0};
        _hits = 0;
    }

    inline void count() { ++_hits; }

private:
    struct timeval _tv;
    int _hits;
};

map<std::string, vector<std::string>> run(bool optimize) {
    map<std::string, vector<std::string>> result;
    Timer tIteration;
    tIteration.count();

    cout << "tracker perftest start " << endl;
    FileParser fileParser;

    // initialize locator
    

    vector<Fingerprint> histograms = fileParser.getFingerprints(histogramsFilePath);
    vector<Fingerprint> sourcefingerprints = fileParser.getFingerprints(fingerprintsFilePath);
    vector<Fingerprint> fingerprints = sourcefingerprints;

    

    UserLocator ul(LOCALISATION_PARTICLEFILTER);
    ul.setFingerprints(fingerprints);
    

    for(int cont=0; cont<histograms.size(); cont++) {
    
        // create histogram data
        Histogram hist = histograms[cont].getHistogram();

        
        cout << "got " << fingerprints.size() << " fingerprints" << endl;

        if(optimize){
            FingerprintPreprocessor p(sourcefingerprints);
            p.excludeFingerprintsByMean(hist, 30);
            fingerprints =p.getProcessedFingerprints();
            ul.setFingerprints(fingerprints);
        }
        

        // run timing loop
        Timer t;
        vector<GeoPoint> distanceSet;
        vector<float> distances;

        t.count();
        distanceSet = ul.calculateDistances(hist);
        t.elapsed();

        vector<GeoPoint> nearestNeighbours = ul.getNearestNeighbours(distanceSet);

        cout << "HistogramFingerprintID " << hist.getId() << endl;
        vector<std::string> result_fp;
        for(int i=0; i<nearestNeighbours.size(); i++){
            std::string fp_id ="";
            for(auto fp = fingerprints.begin(); fp != fingerprints.end(); fp++){
                if(fp->getPoint().getX() == nearestNeighbours[i].getX() && fp->getPoint().getY() == nearestNeighbours[i].getY()){
                    fp_id = fp->getId();
                    break;
                }

            }

            result_fp.push_back(fp_id);

            std::cout << "DIST[" << i << "] id=" << fp_id << " x=" << nearestNeighbours[i].getX() << " y="<< nearestNeighbours[i].getY() << " d=" << nearestNeighbours[i].getDistance() << std::endl;

        }
        
        result.insert(std::pair<std::string, vector<std::string>>(hist.getId(), result_fp));

        cout << "got " << distanceSet.size() << " points" << endl;
        cout << "tracker perftest end" << endl;
    }    
    tIteration.elapsed();
    
    return result;
}

int main(int argc, char **argv) {
    cpu_set_t cpuMask;
    CPU_ZERO(&cpuMask);
    CPU_SET(0, &cpuMask);
    sched_setaffinity(0, sizeof(cpuMask), &cpuMask);


    map<std::string, vector<std::string>> result1 = run(true);

    cout << "*************************" << endl;

    map<std::string, vector<std::string>> result2 = run(false);

    cout << "*************************" << endl;

    bool isEqual = false;
    int countFalse = 0;

    for(auto it = result1.begin(); it != result1.end(); it++){

        auto it2= result2.find(it->first);
        
        if ( it->second.size() < it2->second.size() )
          isEqual = std::equal ( it->second.begin(), it->second.end(), it2->second.begin() );
        else
          isEqual = std::equal ( it2->second.begin(), it2->second.end(), it->second.begin() );


        if(!isEqual) {
            cout << "HistogramFingerprintID " << it->first << " shows differences." << endl;
            countFalse++;
        }

        isEqual = false;
    }
    cout << "Counted " << countFalse << " differences between calculations." << endl;
	
	#ifdef DEBUG_TIMER
        for (int i=0; i<DebugTimer_Count; ++i)
        {
            DebugTimer* t = DebugTimers + i;
            if (t->hits > 0)
            {
                cout << i << " " << t->cycles << "c " << t->hits << "h " << t->cycles/t->hits << "c/h";
                cout << " " << t->seconds << "s " << std::fixed << std::setprecision(8) << t->seconds/t->hits << "s/h";
                cout << " " << t->cycles/t->seconds << "c/s" << endl;
            }
        }
    #endif
}

#endif
