cmake_minimum_required(VERSION 2.8)

set(PROJECT_NAME_STR perftest)
project(${PROJECT_NAME_STR} C CXX)

file(GLOB SRC ${PROJECT_SOURCE_DIR}/src/*.cpp)

add_executable(${PROJECT_NAME_STR} ${SRC})
add_dependencies(${PROJECT_NAME_STR} tracker)

set(PROJECT_EXECUTABLE_NAME ${PROJECT_NAME_STR})

include_directories(
	${X11_INCLUDE_DIRS}
    ${TRACKER_INCLUDE_DIR}
	${JSON_SPIRIT_INCLUDE_DIR}
	${EIGEN_INCLUDE_DIR}
    ${Boost_INCLUDE_DIRS}
    ${LODEPNG_INCLUDE_DIR}
) 

target_link_libraries(
    ${PROJECT_EXECUTABLE_NAME}
    trackerall
	${Boost_LIBRARIES}
	${GCOV}
	${ADDITIONAL_LINK_LIBRARIES}
	${X11_LIBRARIES}
)

install (TARGETS ${PROJECT_NAME_STR} RUNTIME DESTINATION bin)

file(GLOB RES ${PROJECT_SOURCE_DIR}/../res/*.*)
install(FILES ${RES} DESTINATION bin/res)
install(DIRECTORY DESTINATION bin/log)
