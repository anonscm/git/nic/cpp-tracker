#ifndef LOWPASSFILTER_H
#define	LOWPASSFILTER_H

#include "SensorMeasurement.h"

/**
 * A low pass filter for of sensor measurement tuples or scalar angles in order to smooth the values
 * <p/><p/>
 * newValue = lastValue * (1 - weight) + x_i * weight
 */
class LowPassFilter {
public:  
    LowPassFilter();
    
    /**
     * weight = 1 disables filter, weight = 0 value does not change
     */
    SensorMeasurement filterRaw(float weight, const SensorMeasurement& sensor, SensorType sensorType);
    float filterCompass(float weight, float newCompassValue);
    
private:
    float m_filteredCompassValue;
    SensorMeasurement m_filteredValues[3];
};

#endif	/* LOWPASSFILTER_H */

