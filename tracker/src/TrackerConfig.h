#ifndef TRACKER_CONFIG_H
#define	TRACKER_CONFIG_H

#include <map>
#include <vector>
#include "TrackerConfigParameter.h"


/**
 * Default values used in the online tracker as well as the offline standalone tracker if not stated otherwise
 */
class TrackerConfig {
public:
    static TrackerConfig* getInstance();
    
    bool getBool(const std::string& name) const;
    int getInt(const std::string& name) const;
    double getDouble(const std::string& name) const;

    std::string getString(const std::string& name) const;

    void setValue(const std::string& name, const std::string& value);

    std::vector<TrackerConfigParameter> getConfig() const;
    
    
private:
    TrackerConfig();

    TrackerConfigParameter* getParameter(const std::string& name) const;

    std::map<std::string, TrackerConfigParameter*> m_config;
};

#endif	/* TRACKER_CONFIG_H */

