#ifndef HISTORY_H
#define	HISTORY_H

#include <vector>

#include "GeoPoint.h"

/**
 * A history of geopoint positions with calculations for the mean or median point out of those positions
 */
class History {
public:
    History(int size);
    
    void add(GeoPoint pos);
    
    GeoPoint getAveragePoint() const;
    GeoPoint getMedianPoint() const;
    
    std::vector<GeoPoint> getHistory() const {
        return m_history;
    }
    
private:
    int m_historySize;

    // TODO: change this to be a deque?
    std::vector<GeoPoint> m_history;
};

#endif	/* HISTORY_H */

