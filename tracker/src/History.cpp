#include "History.h"

#include <algorithm>

using namespace std;

History::History(int size) {
    m_historySize = size;
}

void History::add(GeoPoint pos) {
    if (m_history.size() == m_historySize)
        m_history.erase (m_history.begin());
    m_history.push_back(pos);
}

GeoPoint History::getAveragePoint() const {
    float meanX = 0;
    float meanY = 0;
    
    for (int i = 0; i < m_history.size(); i++) {
        meanX += m_history[i].getX();
        meanY += m_history[i].getY();
    }
    if (!m_history.empty()) {
        meanX = meanX/m_history.size();
        meanY = meanY/m_history.size();
    }
    
    return GeoPoint::makePoint(meanX, meanY);
}

GeoPoint History::getMedianPoint() const {
    vector<double> xVals;
    vector<double> yVals;
    double medianX = 0;
    double medianY = 0;
    
    if (!m_history.empty()) {
        for (int i = 0; i < m_history.size(); i++) {
            xVals.push_back(m_history[i].getX());
            yVals.push_back(m_history[i].getY());
        }

        sort(xVals.begin(), xVals.end());
        sort(yVals.begin(), yVals.end());

        int middleIndex = m_history.size() / 2;
        if (m_history.size() % 2 == 1) {
            // odd list size
            medianX = xVals[middleIndex];
            medianY = yVals[middleIndex];
        } else {
            // even list size
            medianX = 0.5f * (xVals[middleIndex - 1] + xVals[middleIndex]);
            medianY = 0.5f * (yVals[middleIndex - 1] + yVals[middleIndex]);
        }
    }

    return GeoPoint::makePoint(medianX, medianY);
}