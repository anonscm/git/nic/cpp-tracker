#include "FingerprintPreprocessor.h"
#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <climits>

using namespace std;

FingerprintPreprocessor::FingerprintPreprocessor() {
}

FingerprintPreprocessor::FingerprintPreprocessor(const vector<Fingerprint>& fingerprints)
    : m_sourceFingerprints(fingerprints) {
        computeFingerprintsWithMeans();
}

void FingerprintPreprocessor::setFingerprints(const vector<Fingerprint>& fingerprints) {
    m_sourceFingerprints = fingerprints;
    computeFingerprintsWithMeans();
}

void FingerprintPreprocessor::computeFingerprintsWithMeans() {
    m_fingerprintsWithMeans.reserve(m_sourceFingerprints.size());
    for (auto it = m_sourceFingerprints.begin(); it != m_sourceFingerprints.end(); it++) {
        FingerprintWithMean fingerprintWithMean(*it);
        
        const Histogram &hist = it->getHistogram();

        for (auto it = hist.begin(); it != hist.end(); it++) {
            fingerprintWithMean.transmitterMeanValues[it->first] = getMeanOfTransmitterValues(it->second);
        }

        m_fingerprintsWithMeans.push_back(fingerprintWithMean);
    }
}

void FingerprintPreprocessor::excludeFingerprintsByMean(const Histogram& userHistogram, size_t maxCount) {
    map<string, int> userHistogramMeanValues;

    for (auto it = userHistogram.begin(); it != userHistogram.end(); it++) {
        int mean = getMeanOfTransmitterValues(it->second);
        userHistogramMeanValues[it->first] = mean;
    }

    // Fuer alle fingerprints...    
    for (auto it = m_fingerprintsWithMeans.begin(); it != m_fingerprintsWithMeans.end(); it++) {
        it->score = 0;
        it->scoreCount = 0;
        // Fuer alle jetzt gesehenen transmitter...
        for (auto userHistIter = userHistogramMeanValues.begin(); userHistIter != userHistogramMeanValues.end(); userHistIter++) {
            // Finde iterator im aktuellen FP, der auf den mean des aktuellen transmitter zeigt...
            auto transmitterMeanIter = it->transmitterMeanValues.find(userHistIter->first);

            // Gibt es den transmitter in diesem FP ueberhaupt?
            if (transmitterMeanIter != it->transmitterMeanValues.end()) {

                int meanHist = userHistIter->second;
                int meanFP = transmitterMeanIter->second;
                
                it->score += abs(meanHist-meanFP);
                it->scoreCount++;
            }
        }
        if (it->scoreCount != 0) {
            it->score = it->score / it->scoreCount;
        } else {
            it->score = INT_MAX;
        }
    }

    sort(m_fingerprintsWithMeans.begin(), m_fingerprintsWithMeans.end());

    maxCount = min(maxCount, m_fingerprintsWithMeans.size());

    vector<Fingerprint> newFingerprints;
    newFingerprints.reserve(maxCount);

    for (int i=0; i<maxCount; i++) {
        newFingerprints.push_back(m_fingerprintsWithMeans[i].fingerprint);
    }

    m_processedFingerprints = newFingerprints;
}


// We assume that the transmitterValues are normalized, i.e. their float-values
// (which are the weights/probabilities of their respective strength) always add up to 1.
//
// If transmitterValues is empty return INT_MAX as mean... that really should
// not happen, because if there are no values for a transmitter then there
// should not be a(n empty) histogram for that transmitter either.

int FingerprintPreprocessor::getMeanOfTransmitterValues(const map<int, float>& transmitterValues) const {
    if (transmitterValues.empty()) {
        return INT_MAX;
    } else {
        float sum = 0;
        for (auto it = transmitterValues.begin(); it != transmitterValues.end(); it++) {
            sum += it->first * it->second;
        }

        return sum;
    }
}