    #ifndef GEOPOINT_H_
#define GEOPOINT_H_

#include <cmath>
#include "V2d.h"

//some compilers dont have M_PI defined in cmath
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

static const int LATITUDE_LINE_DISTANCE = 111325; // The static distance in meters between two lines of latitude

/**
 * Three constant values are used in the calculation because of the earth's irregular (not completely spherical)
 * shape.
 * 
 * Calculation source: http://www.nymanz.org/sandcollection/swaplist/Latitude%20and%20Longitude.pdf
 */
static const double EARTH_SHAPE_CONSTANTS[3] = {111412.84f, -93.5f, 0.118f};

#define COMPATIBILITY_SCALE     1E6

/**
 * Data structure for saving latitude and longitude, conversions to a (x,y)-coordinate system in which we can measure distances 
 * and functions for comparing two GeoPoints and distance calculation between them
 */
class GeoPoint {
public:

    GeoPoint();
    GeoPoint(double latitude, double longitude); // degrees (lat -90 to 90) (longitude -180 to 180))

    GeoPoint(int latitudeE6, int longitudeE6); // E6 format *10^6 (old, deprecated)
#ifdef TESTING
    GeoPoint(int latitudeE6, int longitudeE6, double distance);
#endif
    
    static GeoPoint makePoint(double x, double y);  // convenience factory to create a GeoPoint from a given X/Y coordinate

    double getX() const;
    double getY() const;

    void reset();
    void setXY(double x, double y);

    double calculateEuclideanDistanceTo(const GeoPoint&otherPoint) const;
    bool equals(const GeoPoint& other);
    int compareTo(const GeoPoint& other);
    static int compare(const GeoPoint& pointX, const GeoPoint& pointY);

    inline int getLongitudeE6() const {
        return m_longitude * COMPATIBILITY_SCALE;
    }

    inline int getLatitudeE6() const {
        return m_latitude * COMPATIBILITY_SCALE;
    }

    inline double getLongitude() const {
        return m_longitude;
    }

    inline double getLatitude() const {
        return m_latitude;
    }

    inline double getDistance() const {
        return m_distance;
    }

    inline void setDistance(double distance) {
        m_distance = distance;
    }

    double calculateGreatCircleDistanceTo(const GeoPoint&otherPoint) const;

private:

    double m_latitude, m_longitude;

    mutable V2d xyCache;
    double m_distance; // QF distance

    void setX(double x);
    void setY(double y);
    double getLatitudeInRadians() const;
    double getLongitudeDistanceInMeters() const;
};

#endif /* GEOPOINT_H_ */
