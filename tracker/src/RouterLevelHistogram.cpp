
#include "RouterLevelHistogram.h"

#include <vector>

// TODO: this code is unused, remove it

using namespace std;

RouterLevelHistogram::RouterLevelHistogram() {
}

RouterLevelHistogram::~RouterLevelHistogram() {
}

/*
 * Remove all elements from this histogram that are not present in some other histogram.
 */
int RouterLevelHistogram::intersect(RouterLevelHistogram rlh) {
    int size = m_rlh.size();
    
    for (map<string, float>::iterator it = m_rlh.begin(); it != m_rlh.end();) {
        if (rlh.getRouterLevelHistogram().find(it->first) == rlh.getRouterLevelHistogram().end()) {
            m_rlh.erase(it++);
        } else {
            ++it;
        }
    }
    
    return size - m_rlh.size();
}

/*
 * Convert a Histogram to a RouterLevelHistogram using the median of the measured signal-strengths for each
 * access point.
 */
void RouterLevelHistogram::makeMedianHistogram(Histogram histogram) {
    for (map<string, map<int, float> >::iterator it = histogram.begin(); it != histogram.end(); ++it) {
        m_rlh.insert(pair<string, float>(it->first, calculateMedianLevel(it->second)));    
    }  
}

/*
 * Convert a Histogram to a RouterLevelHistogram using the weighted average of the measured signal-strengths for
 * each access point.
*/
void RouterLevelHistogram::makeAverageHistogram(Histogram histogram) {
    for (map<string, map<int, float> >::iterator it = histogram.begin(); it != histogram.end(); ++it) {
        m_rlh.insert(pair<string, float>(it->first, calculateWeightedAverageLevel(it->second)));    
    }  
}

/*
 * Create a normalized RouterLevelHistogram, i.e. one where all the values sum up to 1.
 * For some algorithms, e.g. Kullback-Leibler, the histogram needs to be normalized as if it were a real
 * probability-distribution (which RouterLevelHistograms are not in any meaningful sense because the bins are all
 * totally independent).
*/
map<string, float> RouterLevelHistogram::normalize() {
   map<string, float> rlh;

   float sum = 0;
   for (map<string, float>::iterator it = m_rlh.begin(); it != m_rlh.end(); ++it) {
        sum += it->second;
    }

    for (map<string, float>::iterator it = m_rlh.begin(); it != m_rlh.end(); ++it) {
        rlh.insert(pair<string, float>(it->first, it->second/sum));    
    }  

   return rlh;
}

float RouterLevelHistogram::calculateMedianLevel(map<int, float> levels) {
    vector<int> strengths;
    
    for (map<int, float>::iterator it = levels.begin(); it != levels.end(); ++it) {
        strengths.push_back(it->first);
    }
    sort(strengths.begin(), strengths.end());
    
    int middleIndex = strengths.size() / 2;
    double median;

    if (strengths.size() % 2 == 1) {
        // odd list size
        median = strengths[middleIndex];
    } else {
        // even list size
        median = 0.5f * (strengths[middleIndex - 1] + strengths[middleIndex]);
    }
    return median;
}

float RouterLevelHistogram::calculateWeightedAverageLevel(map<int, float> levels) {
    float average = 0;
    for (map<int, float>::iterator it = levels.begin(); it != levels.end(); ++it) {
        average += it->first * it->second;
    }
    return average;
}

