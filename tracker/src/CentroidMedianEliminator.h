#ifndef CENTROIDMEDIANELIMINATOR_H
#define	CENTROIDMEDIANELIMINATOR_H

#include <vector>

#include "GeoPoint.h"

/**
 * Eliminates all points further than the calculated threshold distance with the given direct factor from the centroid, which is the median of all points. 
 * 
 * @param medianThresholdFactor
 */
class CentroidMedianEliminator {
public:
    CentroidMedianEliminator(float medianThresholdFactor);
    
    std::vector<GeoPoint> removeOutliers(const std::vector<GeoPoint>& candidateSet);
    
    void setThreshold(float threshold) {
        m_medianThresholdFactor = threshold;
    }
    
private:
    float m_medianThresholdFactor;
    
    GeoPoint getMedianPoint(const std::vector<GeoPoint>& candidateSet);
    double getMedianFromSortedList(const std::vector<double>& list);
    double calculateMedianReferencePointDistance(const std::vector<GeoPoint>& candidateSet, const GeoPoint& referencePoint);
    
};

#endif	/* CENTROIDMEDIANELIMINATOR_H */

