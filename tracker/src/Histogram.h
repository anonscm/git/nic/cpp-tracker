#ifndef HISTOGRAM_H_
#define HISTOGRAM_H_

#include <string>
#include <map>
#include <set>

/**
 * Saves RSSI values and their probabilities and provides functions for intersecting two histograms with regard to their measured access points
 */

class Histogram {
public:
    Histogram();
    Histogram(std::string id);
    
    // void retainAll(const std::map<std::string, std::map<int, float> > &toRetain);
    void retainAll(const std::set<std::string> &toRetain);

    // static std::map<std::string, std::map<int, float> > getCommonKeys(const Histogram &histogram1, const Histogram &histogram2);
    static std::set<std::string> getCommonKeys(const Histogram &histogram1, const Histogram &histogram2);

    inline const std::map<std::string, std::map<int, float> > &getHistogram() const {
        return m_histogram;
    }

    inline void setHistogram(std::map<std::string, std::map<int, float> > histogram) {
        m_histogram = histogram;
    }

    inline const std::string &getId() const {
        return m_id;
    }

    inline void setId(std::string id) {
        m_id = id;
    }
    
    /**
     * Insert function from map.
     * 
     * @param key MAC address or iBeacon id
     * @param value ($SIGNAL_STRENGTH, $PROBABILITY)
     */
    inline void insert(const std::string &key, std::map<int, float> value) {
        m_histogram.insert(std::make_pair(key, value));
    }

    /**
     * Begin function from map (const and non-const versions)
     */
    inline std::map<std::string, std::map<int, float> >::const_iterator begin() const {
        return m_histogram.begin();
    }
    inline std::map<std::string, std::map<int, float> >::iterator begin() {
        return m_histogram.begin();
    }

    /**
     * End function from map.
     */
    inline std::map<std::string, std::map<int, float> >::const_iterator end() const {
        return m_histogram.end();
    }

    /**
     * Find function from map.
     * 
     * @param key the MAC address or iBeacon id to search for
     */
    inline std::map<std::string, std::map<int, float> >::const_iterator find(const std::string &key) const {
        return m_histogram.find(key);
    }

    inline size_t size() const {
        return m_histogram.size();
    }

    // needs to be public for JNI access
    std::map<std::string, std::map<int, float> > m_histogram;

private:
    std::string m_id;
};

#endif /* HISTOGRAM_H_ */
