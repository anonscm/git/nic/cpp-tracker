#ifndef PARTICLEFILTER_H
#define	PARTICLEFILTER_H

#include <vector>
#include <random>

#include <boost/math/distributions/normal.hpp>

#include "GeoPoint.h"
#include "Particle.h"
#include "OccupancyGridMap.h"

/**
 * Probabilistic approach for combining motions and sensor readings i.e. dead reckoning and wifi/bluetooth positioning
 * 
 * @param scale Needs the scale of the map as an input to calculate the right covariance
 */
class ParticleFilter {

public:    
    ParticleFilter();
    
    void addRandomParticles(float curX, float curY);
    void initialize(float curX, float curY);
    void updateWithDeadReckoning(float dx, float dy);
    void updateWithWifiPosition(float x, float y);
    void resample();
    void normalizeWeights(float norm);
    GeoPoint calculatePosition();
    std::vector<Particle> getBiggestCluster();
    
    void setScale(float scale) {
        m_scale = scale;
    }

    #ifdef TESTING
    void setNumParticles(int numParticles) {
        m_numParticles = numParticles;
    }
    #endif
    
    void clearParticles() {
         m_particles.clear();
    }
    
    void setGridMap(OccupancyGridMap* gridMap) {
        m_gridMap = gridMap;
    }
    
    float m_clusterRadius;
    std::vector<Particle> m_particles;

private:
    float m_scale;
    int m_numParticles, m_numRandomParticles;
    float m_sigmaInit, m_sigmaAction, m_sigmaSensor, m_sigmaRandomParticles;
    
    OccupancyGridMap* m_gridMap;
        
    std::default_random_engine m_generator;
    
    virtual float nextGaussian();
	virtual float nextRandom();
};


#endif	/* PARTICLEFILTER_H */
