#ifndef PARTICLE_H
#define	PARTICLE_H

struct Particle {
    float x, y;
    float importanceWeight;
    
    Particle(float x, float y, float w) {
    	this->x = x;
    	this->y = y;
    	importanceWeight = w;
    };
};

#endif	/* PARTICLE_H */

