#include "QuadraticFormDistance.h"
#include "TrackerConfig.h"

#include <iostream>
#include <set>
#include <Eigen/Core>

#include "DebugTimer.h"

using namespace std;

QuadraticFormDistance::QuadraticFormDistance() {
	m_useLogScale = TrackerConfig::getInstance()->getBool(QFD_USE_LOG_SCALE);
}

// compare two Histograms, p and q. returns the qf distance between these Histograms
double QuadraticFormDistance::compareHistograms(const Histogram &p, const Histogram &q) const {
    BEGIN_TIMER(compareHistograms);
    double sumQFDist = 0.f;

    // Get the common keys from both p and q.
    set<string> commonKeys(Histogram::getCommonKeys(p, q));

    // Make copies of the original Histograms p and q.
    Histogram cutP = p;
    Histogram cutQ = q;

    // Retain only the keys that match the common keys.
    cutP.retainAll(commonKeys);
    cutQ.retainAll(commonKeys);
    
    // for all routers map <string, <dB, weight>>.. get map out of it
    for (map<string, map<int, float> >::const_iterator it = cutP.begin(); it != cutP.end(); ++it) {
        const string &transmitter = it->first;
        const map<int, float> &histP = it->second;

        const map<int, float> &histQ = cutQ.getHistogram().at(transmitter);
        const vector<int> sortedKeys = getAllSortedKeys(histP, histQ);

        if (sortedKeys.size() < 2) {
            continue;
        }

        SimpleMatrix groundDistanceMatrix = calculateGroundDistanceMatrix(histP, histQ, sortedKeys);
        SimpleMatrix similarityMatrix = calculateSimilarityMatrix(groundDistanceMatrix);

        // compare histograms for real
        sumQFDist += computeQFDistance(histP, histQ, similarityMatrix, sortedKeys);
    }

    if (cutP.getHistogram().size() > 0) {
        sumQFDist = sumQFDist / cutP.getHistogram().size();
    }

    END_TIMER(compareHistograms);
    return sumQFDist;
}

SimpleMatrix QuadraticFormDistance::calculateGroundDistanceMatrix(const map<int, float> &p, const map<int, float> &q, const vector<int> &allKeys) const {
    BEGIN_TIMER(calculateGroundDistanceMatrix);
    const size_t size = allKeys.size();
    SimpleMatrix groundDistanceMatrix(size);

    // calculate L1-metric for the dBs
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            double val = m_useLogScale ? abs(convertToLogScale(allKeys[i]) - convertToLogScale(allKeys[j])) : abs(allKeys[i] - allKeys[j]);
            groundDistanceMatrix.data[j * size + i] = val;
            if (val > groundDistanceMatrix.max) {
                groundDistanceMatrix.max = val;
            }
        }
    }
    END_TIMER(calculateGroundDistanceMatrix);
    return groundDistanceMatrix;
    
}

SimpleMatrix QuadraticFormDistance::calculateSimilarityMatrix(const SimpleMatrix &groundDistanceMatrix) const {
    BEGIN_TIMER(calculateSimilarityMatrix);
    // construct similarity matrix by means of ground distance matrix
    // formula: a_ij = 1 - d_ij/d_max
    const size_t size = groundDistanceMatrix.size;
    SimpleMatrix similarityMatrix(size);
    
    const double maxCoeff = groundDistanceMatrix.max;
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            const size_t offset = j * size + i;
            similarityMatrix.data[offset] = 1 - (groundDistanceMatrix.data[offset] / maxCoeff);
        }
    }
    END_TIMER(calculateSimilarityMatrix);
    return similarityMatrix;
}

double QuadraticFormDistance::computeQFDistance(const map<int, float> &p, const map<int, float> &q, const SimpleMatrix &similarityMatrix, const vector<int> &allKeys) const {
    BEGIN_TIMER(computeQFDistance);
    const size_t size = allKeys.size();

    Eigen::VectorXd distV = Eigen::VectorXd::Zero(size);
    for (size_t i = 0; i < size; i++) {
        int key = allKeys[i];
        float pVal = (p.find(key) != p.end()) ? p.at(key) : 0.f;
        float qVal = (q.find(key) != q.end()) ? q.at(key) : 0.f;

        distV(i) = pVal - qVal;
    }

    // create an Eigen matrix on top of the memory of our own SimpleMatrix so that we can use the nice
    // multiplication operators from Eigen
    Eigen::MatrixXd simMatrix = Eigen::MatrixXd::Map(similarityMatrix.data, similarityMatrix.size, similarityMatrix.size);

    // matrix multiplication formula: sqrt((p-q)T*A*(p-q))
    double qfDist = distV.transpose() * simMatrix * distV;
    qfDist = sqrt(qfDist);
    END_TIMER(computeQFDistance);
    return qfDist;
}

// from maps p and q, create a sorted vector of the union of all keys
vector<int> QuadraticFormDistance::getAllSortedKeys(const map<int, float> &p, const map<int, float> &q) const {
    BEGIN_TIMER(getAllSortedKeys);
    set<int> sortedKeys;
    
    for (map<int, float>::const_iterator it = p.begin(); it != p.end(); ++it) {
        sortedKeys.insert(it->first);
    }

    for (map<int, float>::const_iterator it2 = q.begin(); it2 != q.end(); ++it2) {
        sortedKeys.insert(it2->first);
    }

    vector<int> allKeys;
    allKeys.reserve(sortedKeys.size());
    for (set<int>::iterator it3 = sortedKeys.begin(); it3 != sortedKeys.end(); ++it3) {
        allKeys.push_back(*it3);
    }
    END_TIMER(getAllSortedKeys);
    return allKeys;
}

double QuadraticFormDistance::convertToLogScale(float db) const {
    double logVal = sqrt(pow(10.f, (db / 10.f)));

    return logVal;
}
