
#include "OccupancyGridMap.h"
#include "TrackerConfig.h"


#include <vector>
#include <iostream>
#include <queue>
#include <algorithm>
#include <cfloat>


using namespace std;

OccupancyGridMap::OccupancyGridMap(const vector<float>& pixelValues, int width, int height) {        
    init();
    initializeGridmap(pixelValues, width, height);
}

void OccupancyGridMap::init() {
    TrackerConfig* config = TrackerConfig::getInstance();
    m_ogmPixelBlack = config->getDouble(OGM_PIXEL_BLACK);
    m_ogmPixelWhite = config->getDouble(OGM_PIXEL_WHITE);
    m_ogmPixelGrey = config->getDouble(OGM_PIXEL_GREY);
}

void OccupancyGridMap::initializeGridmap(const vector<float>& pixelValues, int width, int height) {
    m_dim = V2i(width, height);
    m_resolution = { -1, -1 };

    m_accessiblePixels.clear();
    m_accessiblePixels.reserve(width*height);

    if ((width*height) != pixelValues.size()) {
        m_properlyInitialised = false;
    } else {
        m_properlyInitialised = true;

        m_gridMap.resize(m_dim.x);
        for (int i = 0; i < m_dim.x; i++) {
            m_gridMap[i].resize(m_dim.y);
        }

        // initialise gridmap with pixel values and accessiblePixels vector
        for (int x = 0; x < m_dim.x; x++) {
            for (int y = 0; y < m_dim.y; y++) {
                m_gridMap[x][y] =  pixelValues[x + m_dim.x * y];

                if (m_gridMap[x][y] != m_ogmPixelBlack) {
                    m_accessiblePixels.push_back(V2i(x,y));
                }
            }
        }
    }
}

const vector<V2i>& OccupancyGridMap::getAccessiblePixels() {
    random_shuffle(m_accessiblePixels.begin(), m_accessiblePixels.end());
    return m_accessiblePixels;
}

void OccupancyGridMap::setBoundingBox(GeoPoint min, GeoPoint max) {
    m_min = min;
    m_max = max;

    m_accessiblePositionsInPixel.clear();

    if (m_min.getLatitudeE6() < m_max.getLatitudeE6() && m_min.getLongitudeE6() < m_max.getLongitudeE6() && m_dim.x > 0 && m_dim.y > 0) { 

        m_resolution.x = (float)std::abs(m_min.getLongitudeE6() - m_max.getLongitudeE6()) / m_dim.x;
        m_resolution.y = (float)std::abs(m_min.getLatitudeE6() - m_max.getLatitudeE6()) / m_dim.y;

        int sizeX = m_resolution.x;
        int sizeY = m_resolution.y;

        // initialise accessiblePositionsInPixel vector
        // TODO: use row 0 / column 0 as well!
        // this currently doesn't work because the translation between lat/long 
        // and x/y coordinates is not reversible due to floating point truncation :(
        for (int x = 1; x < sizeX; x++) {
            for (int y = 1; y < sizeY; y++) {
                m_accessiblePositionsInPixel.push_back(V2i(x, y));
            }
        }
    }
}


float OccupancyGridMap::getGridValue(const GeoPoint& point) const {    
    // no proper map set or no bounding box set
    if (!ogmProperlyInitialised()) {
        // cannot calculate indices (i,j), therefore everything is free space for the particle filter
        return 1.f;
    } else {
        
        V2i pixel = getGridPosition(V2i(point.getLatitudeE6(), point.getLongitudeE6()));
        // positions outside of the bounding box have 0 as value by default
        // therefore it doesn't matter if (x,y) or (lat, long) are outside the bounding box

        return getGridValue(pixel);
    }
}

float OccupancyGridMap::getGridValue(V2i pixel) const { 
    if (pixel.x >= 0 && pixel.x < m_dim.x && pixel.y >= 0 && pixel.y < m_dim.y) {
        return m_gridMap[pixel.x][pixel.y]; // considers every obstacle in the map = grid map is enabled
    } else {
        return 0.f;
    }
}

bool OccupancyGridMap::isPixelAccessible(V2i pixel) const {
    return getGridValue(pixel) != m_ogmPixelBlack;
}

bool OccupancyGridMap::isPointAccessible(const GeoPoint& point) const {
    return getGridValue(point) != m_ogmPixelBlack;
}

vector<GeoPoint> OccupancyGridMap::gridPositionToGeoPoint(vector<GridPosition>& gridPosPath) const {
    // translates a planned path within the gridmap to a list of geopoints
    vector<GeoPoint> path;
    
    for (int i = 0; i < gridPosPath.size(); i++) {
        V2i latlon = getRealPosition(V2i(gridPosPath[i].x, gridPosPath[i].y));
        GeoPoint tmpPoint(latlon.latitude, latlon.longitude);
        path.push_back(tmpPoint);
    }
    
    return path;
}

bool OccupancyGridMap::ogmProperlyInitialised() const {
    // are pixel values and bounding box there? ready to use?
    if ( (!m_properlyInitialised) || m_dim.x == 0 || m_dim.y == 0 || m_resolution.x == -1 || m_resolution.y == -1) {
        return false;
    } else {
        return true;
    }
}

float OccupancyGridMap::nextRandom() {
    uniform_real_distribution<float> distribution(0.f, 1.f);
    
    return distribution(m_generator);
}

// This method can only be call after calling setBoundingBox
V2i OccupancyGridMap::getRandomDistributedPositionInPixel(V2i pixel) const {

    // Get the first position and add the selected increment in pixel to the top-left position in pixel
    int r = rand() % m_accessiblePositionsInPixel.size();

    V2i xyOffset = m_accessiblePositionsInPixel[r];

    // TODO: normalize subpixels coordinates
    V2i result;
    result.longitude = pixel.x * m_resolution.x + xyOffset.x + m_min.getLongitudeE6();
    result.latitude = pixel.y * m_resolution.y + xyOffset.y + m_min.getLatitudeE6();
    return result;
}

// position in real coordinates (lat,long) to grid coordinates
// NB this returns the top-left coordinate of the pixel
V2i OccupancyGridMap::getGridPosition(V2i latlon) const {
    // minus bounding box min
    V2i pos;
    pos.x = latlon.longitude - m_min.getLongitudeE6();
    pos.x /= m_resolution.x;

    pos.y = latlon.latitude - m_min.getLatitudeE6();
    pos.y /= m_resolution.y;

    return pos;
}

V2i OccupancyGridMap::getGridPosition(const GeoPoint& geoPoint) const {
    return getGridPosition(V2i(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()));
}

// position in grid coordinates to real coordinates
// NB this returns the center coordinate of the pixel
V2i OccupancyGridMap::getRealPosition(V2i pixel) const {
    V2i result;
    result.latitude = pixel.y * m_resolution.y + m_resolution.y/2.0 + m_min.getLatitudeE6();
    result.longitude = pixel.x * m_resolution.x + m_resolution.x/2.0 + m_min.getLongitudeE6();
    return result;
}

// ---- path planning methods ----

vector<GeoPoint> OccupancyGridMap::planPath(GeoPoint startPoint, GeoPoint goalPoint) const {    
    
    vector<GeoPoint> plannedPath;

    // return empty vector if no bounding box or proper grid map is there, or if the goalPoint
    // is in inaccessible space
    if (!ogmProperlyInitialised() || getGridValue(goalPoint) == m_ogmPixelBlack) {
        return plannedPath;
    }
    
    // initialise and reset helper gridmaps
    vector<vector<int> > closed;
    vector<vector<float> > scores;
    
    closed.resize(m_dim.x);
    scores.resize(m_dim.x);
    for (int i = 0; i < m_dim.x; i++) {
        closed[i].resize(m_dim.y);
        scores[i].resize(m_dim.y);
    }
    
    for (int i = 0; i < m_dim.x; i++) {
        for (int j = 0; j < m_dim.y; j++) {
            closed[i][j] = 0;
            scores[i][j] = 0;
        }
    }
    

    // A* algorithm
    // init start and goal
    V2i startXY = getGridPosition(V2i(startPoint.getLatitudeE6(), startPoint.getLongitudeE6()));
    GridPosition start;
    start.x = startXY.x;
    start.y = startXY.y;
    start.cost = 0;
    start.score = 0;

    V2i goalXY = getGridPosition(V2i(goalPoint.getLatitudeE6(), goalPoint.getLongitudeE6()));
    GridPosition goal;
    goal.x = goalXY.x;
    goal.y = goalXY.y;
    goal.cost = -1;
    goal.score = -1;
    
    // test if start and goal point are within grid
    if ((start.x >= 0 && start.x < m_dim.x && start.y >= 0 && start.y < m_dim.y) && (goal.x >= 0 && goal.x < m_dim.x && goal.y >= 0 && goal.y < m_dim.y)) {
        priority_queue<GridPosition> priorityqueue;
        vector<GridPosition> visited;
        vector<GridPosition> path;

        GridPosition currPos;
        GridPosition tmpPos;
        GridPosition pathPos;

        bool reachedGoal = false;
        bool reachedStart = false;

        priorityqueue.push(start);

        while (!priorityqueue.empty()) {
            currPos = priorityqueue.top();
            priorityqueue.pop();
            visited.push_back(currPos) ;

            if (currPos.x == goal.x && currPos.y == goal.y) {
                reachedGoal = true;
                break;
            }

            // expand node
            for (int i = (currPos.x - 1); i <= (currPos.x + 1); i++) {
                for (int j = (currPos.y - 1); j <= (currPos.y + 1); j++) {

                    if (i < 0 || i >= m_dim.x || j < 0 || j >= m_dim.y || (i == currPos.x && j == currPos.y)) {
                        continue;
                    }

                    if (closed[i][j] == 1) {
                        continue;
                    }

                    //In case the pixel is not accesible we do not take it. But still we use m_gridMap[i][j] to comute the score for gray pixels
                    if (m_gridMap[i][j] == m_ogmPixelBlack) {
                        continue;
                    }

                    tmpPos.x = i;
                    tmpPos.y = j;

                    // calculate cost
                    float dist = sqrt((currPos.x - tmpPos.x) * (currPos.x - tmpPos.x) + (currPos.y - tmpPos.y) * (currPos.y - tmpPos.y));
                    float cost = currPos.cost + dist + (1 - m_gridMap[i][j]) * 20.f; // random high value
                    tmpPos.cost = cost;

                    // calculate heuristic
                    float heuristic = sqrt((goal.x - tmpPos.x) * (goal.x - tmpPos.x) + (goal.y - tmpPos.y) * (goal.y - tmpPos.y));
                    float score = cost + heuristic;

                    if ((scores[i][j] != 0) && (scores[i][j] <= score)) {
                        continue;
                    }

                    // parent node
                    tmpPos.px = currPos.x;
                    tmpPos.py = currPos.y;

                    scores[i][j] = score;
                    tmpPos.score = score;
                    priorityqueue.push(tmpPos);
                }
            }
            closed[currPos.x][currPos.y] = 1;
        }

        // backtrack path
        if (reachedGoal && !visited.empty()) {  
            pathPos = visited.back();
            visited.pop_back();
            path.push_back(pathPos);

            while (!reachedStart) {         
                float score = FLT_MAX;
                for (unsigned int i = 0; i < visited.size(); i++) {     
                    if (visited[i].x == pathPos.px && visited[i].y == pathPos.py) {
                        if (visited[i].score < score) {
                            score = visited[i].score;
                            pathPos = visited[i];
                        }
                    }
                }

                path.push_back(pathPos);
                if (pathPos.x == start.x && pathPos.y == start.y) {
                    reachedStart = true;
                    break;
                }
            }
        }

        std::reverse(path.begin(), path.end());
        plannedPath = gridPositionToGeoPoint(path);
    } else {
        cerr << "Start " << start.x << " " << start.y << " or goal " << goal.x << " " << goal.y << " is not within the gridmap." << endl;
    }
    
    
    return plannedPath;
}

float OccupancyGridMap::getPathLength(vector<GeoPoint> path) const {
    float dist = 0;
    for (size_t i = 1; i < path.size(); i++) {
        dist += path[i-1].calculateEuclideanDistanceTo(path[i]);
    }
    return dist;
}

vector<GeoPoint> OccupancyGridMap::planShortestPath(GeoPoint start, vector<GeoPoint> goals) const {
    // start is set, final goal is the last entry of goals => goals[goals.size()-1], remaining waypoints are shuffled regarding the shortest path
    // approximate solution for TSP: NN with A*
    vector<GeoPoint> finalPath;

    // return empty path if no bounding box or proper grid map is set
    if (!ogmProperlyInitialised()) {
        return finalPath;
    }


    vector< vector<GeoPoint> > paths;

    priority_queue<Path> pathsInIteration;
    GeoPoint curStart = start;
    size_t numIt = goals.size()-2;

    for (size_t i = 0; i < numIt; i++) { // start, goal and the last waypoint before goal don't need an iteration
        Path tmpPath;
        for (size_t j = 0; j < goals.size()-1; j++) {
            tmpPath.goalIndex = j;
            tmpPath.waypoints = planPath(curStart, goals[j]);
            tmpPath.length = getPathLength(tmpPath.waypoints);

            pathsInIteration.push(tmpPath);
        }

        tmpPath = pathsInIteration.top();
        pathsInIteration = priority_queue<Path>();
        // delete last waypoint since it double in our final path
        tmpPath.waypoints.pop_back();
        paths.push_back(tmpPath.waypoints);

        curStart = goals[tmpPath.goalIndex];
        goals.erase(goals.begin()+tmpPath.goalIndex);
    }

    // add paths to last point and goal
    // delete last waypoint since it double in our final path
    vector<GeoPoint> res = planPath(curStart, goals[0]);
    // TODO (gereon): handle the case when res is empty?
    res.pop_back();
    paths.push_back(res);
    paths.push_back(planPath(goals[0], goals[1]));

    // copy paths to one vector
    for (size_t k = 0; k < paths.size(); k ++) {
        for (size_t l = 0; l < paths[k].size(); l++) {
            finalPath.push_back(paths[k][l]);
        }
    }
    
    return finalPath;
}

