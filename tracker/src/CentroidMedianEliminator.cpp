#include "CentroidMedianEliminator.h"

#include <algorithm>
#include <iostream>

using namespace std;

CentroidMedianEliminator::CentroidMedianEliminator(float medianThresholdFactor) {
    m_medianThresholdFactor = medianThresholdFactor; 
}

vector<GeoPoint> CentroidMedianEliminator::removeOutliers(const vector<GeoPoint>& candidateSet) {
    // Eliminator not applicable on two or less candidates.
    if (candidateSet.size() <= 2) {
        return candidateSet;
    }
    
    // 1. Get the centroid.
    // The centroid is the virtual center coordinate from a given amount of coordinates. It is composed of the
    // independent median coordinates for each dimension.
    GeoPoint centroid = getMedianPoint(candidateSet);

    // 2. Calculate the median of the distances between each candidate and the centroid.
    double medianCentroidDistance = calculateMedianReferencePointDistance(candidateSet, centroid);

    // 3. Eliminate all coordinates further then the threshold distance.
    double thresholdDistance = m_medianThresholdFactor * medianCentroidDistance;
    
    vector<GeoPoint> withoutOutliers;
    withoutOutliers.reserve(candidateSet.size());
    for (size_t i = 0; i < candidateSet.size(); i++) {
        if (candidateSet[i].calculateEuclideanDistanceTo(centroid) <= thresholdDistance) {
            withoutOutliers.push_back(candidateSet[i]);
        }
    }
    
    if (candidateSet.size() != withoutOutliers.size()) {
        cerr << "CentroidMedianEliminator: size-in = " << candidateSet.size() << ", size-out = " << withoutOutliers.size() << endl;
    }

    return withoutOutliers;
}

GeoPoint CentroidMedianEliminator::getMedianPoint(const vector<GeoPoint>& candidateSet) {

    vector<double> xValues;
    vector<double> yValues;

    for (size_t i = 0; i < candidateSet.size(); i++) {
        xValues.push_back(candidateSet[i].getX());
        yValues.push_back(candidateSet[i].getY());
    }
    
    sort(xValues.begin(), xValues.end());
    sort(yValues.begin(), yValues.end());
    
    float x = getMedianFromSortedList(xValues);
    float y = getMedianFromSortedList(yValues);

    return GeoPoint::makePoint(x, y);
}

double CentroidMedianEliminator::getMedianFromSortedList(const vector<double>& list) {
    // The size, integer-divided by 2, is the:
    // a) single center element index in an odd list size
    // b) upper center element index for an even list size
    size_t middleIndex = list.size() / 2;
    double median;

    if (list.size() % 2 == 1) {
        // odd list size
        median = list[middleIndex];
    } else {
        // even list size
        median = 0.5f * (list[middleIndex - 1] + list[middleIndex]);
    }
    return median;
}

double CentroidMedianEliminator::calculateMedianReferencePointDistance(const vector<GeoPoint>& candidateSet, const GeoPoint& referencePoint) {

    vector<double> referencePointDistances;

    // for each element get the distance to the reference point
    for (size_t i = 0; i < candidateSet.size(); i++) {
        referencePointDistances.push_back(candidateSet[i].calculateEuclideanDistanceTo(referencePoint));
    }

    sort(referencePointDistances.begin(), referencePointDistances.end());
    
    // the median reference point distance is the median of the distances between
    // each element and the reference point
    double medianReferencePointDistance = getMedianFromSortedList(referencePointDistances);

    return medianReferencePointDistance;
}