#ifndef ROUTERLEVELHISTOGRAM_H
#define	ROUTERLEVELHISTOGRAM_H

#include <string>
#include <map>
#include <algorithm>

#include "Histogram.h"

// TODO: this code is unused, remove it

class RouterLevelHistogram {
public:
    RouterLevelHistogram();
    ~RouterLevelHistogram();
    
    void setRouterLevelHistogram(std::map<std::string, float> rlh) {
        m_rlh = rlh;
    }
    
    std::map<std::string, float> getRouterLevelHistogram() {
        return m_rlh;
    }
    
    int getSize() {
        return m_rlh.size();
    }
   
    
    int intersect(RouterLevelHistogram rlh);
    void makeMedianHistogram(Histogram histogram);
    void makeAverageHistogram(Histogram histogram);
    std::map<std::string, float> normalize();
    
private:
    std::map<std::string, float> m_rlh;
    
    float calculateMedianLevel(std::map<int, float> levels);
    float calculateWeightedAverageLevel(std::map<int, float> levels);

};

#endif	/* ROUTERLEVELHISTOGRAM_H */

