#ifndef OCCUPANCYGRIDMAP_H
#define OCCUPANCYGRIDMAP_H

#include <vector>
#include <random>
#include <cstdlib>
#include <boost/math/distributions/normal.hpp>
#include <iostream>

#include "GeoPoint.h"
#include "V2i.h"
#include "V2d.h"

/**
 * 2-dimensional gridmap with grayscale values in it representing obstacles. Used for path planning and shortest path
 * calculation. Resolution of the map should be 0.5m, so the width and height of the map image have to be chosen
 * accordingly:
 * <p/>
 * See: https://evolvis.org/plugins/mediawiki/wiki/nic/index.php/Partikel_Filter#Creating_the_occupancy_grid_map
 */
class OccupancyGridMap {
public:
    struct GridPosition {
        int x, y;
        int px, py; // parent coordinates
        float cost;
        float score;
        
        // operator for priority queue order
        bool operator<( const GridPosition &a ) const {
            return score > a.score;
        }
    };
    
    struct Path {
        int goalIndex;
        std::vector<GeoPoint> waypoints;
        float length;
        
        // operator for sort
        bool operator<( const Path &a ) const {
            return length > a.length;
        }
    };
    
    OccupancyGridMap(const std::vector<float>& pixelValues = std::vector<float>(), int width = 0, int height = 0);
    
    void setBoundingBox(GeoPoint min, GeoPoint max);

    std::vector<GeoPoint> planPath(GeoPoint startPoint, GeoPoint goalPoint) const;
    std::vector<GeoPoint> planShortestPath(GeoPoint start, std::vector<GeoPoint> goals) const;

    V2i getRandomDistributedPositionInPixel(V2i pixel) const;
    
    // position in real coordinates (lat,long) to grid coordinates
    V2i getGridPosition(V2i latlon) const;
    V2i getGridPosition(const GeoPoint& geoPoint) const;
    
    // position in grid coordinates to real coordinates (lat, long)
    V2i getRealPosition(V2i pixel) const; 

    float getGridValue(const GeoPoint& point) const;
    float getGridValue(V2i pixel) const;

    bool isPixelAccessible(V2i pixel) const;
    bool isPointAccessible(const GeoPoint& point) const;
    
    V2d getResolution() const {
        return m_resolution;
    }

    std::vector<std::vector<float> > getGridMap() const {
        return m_gridMap;
    }

    // get the accessible pixels of the grid map. 
    // NB: This vector is shuffled in-place each time this method is called!
    const std::vector<V2i>& getAccessiblePixels();
    
    std::vector<GeoPoint> gridPositionToGeoPoint(std::vector<GridPosition>& gridPosPath) const;

    bool ogmProperlyInitialised() const;
    
private:
    float m_ogmPixelBlack;
    float m_ogmPixelWhite;
    float m_ogmPixelGrey;

    bool m_properlyInitialised;
    std::vector<V2i> m_accessiblePixels;
    std::vector<V2i> m_accessiblePositionsInPixel;
    std::vector<std::vector<float> > m_gridMap;
    
    V2d m_resolution;
    V2i m_dim;              // number of cells
    GeoPoint m_min, m_max;  // bounding box

    std::default_random_engine m_generator;
        
    float getPathLength(std::vector<GeoPoint> path) const;
    void init();
    void initializeGridmap(const std::vector<float>& pixelValues, int width, int height);
    virtual float nextRandom();
    
};

#endif  /* OCCUPANCYGRIDMAP_H */
