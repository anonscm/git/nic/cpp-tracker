#ifndef KULLBACKLEIBLER_H
#define	KULLBACKLEIBLER_H

// TODO: this code is unused, remove it

#include "RouterLevelHistogram.h"

class KullbackLeibler {
public:
    KullbackLeibler();
    ~KullbackLeibler();
    
    float compareHistograms(RouterLevelHistogram p, RouterLevelHistogram q) ;
    
    float getDivergence() {
        return m_symmetricalDivergence;
    }
    
    float getConfidence() {
        return m_confidence;
    }
    
private:
    float m_symmetricalDivergence;
    float m_confidence;
    
    float calculateDirectionalDivergence(RouterLevelHistogram p, RouterLevelHistogram q);
    
};

#endif	/* KULLBACKLEIBLER_H */

