#include "ParticleFilter.h"
#include "TrackerConfig.h"

#include <iostream>

using namespace std;

ParticleFilter::ParticleFilter() {
    m_scale = 0;
    m_gridMap = nullptr;

    TrackerConfig* config = TrackerConfig::getInstance();

    m_numParticles = config->getInt(NUM_PARTICLES);
    double pctRandom = config->getDouble(PCT_RANDOM_PARTICLES);
    m_numRandomParticles = m_numParticles * pctRandom / 100.0;
    m_sigmaInit = config->getInt(SIGMA_INIT);
    m_sigmaAction = config->getInt(SIGMA_ACTION);
    m_sigmaSensor = config->getInt(SIGMA_SENSOR);
    m_sigmaRandomParticles = config->getInt(SIGMA_RANDOM_PARTICLES);
    m_clusterRadius = config->getDouble(CLUSTER_RADIUS);
}

void ParticleFilter::initialize(float curX, float curY) {
    const vector<V2i> &allAccessiblePixels = m_gridMap->getAccessiblePixels();
    int allAccessiblePixelsSize = allAccessiblePixels.size();
    const float importanceWeight = 1.0 / m_numParticles;

    if (allAccessiblePixelsSize != 0) {

        for (int i = 0; i < m_numParticles; i++) {

            V2i position = m_gridMap->getRandomDistributedPositionInPixel(allAccessiblePixels[i%allAccessiblePixelsSize]);
            GeoPoint curPos(position.latitude, position.longitude);
            Particle tmp(curPos.getX(), curPos.getY(), importanceWeight);

            m_particles.push_back(tmp);
        }
    } else { //allAccessiblePixels.size() == 0. Hardly will enter here
        for (int i = 0; i < m_numParticles; i++) {
            // take the first wifi position and add gaussian noise to spread the particles around the initial position
            // all particles have the same uniform weight at the beginning

            float rnd1 = nextGaussian() * m_sigmaInit * m_scale;
            float rnd2 = nextGaussian() * m_sigmaInit * m_scale;
            float posX = curX + rnd1;
            float posY = curY + rnd2;
            GeoPoint newPos = GeoPoint::makePoint(posX, posY);

            // consider obstacles from the occupancy gridmap
            // if no grid map is set, everything is free space
            float newX = curX + rnd1 * m_gridMap->getGridValue(newPos);
            float newY = curY + rnd2 * m_gridMap->getGridValue(newPos);

            Particle tmp(newX, newY, importanceWeight);

            m_particles.push_back(tmp);
        }
    }
}

// Overwrite some of our particles with random positions.
// Of course m_numRandomParticles must be <= m_particles.size().
void ParticleFilter::addRandomParticles(float centerX, float centerY) {
    const float importanceWeight = 1.0 / m_particles.size();

    const vector<V2i> &allAccessiblePixels = m_gridMap->getAccessiblePixels();
    int allAccessiblePixelsSize = allAccessiblePixels.size();

    random_shuffle ( m_particles.begin(), m_particles.end() );

    if (allAccessiblePixelsSize != 0) {

        for (int i = 0; i < m_numRandomParticles; i++) {

            V2i position = m_gridMap->getRandomDistributedPositionInPixel(allAccessiblePixels[i%allAccessiblePixelsSize]);
            GeoPoint curPos(position.latitude, position.longitude);

            m_particles[i].x = curPos.getX();
            m_particles[i].y = curPos.getY();
            m_particles[i].importanceWeight = importanceWeight;
        }
    } else { //allAccessiblePixels.size() == 0. Hardly will enter here
        const float sigmaScaleFactor = m_sigmaRandomParticles * m_scale;
        for (int i = 0; i < m_numRandomParticles; i++) {

            // Find a random position
            float x = centerX + nextGaussian() * sigmaScaleFactor;
            float y = centerY + nextGaussian() * sigmaScaleFactor;

            m_particles[i].x = x;
            m_particles[i].y = y;
            m_particles[i].importanceWeight = importanceWeight;
        }
    }
}



void ParticleFilter::updateWithDeadReckoning(float dx, float dy) {
    // propagate position through motion model
    const float sigmaScale = m_sigmaAction * m_scale;

    GeoPoint newPos;
    for (unsigned int i=0; i < m_particles.size(); i++) {
		float valX = nextGaussian() * sigmaScale;
		float valY = nextGaussian() * sigmaScale;
		
        float newX = m_particles[i].x + dx + valX;
        float newY = m_particles[i].y + dy + valY;
        newPos.setXY(newX, newY);

        // consider obstacles from the occupancy gridmap
        m_particles[i].x += (dx + valX) * m_gridMap->getGridValue(newPos);
        m_particles[i].y += (dy + valY) * m_gridMap->getGridValue(newPos);
    }
}

void ParticleFilter::updateWithWifiPosition(float x, float y) {
    float normalize = 0.f;

    // determine the distance between each particle and the measured wifi-position
    for (unsigned int i = 0; i < m_particles.size(); i++) {
        float sqrX = powf(x - m_particles[i].x, 2.f);
        float sqrY = powf(y - m_particles[i].y, 2.f);
        float sqrtval = sqrt(sqrX + sqrY);
        // avoid division by zero
        if (sqrtval == 0.f) {
            sqrtval = 0.000001f;
        }

        // take inverse distance as weight, so near particles have a higher weight
        float weight = 1.f/sqrtval;

        m_particles[i].importanceWeight = weight;

        normalize += weight;
    }
    // normalize the weights , as they represent a probability for each hypothesis being true
    normalizeWeights(normalize);
}

void ParticleFilter::normalizeWeights(float norm) {
    for (unsigned int i = 0; i < m_particles.size(); i++) {
        m_particles[i].importanceWeight = m_particles[i].importanceWeight/norm;
    }
}

void ParticleFilter::resample() {
    vector<Particle> tmpParticles;

    // Generate cumulative distribution function from particle weights,
    // so that cdf[n] equals the sum of the weights of the first n particles.
    vector<float> cdf;
    cdf.reserve(m_numParticles);
    cdf[0] = m_particles[0].importanceWeight;
    for (int i = 1; i < m_numParticles; i++) {
        cdf[i] = cdf[i-1] + m_particles[i].importanceWeight;
    }

    // Sample a random value between 0 and 1/numParticles.
    // This is the starting pointer into the cdf-space, i.e. a random point
    // inside the first of numParticles, equally sized, partitions of the cdf.
    // We will then take a sample from each of these partitions.
    // TODO (rob): why is this a vector? Isn't a simple float enough? We don't
    //             reuse values below our current index anyway.
    vector<float> u;
    u.reserve(m_numParticles+1);
    do {
        u[0] = nextRandom() * 1.f/m_numParticles;
    }
    while(!(u[0] > 0)); // Note that this is not a "real loop", but only to guard against 0 as a random number.

    int particleNumber = 0;

    // Draw samples from the cumulative distribution function with a fixed interval of 1/numParticles.
    for (int j = 0; j < m_numParticles; j++) {
        // Skip all the particles which come before our current checkpoint,
        // possibly skipping particles with a small weight.
        while (u[j] > cdf[particleNumber]) {
            particleNumber++;
        }

        // Add noise to the chosen particle.
        // TODO: check that particle position is in free space. If not, then find the best & closest
        //       free space position to the calcultated particle position
        Particle tmp(
            m_particles[particleNumber].x + nextGaussian() * m_sigmaSensor * m_scale,
            m_particles[particleNumber].y + nextGaussian() * m_sigmaSensor * m_scale,
            1.f/m_numParticles);
        // debug stuff: cout << "scale("<<m_scale<<"), sigma("<<m_sigmaSensor<<"), rnd("<<absGaussSum/count<<"), displacement: " << m_particles[particleNumber].x - tmp.x << ", " << m_particles[particleNumber].y - tmp.y << endl;
        tmpParticles.push_back(tmp);

        // Advance the pointer into the cdf:
        u[j+1] = u[j] + 1.f/m_numParticles;
    }

    // Set new resampled particle set:
    m_particles = tmpParticles;
}


// TODO (rob): this seems to be unnecessarily complex for simple particle noise.
float ParticleFilter::nextGaussian() {
    normal_distribution<double> distribution(0.f, 1.f);

    return distribution(m_generator);
}


float ParticleFilter::nextRandom() {
    uniform_real_distribution<float> distribution(0.f, 1.f);

    return distribution(m_generator);
}

GeoPoint ParticleFilter::calculatePosition() {
    float meanX = 0.f;
    float meanY = 0.f;

    vector<Particle> biggestCluster;
    if (!m_particles.empty()) {
        biggestCluster = getBiggestCluster();
    }

    // assumption: unimodal distribution, therefore approximate position with a gaussian and take mean
    for (unsigned int i = 0; i < biggestCluster.size(); i++) {
        meanX += biggestCluster[i].x;
        meanY += biggestCluster[i].y;
    }

    //TODO: check that this position is in free space. If not, then find the best & closest free space position to the calcultated position
    GeoPoint currentPosition = GeoPoint::makePoint(meanX/biggestCluster.size(), meanY/biggestCluster.size());

    return currentPosition;
}

// All particles that are within distance m_clusterRadius of any other
// particle are chained together into one cluster.
// Weaknesses: - long chains of particles, that are not really clusters at all, are considered clusters.
//             - two clusters can be connected by "bridge particles", making them one cluster.
//TODO: Improve the algorithm using an euclidean-distance matrix or whatever seems appropriate.
vector<Particle> ParticleFilter::getBiggestCluster() {
    float sqrX, sqrY, dist; // To compute euclidean distance

    vector< vector<Particle> > clusters;    // Contains all the clusters
    vector<Particle> cluster;               // To generate each cluster

    // no particles?
    if (m_particles.size() == 0) {
        return cluster;
    }

    vector<Particle> tmpParticles(m_particles);
    cluster.reserve(m_particles.size());
    cluster.push_back(tmpParticles[0]); // Assign the first particle to the first cluster.
    tmpParticles.erase(tmpParticles.begin());

    // no particles left?
    if (tmpParticles.size() == 0) {
        return cluster;
    }

    while (tmpParticles.size() > 0) {
        // For each possible cluster will try to insert the corresponding particles.
        for (int i = 0; i < cluster.size(); i++) {
            if (tmpParticles.size() == 0)
                break;

            for (int j = 0; j < tmpParticles.size(); j++) {
                sqrX = (cluster[i].x - tmpParticles[j].x) * (cluster[i].x - tmpParticles[j].x);
                sqrY = (cluster[i].y - tmpParticles[j].y) * (cluster[i].y - tmpParticles[j].y);
                dist = sqrt(sqrX + sqrY);

                if (dist < m_clusterRadius) {
                    // The new particle is close enough to the last particle in the cluster => insert the new particle into the cluster.
                    cluster.push_back(tmpParticles[j]);
                    tmpParticles.erase(tmpParticles.begin()+j);
                    j--; // We want to check j again because the erasure shifted the subsequent particles one step down.
                }
            }
        }

        clusters.push_back(cluster);
        cluster.clear();
        if (tmpParticles.size() != 0) {
            cluster.push_back(tmpParticles[0]);
            tmpParticles.erase(tmpParticles.begin());
        }
    }

    int maxParticles = 0;
    int bestIndex = 0;
    for (int i = 0; i < clusters.size(); i++) {
        if (clusters[i].size() > maxParticles) {
            maxParticles = clusters[i].size();
            bestIndex = i;
        }
    }

    return clusters[bestIndex];
}

/*
This is a work in progress snapshot of a different cluster finding algorithm.

vector<Particle> ParticleFilter::getBiggestCluster() {
    vector<int> clusterIndexes;
    clusterIndexes.reserve(m_particles.size());
    vector<Particle> bestCluster;

    //initialize the clusters
    for(int i=0; i<m_particles.size(); i++) {
        clusterIndexes[i] = -1;
    }


    //Get an upper triangle euclidean-distance matrix
    MatrixXd distanceMatrix = Eigen::MatrixXd::Zero(m_particles.size(), m_particles.size());

    for(int i=0; i<m_particles.size(); i++) {
        for(int j=i; j<m_particles.size(); j++) {
            if(i != j) {
                //If they are different particles then calculate the euclidean distance
                double dx = m_particles[i].x - m_particles[j].x;
                double dy = m_particles[i].y - m_particles[j].y;
                distanceMatrix(i, j) = sqrt((dx * dx) + (dy * dy));
            }
    }

    int numClusters = 1;

    //For the first, create the first cluster
    clusterIndexes[0] = 1;

    //Generate cluster indexes
    for(int i=0; i<m_particles.size(); i++) {

        //If i has not assigned a cluster, then assign numClusters++

        for(int j=i; j<m_particles.size(); j++) {
            if(i != j) {
                //If it is the first, create the first
                if(i == 0) {
                    clusterIndexes[i] = 1;
                    numClusters++;
                }

                //If the distance is smaller then asign the cluster too
                if(distanceMatrix(i, j) < m_clusterRadius) {
                    clusterIndexes(j) = numClusters;
                }
            }
        }
    }


    //Generate the bestCluster from clusterIndexes
    return bestCluster;
}
*/
