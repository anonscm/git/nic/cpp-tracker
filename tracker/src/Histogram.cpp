#include "Histogram.h"

using namespace std;

typedef map<string, map<int, float> >::const_iterator constIteratorType;

Histogram::Histogram() {
}

Histogram::Histogram(string id) {
    m_id = id;
}

set<string> Histogram::getCommonKeys(const Histogram &histogram1, const Histogram &histogram2) {
    // erases all APs/iBeacons from histogram1 which are not in histogram2
    // used to get the MAC addresses/iBeacon ids which are in both histograms
    
    set<string> result;
    // result.reserve(histogram1.size() + histogram2.size());

    for (constIteratorType iterator = histogram1.begin(); iterator != histogram1.end(); ++iterator) {
        const string& str = iterator->first;
        if (histogram2.find(str) != histogram2.end()) {
            result.insert(str);
        }
    }

    return result;
}

void Histogram::retainAll(const set<string> &toRetain) {
    // erases all APs/iBeacons from this particular instance which are not in the toRetain name list
    // has to be done for both histograms to get the intersection for same MAC addresses/iBeacon ids with different values

    for (constIteratorType iterator = m_histogram.begin(); iterator != m_histogram.end();) {
        if (toRetain.find(iterator->first) == toRetain.end()) {
            m_histogram.erase(iterator++);
        } else {
            ++iterator;
        }
    }
}
