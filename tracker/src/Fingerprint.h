#ifndef FINGERPRINT_H_
#define FINGERPRINT_H_

#include <string>

#include "Histogram.h"
#include "GeoPoint.h"

/**
 * Translation from fingerprint json to respective data structure
 */
class Fingerprint {
public:
	Fingerprint();
	Fingerprint(Histogram histogram, GeoPoint geoPoint);

	inline const Histogram &getHistogram() const {
		return m_histogram;
	}

	inline const GeoPoint &getPoint() const {
		return m_geoPoint;
	}

	inline const std::string &getId() const {
		return m_id;
	}

private:

	Histogram m_histogram;
	GeoPoint m_geoPoint;
	std::string m_id;

};

#endif /* FINGERPRINT_H_ */
