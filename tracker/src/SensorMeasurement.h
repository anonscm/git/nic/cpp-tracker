#include <stdint.h>

#ifndef SENSORMEASUREMENT_H
#define SENSORMEASUREMENT_H

struct SensorMeasurement {
    uint64_t timestamp;
    float x, y, z;        
    
    SensorMeasurement() {
         timestamp = 0;
         x = 0.f;
         y = 0.f;
         z = 0.f;
    };
    
    SensorMeasurement(float xVal, float yVal, float zVal) {
        timestamp = 0;
        x = xVal;
        y = yVal;
        z = zVal;
    }
};

typedef enum { ACCELEROMETER_DATA, MAGNETOMETER_DATA, LINEAR_ACCELERATION_DATA } SensorType;

#endif /* SENSORMEASUREMENT_H */