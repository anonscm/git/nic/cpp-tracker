#ifndef DEBUG_TIMER_H_
#define DEBUG_TIMER_H_

#ifdef DEBUG_TIMER

#include "x86intrin.h"
#include <sys/time.h>

enum {
	DebugTimer_calculateDistances,
	DebugTimer_compareHistograms,
	DebugTimer_calculateGroundDistanceMatrix,
	DebugTimer_calculateSimilarityMatrix,
	DebugTimer_computeQFDistance,
	DebugTimer_getAllSortedKeys,

	DebugTimer_Count
};

typedef struct {
	uint64_t cycles;
	double seconds;
	uint32_t hits;
} DebugTimer;

extern DebugTimer DebugTimers[];

#define BEGIN_TIMER(ID)	struct timeval _tv_##ID; \
						gettimeofday(&_tv_##ID,0); \
						double _starttime_##ID = _tv_##ID.tv_sec + (_tv_##ID.tv_usec / 1000000.0); \
						uint64_t _startcycles_##ID = __rdtsc();


#define END_TIMER(ID)	do { \
						DebugTimers[DebugTimer_##ID].cycles += __rdtsc() - _startcycles_##ID; \
						++DebugTimers[DebugTimer_##ID].hits; \
						gettimeofday(&_tv_##ID, 0); \
						DebugTimers[DebugTimer_##ID].seconds += (_tv_##ID.tv_sec + (_tv_##ID.tv_usec / 1000000.0)) - _starttime_##ID; \
						} while(0)

#else

#define BEGIN_TIMER(ID)		// nop
#define END_TIMER(ID)		// nop

#endif // DEBUG_TIMER

#endif // DEBUG_TIMER_H_