#ifndef _UTILS_H__
#define _UTILS_H__

class Utils {
public:

    /**
     * Gets the greyscale value from RGB.
     *
     * @param in rgb value in range [0, 255]
     * @return greyscale value in range [0, 255]
     */
    static unsigned char greyscale(unsigned char r, unsigned char g, unsigned char b);

    /**
     * Gets the normalized greyscale value from RGB.
     *
     * @param in rgb value in 0-255 range
     * @return normalized float in [0, 1] range for the greyscale
     */
    static float greyscaleNormalized(unsigned char r, unsigned char g, unsigned char b);
};

#endif /* _UTILS_H__ */
