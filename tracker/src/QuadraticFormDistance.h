#ifndef QUADRATICFORMDISTANCE_H_
#define QUADRATICFORMDISTANCE_H_

#include <vector>
#include <map>
#include <cfloat>

#include "Histogram.h"

// replacement class for the Eigen MatrixXd that was used previously - we know that in
// our case, we will only ever use square matrixes of doubles, and can implement them
// much more efficiently.
struct SimpleMatrix
{
    double max;
    size_t size;
    double *data;

    SimpleMatrix(size_t sz) { 
        size = sz;
        max = DBL_MIN;
        data = this->s_data;
        s_data[0] = 0;      // avoids sonar warning
        h_data = 0;
        if (sz > MAX_STACK_SIZE)
        {
            // use heap instead of stack
            h_data = new double[sz*sz];
            data = h_data;
        }
    }

    ~SimpleMatrix() {
        if (h_data) {
            delete[] h_data;
        }
    }

private:
    static const int MAX_STACK_SIZE = 32;
    double s_data[MAX_STACK_SIZE*MAX_STACK_SIZE];   // for small matrices: use memory on the stack
    double *h_data;                                 // for larger matrices: allocate 
};

/**
 * Cross-bin comparison measure for two histograms
 */
class QuadraticFormDistance {

public:
    QuadraticFormDistance();

    // compare two Histograms, returns the quadratic form distance
    double compareHistograms(const Histogram &p, const Histogram &q) const;
    
    // TODO: is this used anywhere? - if not, remove it and make all methods of this class static
    inline void setUseLogScale(bool val) {
        m_useLogScale = val;
    }    

private:

    bool m_useLogScale;

    SimpleMatrix calculateGroundDistanceMatrix(const std::map<int, float> &p, const std::map<int, float> &q, const std::vector<int>& allKeys) const;
    SimpleMatrix calculateSimilarityMatrix(const SimpleMatrix &groundDistanceMatrix) const;

    double computeQFDistance(const std::map<int, float> &p, const std::map<int, float> &q, const SimpleMatrix &similarityMatrix, const std::vector<int>& allKeys) const;    

    std::vector<int> getAllSortedKeys(const std::map<int, float> &p, const std::map<int, float> &q) const;
    double convertToLogScale(float db) const;
};

#endif /* QUADRATICFORMDISTANCE_H_ */
