#ifndef TRACKER_H_
#define TRACKER_H_

#include <vector>
#include <string>
#include <assert.h>
#include <stdio.h>

#include "GeoPoint.h"
#include "Fingerprint.h"
#include "Particle.h"
#include "TrackerConfigParameter.h"

// Error codes
static const int SUCCESS = 0;
static const int OPTIONAL_MISSING = 1;
static const int MANDATORY_MISSING = 2;


//forward declarations as we want to hide the implementation in the public interface
class UserLocator;
class TrackerConfig;
class OccupancyGridMap;

/**
 * The public interface of the Tracker.
 **/
class Tracker {
public:

    /**
     * Constructor.
     */
    Tracker();

    /**
     * Destructor.
     */
    ~Tracker();

    /********************************************************//**
     * @name Mandatory
     ***********************************************************/

    /**
     * Set the fingerprints used for tracking.
     * <p/>
     * A fingerprint consists of a unique id as a string, a GeoPoint containing the (lat,long) position where
     * it was recorded and a histogram containing the captured signal strengths.
     * The fingerprints are usually recorded using Wifi or Bluetooth LE signals.
     * <p/>
     * A histogram consists of:
     * <p/>
     *
     *      [unique string identifier for a transmitter i.e. MAC address or UUID]:map<rssi, probability>
     *
     * <p/>
     * Range of RSSI: [-100, -10] as integer <br>
     * Range of probability: [0, 1] as float
     * <p/>
     * Example histogram:
     * <p/>
     *
     *      {
     *          "bec26202-a8d8-4a94-b0fc-9ac1de37daa6_5_14":{
     *          "-60":0.03333334,
     *          "-61":0.03333334,
     *          "-62":0.5333334,
     *          "-63":0.3333333,
     *          "-66":0.03333334,
     *          "0":0.03333334
     *      },
     *          "bec26202-a8d8-4a94-b0fc-9ac1de37daa6_5_3":{
     *          "-72":0.5333334,
     *          "-73":0.3,
     *          "-75":0.06666667,
     *          "-76":0.03333334,
     *          "-78":0.03333334,
     *          "0":0.03333334
     *      }
     * <p/>
     * Fingerprints should be captured online on a device using a dedicated application. <br>
     *
     * @param a vector containing at least 1 fingerprint.
     */
    void setFingerprints(const std::vector<Fingerprint>& fingerprints);

    /**
     * Sets the scale for scaling the calculated movement delta to the underlying map.
     * The scale defines how large the map is in comparison with the real world.
     * <p/>
     * If the map was matched with the size of the actual building, the scale would be 1. If the map is 2x bigger, the scale would be 2.
     * <p/>
     * One way to determine the scale is to measure a door on the map and a door in real life and then compare the two.
     *
     * @param scale the scale (>0)
     */
    void setScale(float scale);

    /**
     * Sets the north angle used for calculating the movement delta in the dead reckoning.
     * Since in the editor the maps are not correctly rotated to fit the buildings but all point to the north, the north angle is 
     * needed to rotate the incoming compass measurements from the y-axis of the imagined, correctly rotated map
     * to the real north or y-axis of the actually placed map, respectively
     * 
     *
     * @param northAngle in degrees within range [-359, 0] and [0, 359]
     */
    void setNorthAngle(float northAngle);



    /********************************************************//**
     * @name Optional for tracking, mandatory for navigation
     ***********************************************************/

    // TODO: remove the dependency on the order of these two method calls (e.g. by merging them to one)
     
    /**
     * Sets an occupancy grid map which includes information about obstacles in the map.
     * The occupancy grid map consists of grayscale values [0,1] with 0 = occupied and 1 = free space.
     * 
     * <br/>
     * It is used to calculate a user position only on free space and to navigate around obstacles.
     * 
     * The occupancy grid map needs a bounding box set by {@link setBoundingBox} because the transformation 
     * between (lat,long) and grid coordinates (i,j) does not work without it.
     *
     * @param pixelValues floating point grayscale image values normalized in range [0, 1]
     * @param width the width of the image in pixels
     * @param height the height of the image in pixels
     */
    void setOccupancyGridMap(std::vector<float> gridMap, int width, int height);

    /**
     * Sets the bounding box of the map defined by min and max coordinates in (lat,long).
     * It is mandatory for navigation and when enabling LOCALISATION_DEADRECKONING it is needed to initialise 
     * the user position in the center of the map.
     * 
     * </p>
     * The min coordinates should be the southwest corner of the map and the max coordinates should be the northeast corner.
     *
     * @param minLat the minimum latitude, usually south
     * @param minLong the minimum longitude, usually west
     * @param maxLat the maximum latitude, usually north
     * @param maxLong the maximum longitude, usually east
     */
    void setBoundingBox(double minLat, double minLong, double maxLat, double maxLong);

private: 
    void setBoundingBox(int minLat, int minLong, int maxLat, int maxLong);
public:


    /********************************************************//**
     * @name Tracker usage
     ***********************************************************/

    /**
     * Starts the tracking in the desired mode.
     * The input parameters e.g. fingerprints or scale are only set when calling this function. 
     * All mandatory parameters have to be set before for it to work.
     *
     * @return An error code, as integer. Valid values are:
     *
     *      0 = success
     *      1 = optional parts missing
     *      2 = mandatory parts missing
     *
     */
    int start();

    /**
     * Stops the tracker.
     */
    void stop();

    /**
     * Adds a new measurent of wifi or bluetooth signals to calculate a new position.
     * <p/>
     * Example histogram:
     * <p/>
     *
     *      {
     *          "bec26202-a8d8-4a94-b0fc-9ac1de37daa6_5_14":{
     *          "-60":0.03333334,
     *          "-61":0.03333334,
     *          "-62":0.5333334,
     *          "-63":0.3333333,
     *          "-66":0.03333334,
     *          "0":0.03333334
     *      },
     *          "bec26202-a8d8-4a94-b0fc-9ac1de37daa6_5_3":{
     *          "-72":0.5333334,
     *          "-73":0.3,
     *          "-75":0.06666667,
     *          "-76":0.03333334,
     *          "-78":0.03333334,
     *          "0":0.03333334
     *      }
     * <p/>
     *
     * @param histogram the Histogram
     */
    void addHistogram(Histogram histogram);

    /**
     * Adds sensor data from the accelerometer, magnetometer and the composed linear acceleration to calculate current movements.
     * 
     * The movement is detected by calculating the quadratic vector length of the linear acceleration and comparing it to a threshold.
     * The threshold for motion detection is different for Android and iOS devices.
     * Also there are some not properly identified devices with Android versions which also send weird values e.g. Samsung S3 mini
     * For devices without gyroscope { @link setConfigParameter} has to be executed with "has_gyroscope" set to false.
     * Then the accelerometer values are also used as linear acceleration.
     * 
     * Threshold values are
     * Android: 1.5
     * iOS: 0.2
     * Devices without gyroscope: +120
     * 
     *
     * @param accX x-value of the accelerometer
     * @param accY y-value of the accelerometer
     * @param accZ z-value of the accelerometer
     * @param linAccX x-value of linear acceleration
     * @param linAccY y-value of linear acceleration
     * @param linAccY z-value of linear acceleration
     * @param magX x-value of the magnetometer
     * @param magY y-value of the magnetometer
     * @param magZ z-value of the magnetometer
     */
    void addSensorData( float accX, float accY, float accZ,
                        float linAccX, float linAccY, float linAccZ,
                        float magX, float magY, float magZ);

    /**
     * Gets the current calculated position as a GeoPoint.
     * 
     * If tracking is not started (0,0) is returned.
     *
     * @return the GeoPoint containing latitude and longitude coordinates
     */
    GeoPoint getCurrentPosition();



    /********************************************************//**
     * @name Navigation usage
     ***********************************************************/

    /**
     * Plans the shortest path from a start position to a goal position considering the obstacles defined in the Occupancy Grid Map.
     * 
     * If the map is not initialised properly or start and goal position are not within the grid an empty vector is returned.
     *
     * @param start the starting GeoPoint
     * @param goal the goal GeoPoint
     * @return the path from the starting point to the goal.
     */
    std::vector<GeoPoint> getPath(GeoPoint start, GeoPoint goal);

    /**
     * Plans the approximately shortest path from a start position, over several stopovers to a goal position (last entry in the goals vector) 
     * considering the obstacles defined in the Occupancy Grid Map.
     * 
     * If the map is not initialised properly or start and goal positions are not within the grid an empty vector is returned.
     *
     * @param start the starting GeoPoint
     * @param goals the goal {@link GeoPoint}s
     * @return the shortest path between the starting and all of the goals
     */
    std::vector<GeoPoint> getShortestPath(GeoPoint start, std::vector<GeoPoint>& goals);



    /********************************************************//**
     * @name Configuration and Debug
     ***********************************************************/

    /**
     * Gets the tracker version string.
     * </p>
     * The corresponding preprocessor defines are:
     *
     *      INVIO_TRACKER_VERSION_MAJOR
     *      INVIO_TRACKER_VERSION_INTERFACE
     *      INVIO_TRACKER_VERSION_MINOR
     *      INVIO_TRACKER_BUILD_NUMBER
     *      INVIO_TRACKER_DEPLOY_NUMBER
     *
     * </p>
     * And as a string with quotation marks
     *
     *      INVIO_TRACKER_GIT_HASH
     *
     * <p/>
     * Example: 1.1.5.170.15-c5a6f1f
     *
     * @return the current tracker version
     */
    static std::string version();

    /**
     * Gets the current particles of the {@link ParticleFilter} mainly for visual debugging.
     * <br>
     * Returns an empty vector if the {@link ParticleFilter} is not enabled.
     * </p>
     * A Particle is defined by {@link GeoPoint} (x, y) coordinates.<br>
     *
     * @return a copy of the current particles of the @{link ParticleFilter}, if enabled.
     */
    std::vector<Particle> particles() const;

    /**
     * Sets the localisation mode to be used:
     * </p>
     *      0 = using only histogram matching
     *      1 = using only dead reckoning
     *      2 = combined histogram matching and dead reckoning using a particle filter
     *
     * @param mode Localisation mode as integer
     */
    void setLocalisationMode(int mode);

    /**
     * Gets the value of the given configuration parameter.
     *
     * @param the configuration parameter
     * @return the configuration parameter value
     */
    std::string getConfigParameter(const std::string& param) const;

    /**
     * Sets the given configuration parameter to the given value.
     *
     * @param param the configuration parameter
     * @param value the configuration value
     */
    void setConfigParameter(const std::string& param, const std::string& value);

    std::vector<TrackerConfigParameter> getConfig() const;

    /**
     * Enables or disables the debug log. True if debug log should be shown, false if not.
     * <p/>
     * NOTE: Be sure this is set to FALSE on a release. This is only intended for development debugging.
     *
     * @param enabled whether or not the logs should be enabled
     */
    void setDebugLogEnabled(bool enabled);

    /********************************************************//**
     * @name End
     ***********************************************************/

private:
    bool m_debugLogEnabled;
    UserLocator* m_userLocator;
    OccupancyGridMap* m_gridMap;
    std::vector<Fingerprint> m_fingerprints;
    GeoPoint m_min, m_max;
    float m_northAngle;
    float m_scale; // TODO short description how scale is defined 
    int m_localisationMode;
};

#endif /* TRACKER_H_ */
