#ifndef TRACKER_LOG_H_
#define TRACKER_LOG_H_

#include <string>

typedef enum {
    NVM, BTW, FYI, WTF, OMG
} LogLevel;

typedef enum {
    SINK_NULL, SINK_STDERR, SINK_BUFFER
} LogSink;

class TrackerLogSink;

class TrackerLog
{
public:
    void initializeSink(LogSink sink);
    // static void log(LogLevel level, const char* msg);
    static void log(LogLevel level, const std::string& msg);
    static void log(LogLevel level, const char* fmt, ...);
};

#endif