#ifndef DEADRECKONING_H
#define DEADRECKONING_H

#include <vector>
#include <boost/thread/mutex.hpp>

#include "V2d.h"
#include "GeoPoint.h"
#include "SensorMeasurement.h"
#include "LowPassFilter.h"

#define SENSOR_ACCELEROMETER 0
#define SENSOR_LINEAR_ACCELERATION 1
#define SENSOR_MAGNETOMETER 2

/**
 * Gets the sensor data of the cell phone as an input and calculates a position delta by means of the measured acceleration and orientation angle.
 * A motion model is used to detect and specify the measured movement.
 * Also low pass filtering of the sensor data happens here.
 * 
 * @param baseAngle Since the maps are placed in JOSM without any rotation a base angle is needed to calculate the right orientation.
 *                                   The base angle projects the actual north to the y-axis of the map.
 */

class DeadReckoning {
public:
    
    DeadReckoning();

    void addSensorData(const SensorMeasurement& acc, const SensorMeasurement& linacc, const SensorMeasurement& magn);
    void updateRawSensorValues(const SensorMeasurement& acc, const SensorMeasurement& linacc, const SensorMeasurement& magn);
    virtual GeoPoint getDelta(); // virtual because we need to override this in unit tests
    void calculateDelta();
    void startTracking();
    
    inline double toRadians(double degrees) const {
        return degrees * M_PI / 180.0;
    }
    
    inline double toDegrees(double rad) const {
        return rad * 180.0 / M_PI;
    }
    
    double getBaseAngle() const { 
        return m_baseAngle; 
    }

    double getDistance() const {
        return m_distance;
    }

    void setBaseAngle(double baseAngle) { 
        m_baseAngle = toRadians(baseAngle);
    }
    
    // for testing purposes
#if TESTING
    void setDelta(float x, float y) { 
        m_delta.x = x;
        m_delta.y = y;
    }
#endif
    V2d getCurrentDelta() const {
        return m_delta;
    }

    SensorMeasurement getRawLinearAcceleration() const {
        return m_rawLinearAcceleration;
    }

    SensorMeasurement getRawAcceleration() const {
        return m_rawAccelerometer;
    }

    SensorMeasurement getRawMagnetometer() const {
        return m_rawMagnetometer;
    }

    int getLastUpdatedSensor() const {
        return m_lastUpdatedSensor;
    }
    
    void setLowPassFilterWeights(float compass, float acc, float mag) {
        if ((compass < 0.f) || (compass > 1.f) || (acc < 0.f) || (acc > 1.f) || (mag < 0.f) || (mag > 1.f)) {
            throw std::invalid_argument("Low pass filter weights must have a value range of {0 .. 1}.");
        } else {
            m_filterWeightCompass = compass;
            m_filterWeightAcc = acc;
            m_filterWeightMag = mag;
        }
    }
    
    virtual bool detectMovement() const;
    
protected:
    virtual float calculateAzimuth();

private:
    LowPassFilter m_lowPass;
    
    // weight = 1: disables filter (new values will be used directly),
    // weight = 0: value does not change (new values, after the first one, will be ignored)
    float m_filterWeightCompass;
    float m_filterWeightAcc;
    float m_filterWeightMag;
    float m_detectionSensitivity;
    int m_motionModel;
    
    SensorMeasurement m_linearAcceleration;
    SensorMeasurement m_accelerometer;
    SensorMeasurement m_magnetometer;

    //For tests pourposes
    SensorMeasurement m_rawLinearAcceleration;
    SensorMeasurement m_rawAccelerometer;
    SensorMeasurement m_rawMagnetometer;
    int m_lastUpdatedSensor;
    
    // This will hold the acummulated, estimated movement vector since the last query to the DR.
    // (DR-speed doesn't need to be in sync with overall localization speed)
	V2d m_delta;
    
    double m_baseAngle;    // TODO: in the Tracker this is called northAngle. Shouldn't it be uniform?
    double m_distance;
    
    boost::mutex m_mutex;            // This mutex is used to atomize writes to m_delta, to prevent the reading of partial updates.
    boost::posix_time::ptime m_last;
    boost::posix_time::ptime m_curr;
   
    void init();
    bool getRotationMatrix(V2d* rotation) const;
};

#endif	/* DEADRECKONING_H */
