#include "UserLocator.h"
#include "TrackerConfig.h"
#include "CentroidMedianEliminator.h"
#include "DeadReckoning.h"
#include "OccupancyGridMap.h"
#include "QuadraticFormDistance.h"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <ctime>

#include "DebugTimer.h"

using namespace std;

UserLocator::UserLocator(int localisationMode)
    : m_particleFilter(),
      m_history(5), // TODO: make history size configurable?
      m_gridMap() {
    init(localisationMode);
}

UserLocator::~UserLocator() {
    if (m_isTracking) {
        stopTracking();
    }
    delete m_deadReckoning;
}

void UserLocator::init(int localisationMode) {
    TrackerConfig* config = TrackerConfig::getInstance();

    m_localisationMode = localisationMode;
    m_useOutlierDetection = config->getBool(ENABLE_OUTLIER_DETECTION);
    m_updateRate = config->getInt(LOCALISATION_UPDATE_RATE);
    m_sufficientNumMeasurements = config->getInt(SUFFICIENT_MEASUREMENTS_INIT);
    m_numPreprocessedFingerprint = config->getInt(NUM_PREPROCESSED_FINGERPRINTS);
    m_numNeighbours = config->getInt(NUM_NEIGHBOURS);
    m_outlierDetectionThreshold = config->getDouble(OUTLIER_DETECTION_THRESHOLD);
    m_movementThreshold = config->getDouble(MOVEMENT_THRESHOLD);

    m_centerPosition.setXY(0.f, 0.f); 
    m_currentPosition.setXY(0.f, 0.f); 
    m_nextWifiPosition.setXY(0.f, 0.f);
    m_isTracking = false;
    m_pfInit = false;
    m_newHist = false;
    m_isPositionComputedWithHist = false;
    m_numMeasurements = 0;
    
    m_drInit = false;

#ifdef STANDALONE
    m_writeToFile = false;
    m_histnr = -1;
    m_meanerror = 0.f;
    m_meanerrorMeter = 0.f;
#endif

    m_deadReckoning = new DeadReckoning();
}

void UserLocator::setFingerprints(std::vector<Fingerprint> fingerprints) {
    m_fingerprints = fingerprints;
    m_fingerprintPreprocessor.setFingerprints(fingerprints);
}

void UserLocator::startTracking() {
    m_isTracking = true;

#ifdef STANDALONE
    // for logging the positions for the standalone tracker
    if (m_writeToFile) {
		std::time_t t = std::time(NULL);
		char buffer[100];
		std::strftime(buffer, sizeof(buffer), "%F-%H-%M-%S", std::localtime(&t));

        std::string fileName = "log/positions-";
        fileName += buffer;
        fileName += ".txt";
        
        std::string fileNameFP = "log/fppositions-";
        fileNameFP += buffer;
        fileNameFP += ".txt";

        m_positionFile.open(fileName.c_str(), ios::out | ios::app);      
        m_fpposFile.open(fileNameFP.c_str(), ios::out | ios::app);  
    }
#endif

    // start the tracking loop
    m_localisationThread = boost::thread(boost::bind(&UserLocator::localisationCallback, this));
}

void UserLocator::stopTracking() {
    m_isTracking = false;
    
    // if tracking is stopped and getCurrentPosition is still called it should give back (0,0)
    m_currentPosition.reset();

#ifdef STANDALONE    
    if (m_writeToFile) {
        m_positionFile.close();
        m_fpposFile.close();
    }
#endif

    // make sure our background thread is no longer running when we return - stop the io_service, then wait for the thread to finish
    m_io.stop();
    m_localisationThread.join();
}

void UserLocator::localisationCallback() {    
    // run the tracking loop with a given update rate
    while (m_isTracking) {
        m_io.reset();
        boost::asio::deadline_timer timer(m_io, boost::posix_time::milliseconds(m_updateRate)); // see java user locator
        timer.async_wait(boost::bind(&UserLocator::localisationIteration, this));
        m_io.run();
    }    
}

void UserLocator::localisationIteration() {
    if (m_newHist) {
        // calculate the wifi/bluetooth position when a new measurent came in

		//workaround for long mutex blocks
		m_mutex.lock();
		Histogram copy = m_hist;
		m_mutex.unlock();
		vector<GeoPoint> distanceSet = calculateDistances(copy);

        vector<GeoPoint> nearestNeighbours = getNearestNeighbours(distanceSet);
        vector<GeoPoint> withoutOutliers;
        if (m_useOutlierDetection) {
            withoutOutliers = removeOutliers(nearestNeighbours);
        } else {
            withoutOutliers = nearestNeighbours;
        }        
        GeoPoint position = interpolatePosition(withoutOutliers);
        
        // test if a position could be found or if the position is (0,0)
        if (abs(position.getX()) > 1.f && abs(position.getY()) > 1.f) {
            filterPosition(position);
        } else {
            // no position found, no wifi tracking should be triggered
            m_newHist = false;
        }
        m_numMeasurements++;
    }

    if (m_localisationMode == LOCALISATION_PARTICLEFILTER) {
        trackWithParticleFilter();
    } else {
        trackWithoutParticleFilter();
    }

// completely disabled for now
#if 0 // #ifdef STANDALONE 
    // used for testing fingerprints against each other    
    // save or output the resulting position as well as the respective fingerprint position

    if (m_writeToFile && m_positionFile.is_open() && m_fpposFile.is_open() && 
        abs(getCurrentPosition().getX()) > 1.f && abs(getCurrentPosition().getY()) > 1.f) {
        m_positionFile << getCurrentPosition().getLatitudeE6()/ 1E6 << "  " << getCurrentPosition().getLongitudeE6()/ 1E6 << "\n";
        m_positionFile.flush(); 
        m_fpposFile << m_fingerprints[m_histnr].getPoint().getLatitudeE6()/ 1E6 << " " 
                    << m_fingerprints[m_histnr].getPoint().getLongitudeE6()/ 1E6 << "\n";
        m_fpposFile.flush();
    }

    if (abs(getCurrentPosition().getX()) > 1.f && abs(getCurrentPosition().getY()) > 1.f && m_histnr < m_fingerprints.size()) {
        cout << "FP " << m_fingerprints[m_histnr].getId() << " " 
             << m_fingerprints[m_histnr].getPoint().getLatitudeE6() / 1E6 << " " 
             << m_fingerprints[m_histnr].getPoint().getLongitudeE6() / 1E6 << std::endl;

        cout << "CURRENT POS " << getCurrentPosition().getLatitudeE6() / 1E6 << "  " 
             << getCurrentPosition().getLongitudeE6()/ 1E6 << std::endl;
        
        float dist = m_fingerprints[m_histnr].getPoint().calculateEuclideanDistanceTo(getCurrentPosition())/(m_scale*1000.f);
        float distMeter = m_fingerprints[m_histnr].getPoint().calculateGreatCircleDistanceTo(getCurrentPosition())/(m_scale*1000.f);

        // disabled for our ITAB measurements
        m_meanerror += dist;
        m_meanerrorMeter += distMeter;
        cout << "DIST " << dist << endl;
        cout << "DISTMETER " << distMeter << endl;
        // stop the calculation since all fingerprints are through and calculate the mean error 
        if (m_histnr == m_fingerprints.size()-1) {
            float error = m_meanerror/m_fingerprints.size();
            float errorMeter = m_meanerrorMeter/m_fingerprints.size();
            cout << "MEANERROR " << error << endl;
            cout << "MEANERRORMETER " << errorMeter << endl;
            // stopTracking needed for the fingerprinttest in the standalone version. but does not has to be there for app version. 
            // flag is set inside build.sh>CMakeList
            stopTracking();
        }
    }
#endif
}

void UserLocator::filterPosition(GeoPoint position) {
    // only use median/mean filter if new position is far away from the old one to avoid jumps but not attenuate all changes        
    float dist = -1;
    if (abs(m_nextWifiPosition.getX()) > 1.f && abs(m_nextWifiPosition.getY()) > 1.f) // first iteration?
        dist = m_nextWifiPosition.calculateEuclideanDistanceTo(position);

    m_history.add(position);
    if (dist > m_movementThreshold) { // only distances over 0.1m need to be filtered // Messe, check again
        GeoPoint filteredPosition(m_history.getAveragePoint()); // choose median or mean
        m_nextWifiPosition.setXY(filteredPosition.getX(), filteredPosition.getY());
    } else {
        m_nextWifiPosition.setXY(position.getX(), position.getY());
    }
}

void UserLocator::trackWithParticleFilter() {
    if (m_newHist) {
        if (!m_pfInit && (m_numMeasurements >= m_sufficientNumMeasurements)) {
            // Initialize the particle filter with the first wifi/bluetooth position
            if (m_scale > 0.f) {
                m_particleFilter.setGridMap(m_gridMap);
                m_particleFilter.setScale(m_scale);
                m_particleFilter.initialize(m_nextWifiPosition.getX(), m_nextWifiPosition.getY());
                setCurrentPosition(m_particleFilter.calculatePosition(), true);
                m_pfInit = true;
            }
        } else {
            if (m_pfInit) {
                // Ok, we have a new result from the wifi-tracking, let's just use it:                
                m_particleFilter.addRandomParticles(m_centerPosition.getX(), m_centerPosition.getY());
                m_particleFilter.updateWithWifiPosition(m_nextWifiPosition.getX(), m_nextWifiPosition.getY());
                m_particleFilter.resample();
                setCurrentPosition(m_particleFilter.calculatePosition());
            }
        }
        m_newHist = false;
        m_isPositionComputedWithHist = true;
    } else {
        // if no new RSSI histogram came in use dead reckoning
        GeoPoint normDelta = m_deadReckoning->getDelta();
        if (normDelta.getX() != 0.f && normDelta.getY() != 0.f) {
            m_particleFilter.updateWithDeadReckoning(normDelta.getX()*m_scale, normDelta.getY()*m_scale);
        }
        setCurrentPosition(m_particleFilter.calculatePosition());
        m_isPositionComputedWithHist = false;
    }
}

void UserLocator::trackWithoutParticleFilter() {
    // either localisation via dead reckoning or wifi/bluetooth
    if (m_newHist && m_localisationMode == LOCALISATION_WIFI) {
        // Ok, we have a new result from the wifi-tracking, let's just use it:
        GeoPoint wifiPosition = GeoPoint::makePoint(m_nextWifiPosition.getX(), m_nextWifiPosition.getY());
        setCurrentPosition(wifiPosition);
        m_newHist = false;
        m_isPositionComputedWithHist = true;
    }
    if (m_localisationMode == LOCALISATION_DEADRECKONING) {
        // Initialise the start position to the middle of the map since dead reckoning is primarily used for testing.
        if (!m_drInit) {
            initCenter();
            m_drInit = true;
        }
        // No new wifi-position, we will try to approximate a new position by using our movement data:
        GeoPoint normDelta = m_deadReckoning->getDelta();
        double x = m_currentPosition.getX() + normDelta.getX() * m_scale;
        double y = m_currentPosition.getY() + normDelta.getY() * m_scale;
        GeoPoint drPosition = GeoPoint::makePoint(x, y);
        setCurrentPosition(drPosition);
        m_isPositionComputedWithHist = false;
    }
}

// Set the current user position to newPostion.
// If that's not possible because newPostion is in occupied space, attempt
// to find the nearest free position by walking along a ray from the current 
// to the new positions and stopping just before the first obstacle. 
// The center of the last accessible grid map pixel will become the
// new position.
void UserLocator::setCurrentPosition(const GeoPoint& newPosition, bool force) {

    if (force || m_gridMap == 0 || m_gridMap->isPointAccessible(newPosition)) {
        m_currentPosition = newPosition;
        return;
    }

    // new position is invalid, do the walk
    V2i startPixel = m_gridMap->getGridPosition(m_currentPosition);
    V2i endPixel = m_gridMap->getGridPosition(newPosition);

    float deltaX = endPixel.x - startPixel.x;
    float deltaY = endPixel.y - startPixel.y;

    float steps = std::max(std::abs(deltaX), std::abs(deltaY));
    float stepX = deltaX / steps;
    float stepY = deltaY / steps;

    float testX = startPixel.x;
    float testY = startPixel.y;

    V2i lastGoodPixel = startPixel; // so we always have a valid position to fall back to

    for (int i=0; i<steps; ++i) {
        testX += stepX;
        testY += stepY;

        V2i testPixel = V2i(testX, testY);

        if (m_gridMap->isPixelAccessible(testPixel)) {
            // pixel is good, remember it and move further
            lastGoodPixel = testPixel;
        } else {
            // reached an obstacle. go back to the last known good position
            // and end the loop
            V2i latlon = m_gridMap->getRealPosition(lastGoodPixel);
            m_currentPosition = GeoPoint(latlon.latitude, latlon.longitude);
            break;
        }
    }
}

void UserLocator::addHistogram(Histogram hist) {
    m_mutex.lock();
    m_hist = hist;
    m_newHist = true;
#ifdef STANDALONE
    m_histnr++;
#endif
    m_mutex.unlock();
}

vector<GeoPoint> UserLocator::calculateDistances(Histogram hist) {
    BEGIN_TIMER(calculateDistances);
    vector<GeoPoint> distanceSet;
    distanceSet.reserve(m_numPreprocessedFingerprint);

    m_fingerprintPreprocessor.excludeFingerprintsByMean(hist, m_numPreprocessedFingerprint);
    vector<Fingerprint> fingerprints = m_fingerprintPreprocessor.getProcessedFingerprints();

    QuadraticFormDistance distanceAlgorithm;

    for (size_t i = 0; i < fingerprints.size(); i++) {
        double qfDist = distanceAlgorithm.compareHistograms(hist, fingerprints[i].getHistogram());
        if (qfDist > 0) {
            GeoPoint distancePoint(fingerprints[i].getPoint());
            distancePoint.setDistance(qfDist);
            distanceSet.push_back(distancePoint);
        }
    }
    END_TIMER(calculateDistances);    
    return distanceSet;
}

vector<GeoPoint> UserLocator::getNearestNeighbours(vector<GeoPoint>& distanceSet) {
    // simple knn
    vector<GeoPoint> nearestNeighbours;
    nearestNeighbours.reserve(distanceSet.size());

    sort(distanceSet.begin(), distanceSet.end(), GeoPoint::compare);

    for (int i = 0; i < m_numNeighbours; i++) {
        if (distanceSet.size() > i)
            nearestNeighbours.push_back(distanceSet[i]);
    }

    return nearestNeighbours;
}

vector<GeoPoint> UserLocator::removeOutliers(vector<GeoPoint>& nearestNeighbours) {
    vector<GeoPoint> withoutOutliers;
    withoutOutliers.reserve(nearestNeighbours.size());

    CentroidMedianEliminator eliminator(m_outlierDetectionThreshold);
    if (m_outlierDetectionThreshold > 0) {
        eliminator.setThreshold(m_outlierDetectionThreshold);
    }

    withoutOutliers = eliminator.removeOutliers(nearestNeighbours);
    
    return withoutOutliers;
}

GeoPoint UserLocator::interpolatePosition(vector<GeoPoint>& points) {
    // weighted interpolation with regard to distances of each fingerprint to the measures histogram 
    GeoPoint position;
    
    double x = 0.f;
    double y = 0.f;

    if (!points.empty()) {
        float normalizationFactor = 0.f;
        
        for (unsigned int i = 0; i < points.size(); i++) {
            // If the distance is 0 with a delta of 0.4, we assume, that the user position is the fingerprint position
            if (abs(points[i].getDistance()) <= 0.4f) {
                return points[i];
            }

            double weight;
           
            // think about how to transform the qfdistance to a proper weight
            weight = 1 / (points[i].getDistance()*points[i].getDistance()*points[i].getDistance()); 
            normalizationFactor += weight;

            x += weight * points[i].getX();
            y += weight * points[i].getY();
        }

        x = x / normalizationFactor;
        y = y / normalizationFactor;
    }
    position.setXY(x, y);

    return position;
}

vector<Particle> UserLocator::particles() const { 
    vector<Particle> particles;
    
    if (m_localisationMode == LOCALISATION_PARTICLEFILTER) {
        particles = m_particleFilter.m_particles;
    }
    
    return particles;
}

void UserLocator::initCenter() {
    // used for initialisation of the start position for dead reckoning in the middle of the map
    GeoPoint center;
    float diffX = abs(m_boundingBoxMin.getX() - m_boundingBoxMax.getX());
    float diffY = abs(m_boundingBoxMin.getY() - m_boundingBoxMax.getY());
    
    center.setXY(m_boundingBoxMin.getX() + diffX/2, m_boundingBoxMin.getY() + diffY/2);
    setCurrentPosition(center, true);
}
