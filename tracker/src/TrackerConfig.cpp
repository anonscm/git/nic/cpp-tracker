#include "TrackerConfig.h"

#include <iostream>
#include <boost/lexical_cast.hpp>

using namespace std;

#ifdef ANDROID
    // educated guess for a good threshold
    #define DEFAULT_MOTION_DETECTION_THRESHOLD  "1.5"
#endif
#ifdef IOS
    // Apple devices send different linear acceleration values than Android devices!
    #define DEFAULT_MOTION_DETECTION_THRESHOLD  "0.2"
#endif
#ifndef DEFAULT_MOTION_DETECTION_THRESHOLD
    #define DEFAULT_MOTION_DETECTION_THRESHOLD  "1.7"
#endif

// name, type, min, max, default, description
static TrackerConfigParameter settings[] = {
    // dead reckoning parameters
    { LOW_PASS_FILTER_WEIGHT_ACCELEROMETER, PT_DOUBLE, 0, 1, "1", "DR: weight of Accelerometer data" },
    { LOW_PASS_FILTER_WEIGHT_MAGNETOMETER, PT_DOUBLE, 0, 1, "1", "DR: weight of Magnetometer data" },
    { LOW_PASS_FILTER_WEIGHT_COMPASS, PT_DOUBLE, 0, 1, "1", "DR: weight of Compass data" },
    { MOTION_DETECTION_THRESHOLD, PT_DOUBLE, 0, 10, DEFAULT_MOTION_DETECTION_THRESHOLD, "DR: motion detection threshold" },
    { MOTION_MODEL, PT_INT, 0, 300, "150", "DR: assumed motion speed (cm/s)" },
    { HAS_GYROSCOPE, PT_BOOL, 0, 1, "1", "DR: is gyroscope present?" },

    // occupancy gridmap
    { OGM_PIXEL_BLACK, PT_DOUBLE, 0, 1, "0.0", "OGM: black pixel value" },
    { OGM_PIXEL_WHITE, PT_DOUBLE, 0, 1, "1.0", "OGM: white pixel value" },
    { OGM_PIXEL_GREY, PT_DOUBLE, 0, 1, "0.5", "OGM: grey pixel value" },

    // particle filter
    { NUM_PARTICLES, PT_INT, 1, 1000, "150", "PF: No. of particles" },   // more than 500 particles can only be supported once we change the JNI layer 
                                                                        // (see SEL-666 for details)
    { PCT_RANDOM_PARTICLES, PT_DOUBLE, 0, 100, "33", "PF: % of random particles" }, // default: one third of num_particles
    { CLUSTER_RADIUS, PT_INT, 0, 1000, "300", "PF: cluster radius (cm)" }, // all particles within this radius belong to a cluster
    { SIGMA_SENSOR, PT_INT, 0, 1000, "100", "PF: resample scale" },        // scale factor when the particles are resampled
    { SIGMA_INIT, PT_INT, 0, 1000, "1000", "PF: sigma_init" },             // scale factor for particles when no pixels are accessible
    { SIGMA_ACTION, PT_INT, 0, 1000, "100", "PF: sigma_action" },          // scale factor for particles when no histogram input is available
    { SIGMA_RANDOM_PARTICLES, PT_INT, 0, 10000, "10000", "PF: sigma_random_particles"}, // scale factor for random particles when no pixels are accessible
    
    
    // userlocator
    { LOCALISATION_UPDATE_RATE, PT_INT, 0, 1000, "200", "UL: update rate (ms)" },          // timer interval for the update thread
    { ENABLE_OUTLIER_DETECTION, PT_BOOL, 0, 1, "1", "UL: enable outlier detection" },      // enable outlier detection 
    { OUTLIER_DETECTION_THRESHOLD, PT_INT, 1, 5, "2", "UL: outlier detection threshold" },   // disregard all fingerprints further away than this
    { SUFFICIENT_MEASUREMENTS_INIT, PT_INT, 0, 10, "3", "UL: initial measurements" },      // number of initial measurements to skip
    { NUM_PREPROCESSED_FINGERPRINTS, PT_INT, 1, 100, "30", "UL: preprocessed fingerprints"},  // number of FPs to preprocess
    { NUM_NEIGHBOURS, PT_INT, 1, 10, "5", "UL: No. of neighbours" },                          // number of neighbours
    { MOVEMENT_THRESHOLD, PT_INT, 0, 1000, "200", "UL: movement threshold" },                 // max. direct movement allowed

	//quadratic form distance
	{ QFD_USE_LOG_SCALE, PT_BOOL, 0, 1, "false", "QFD: logarithmic scale" },

    // tracker
    { LOCALISATION_MODE, PT_INT, 0, 2, "2", "Localisation Mode" }, // localisation mode, 0=wifi, 1=dead reckoning, 2=particle filter

#ifdef STANDALONE
    // standalone tracker only
    { SENSOR_MEASUREMENT_UPDATE_RATE, PT_INT, 0, 5000, "100" },
    { WIFI_MEASUREMENT_UPDATE_RATE, PT_INT, 0, 5000, "1000" },
    { POSITION_OUTPUT_FREQ, PT_INT, 1, 2000, "500" },

	{ OCCUPANCY_GRID_MAP_FILE, PT_STRING, 0, 0, "" },
	{ FINGERPRINT_FILE, PT_STRING, 0, 0, "" },
	{ WIFI_MEASUREMENTS_FILE, PT_STRING, 0, 0, "" },
	{ ACCELEROMETER_FILE, PT_STRING, 0, 0, "" },
	{ LINEAR_ACCELERATION_FILE, PT_STRING, 0, 0, "" },
	{ MAGNETOMETER_FILE, PT_STRING, 0, 0, "" },
    { POSITIONS_FILE, PT_STRING, 0, 0, "" },

	{ BOUNDING_BOX_MIN_LAT, PT_DOUBLE, -90, 90, "0" },
	{ BOUNDING_BOX_MAX_LAT, PT_DOUBLE, -90, 90, "0" },
	{ BOUNDING_BOX_MIN_LON, PT_DOUBLE, -180, 180, "0" },
	{ BOUNDING_BOX_MAX_LON, PT_DOUBLE, -180, 180, "0" },
    
    { SCALE, PT_DOUBLE, 0, 10000, "1000" },
    { NORTH_ANGLE, PT_DOUBLE, 0, 10000, "80" },

#endif

    { } // sentinel
};

#define CLAMP(value, lower, upper)      std::max(lower, std::min(value, upper))

static TrackerConfig* _instance = nullptr;

TrackerConfig* TrackerConfig::getInstance() {
    if (_instance == nullptr) {
        _instance = new TrackerConfig();
    }
    return _instance;
}

TrackerConfig::TrackerConfig() {
    for (TrackerConfigParameter* param = settings; param->name.length(); ++param)
    {
        param->currentValue = param->defaultValue;
        m_config.insert(make_pair(param->name, param));
    }
}

bool TrackerConfig::getBool(const string& name) const {
    TrackerConfigParameter* param = getParameter(name);
    if (param) {
        if (param->currentValue == "true") {
            return true;
        }
        if (param->currentValue == "false") {
            return false;
        };
        return boost::lexical_cast<int>(param->currentValue) != 0;
    }
    return false;
}

int TrackerConfig::getInt(const string& name) const {
    TrackerConfigParameter* param = getParameter(name);
    if (param) {
        int value = (int)boost::lexical_cast<double>(param->currentValue);
        return CLAMP(value, (int)param->minValue, (int)param->maxValue);
    }
    return 0;
}

double TrackerConfig::getDouble(const string& name) const {
    TrackerConfigParameter* param = getParameter(name);
    if (param) {
        double value = boost::lexical_cast<double>(param->currentValue);
        return CLAMP(value, param->minValue, param->maxValue);
    }
    return 0.0;
}

string TrackerConfig::getString(const string& name) const {
    TrackerConfigParameter* param = getParameter(name);
    if (param) {
        return param->currentValue;
    }
    return "";
}

TrackerConfigParameter* TrackerConfig::getParameter(const string& name) const {
    auto it = m_config.find(name);
    return it == m_config.end() ? nullptr : it->second;
}

void TrackerConfig::setValue(const string& name, const string& value) {
    auto it = m_config.find(name);
    if (it == m_config.end()) {
        return;
    }
    TrackerConfigParameter* param = it->second;
    param->currentValue = value;
}

vector<TrackerConfigParameter> TrackerConfig::getConfig() const {
    vector<TrackerConfigParameter> result;
    
    for (auto it = m_config.begin(); it != m_config.end(); ++it) {
        result.push_back(*it->second);
    }

    return result;
}
