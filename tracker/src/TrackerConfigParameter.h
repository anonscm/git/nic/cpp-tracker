#ifndef TRACKER_CONFIG_PARAMETER_H
#define TRACKER_CONFIG_PARAMETER_H

#include <string>

typedef enum { PT_INT, PT_BOOL, PT_DOUBLE, PT_STRING } ParameterType;

typedef struct {
    std::string name;
    ParameterType type;
    double minValue;
    double maxValue;
    std::string defaultValue;

    std::string description;
    std::string currentValue;
} TrackerConfigParameter;

#define LOW_PASS_FILTER_WEIGHT_ACCELEROMETER    "low_pass_filter_weight_accelerometer"
#define LOW_PASS_FILTER_WEIGHT_MAGNETOMETER     "low_pass_filter_weight_magnetometer"
#define LOW_PASS_FILTER_WEIGHT_COMPASS          "low_pass_filter_weight_compass"
#define MOTION_DETECTION_THRESHOLD 				"motion_detection_threshold"
#define MOTION_MODEL 							"motion_model"
#define HAS_GYROSCOPE 							"has_gyroscope"
#define OGM_PIXEL_BLACK 						"ogm_pixel_black"
#define OGM_PIXEL_WHITE 						"ogm_pixel_white"
#define OGM_PIXEL_GREY 							"ogm_pixel_grey"
#define NUM_PARTICLES 							"num_particles"
#define PCT_RANDOM_PARTICLES 					"pct_random_particles"
#define SIGMA_INIT 								"sigma_init"
#define SIGMA_ACTION 							"sigma_action"
#define SIGMA_RANDOM_PARTICLES 					"sigma_random_particles"
#define SIGMA_SENSOR 							"sigma_sensor"
#define CLUSTER_RADIUS 							"cluster_radius"
#define LOCALISATION_UPDATE_RATE 				"localisation_update_rate"
#define ENABLE_OUTLIER_DETECTION 				"enable_outlier_detection"
#define SUFFICIENT_MEASUREMENTS_INIT 			"sufficient_measurements_init"
#define NUM_PREPROCESSED_FINGERPRINTS 			"num_preprocessed_fingerprints"
#define NUM_NEIGHBOURS 							"num_neighbours"
#define OUTLIER_DETECTION_THRESHOLD 			"outlier_detection_threshold"
#define MOVEMENT_THRESHOLD 						"movement_threshold"
#define LOCALISATION_MODE						"localisation_mode"
#define QFD_USE_LOG_SCALE						"qfd_use_log_scale"

// these are used in the standalone tracker only
#ifdef STANDALONE
#define FINGERPRINT_FILE						"fingerprint_file"
#define WIFI_MEASUREMENTS_FILE					"wifi_measurements_file"
#define ACCELEROMETER_FILE 						"accelerometer_file"
#define LINEAR_ACCELERATION_FILE 				"linear_acceleration_file"
#define MAGNETOMETER_FILE 						"magnetometer_file"
#define OCCUPANCY_GRID_MAP_FILE 				"occupancy_grid_map_file"
#define POSITIONS_FILE							"positions_file"
#define SENSOR_MEASUREMENT_UPDATE_RATE			"sensor_measurement_update_rate"
#define WIFI_MEASUREMENT_UPDATE_RATE			"wifi_measurement_update_rate"


#define BOUNDING_BOX_MIN_LAT 					"bounding_box_min_lat"
#define BOUNDING_BOX_MIN_LON 					"bounding_box_min_lon"
#define BOUNDING_BOX_MAX_LAT 					"bounding_box_max_lat"
#define BOUNDING_BOX_MAX_LON 					"bounding_box_max_lon"
#define SCALE 									"scale"
#define NORTH_ANGLE								"north_angle"
#define POSITION_OUTPUT_FREQ                    "position_output_freq"
#endif // STANDALONE

#endif // TRACKER_CONFIG_PARAMETER_H