
#ifndef V2i_H
#define V2i_H

/*
 * V2i is a union of two structs, each containing two int members: 
 * X/Y for pixel coordinates, Latitude/Longitude for map coordinates
 * 
 * both structs are designed to support the natural ordering of 
 * members in struct initializers, so you can say
 *
 * V2i pixel = V2i(x,y);
 * V2i coord = V2i(latitude, longitude);
 *
 * unfortunately, we cannot use the brace-initializer syntax 
 * V2i pixel = {1,1} 
 * because the android sdk c++ compiler is too old and doesn't support C++11 fully :(
 *
 */

typedef union V2i {
	struct {
    	int x;
    	int y;
	};
	struct {
		int latitude;
		int longitude;
	};

	V2i(int a, int b) {
		x = a;
		y = b;
	}
	V2i() {
		x = y = 0;
	}

private:
	// overload op= privately so that brace-initializer assignments cause an error
	typedef struct {int a,b;} _2int;
	V2i& operator = (const _2int& x);
} V2i;

#endif // V2i_H
