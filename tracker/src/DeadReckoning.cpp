#include "DeadReckoning.h"
#include "LowPassFilter.h"
#include "TrackerConfig.h"

using namespace std;

DeadReckoning::DeadReckoning() {
    init();
}

void DeadReckoning::init() {
    m_linearAcceleration = {};
    m_accelerometer = {};
    m_magnetometer = {};

    m_rawLinearAcceleration = {};
    m_rawAccelerometer = {};
    m_rawMagnetometer = {};

    m_lastUpdatedSensor = -1;
    
    m_baseAngle = 0.f;

    m_distance = 0.f;
    
    m_delta = { 0.0, 0.0 }; 
    
    TrackerConfig* config = TrackerConfig::getInstance();
    
    m_filterWeightCompass = config->getDouble(LOW_PASS_FILTER_WEIGHT_COMPASS);
    m_filterWeightAcc = config->getDouble(LOW_PASS_FILTER_WEIGHT_ACCELEROMETER);
    m_filterWeightMag = config->getDouble(LOW_PASS_FILTER_WEIGHT_MAGNETOMETER);
    double threshold = config->getDouble(MOTION_DETECTION_THRESHOLD);
    m_detectionSensitivity = threshold * threshold;
    
    if (!config->getBool(HAS_GYROSCOPE)) {
        // from java code
        // value guessed by means of measured sensor data of cell phones without gyroscope
        //
        // TODO: This should let us at least detect acceleration that adds to gravity, but smarter detection would really
        // be necessary...
        m_detectionSensitivity += 120;
    }

    m_motionModel = config->getInt(MOTION_MODEL);
}

//Update raw Sensor values. For tests pourposes
void DeadReckoning::updateRawSensorValues(const SensorMeasurement& acc, const SensorMeasurement& linacc, const SensorMeasurement& magn) {
    if (m_rawLinearAcceleration.x != linacc.x ||
        m_rawLinearAcceleration.y != linacc.y ||
        m_rawLinearAcceleration.z != linacc.z) {

        m_lastUpdatedSensor = SENSOR_LINEAR_ACCELERATION;

    } else if (m_rawAccelerometer.x != acc.x ||
               m_rawAccelerometer.y != acc.y ||
               m_rawAccelerometer.z != acc.z) {

        m_lastUpdatedSensor = SENSOR_ACCELEROMETER;

    } else if (m_rawMagnetometer.x != magn.x ||
               m_rawMagnetometer.y != magn.y ||
               m_rawMagnetometer.z != magn.z) {
        m_lastUpdatedSensor = SENSOR_MAGNETOMETER;        
    } else {
        m_lastUpdatedSensor = -1;
    }

    m_rawLinearAcceleration.x = linacc.x;
    m_rawLinearAcceleration.y = linacc.y;
    m_rawLinearAcceleration.z = linacc.z;

    m_rawAccelerometer.x = acc.x;
    m_rawAccelerometer.y = acc.y;
    m_rawAccelerometer.z = acc.z;

    m_rawMagnetometer.x = magn.x;
    m_rawMagnetometer.y = magn.y;
    m_rawMagnetometer.z = magn.z;
}

void DeadReckoning::addSensorData(const SensorMeasurement& acc, const SensorMeasurement& linacc, const SensorMeasurement& magn) {
    if (m_last == boost::posix_time::not_a_date_time)
        m_last = boost::posix_time::microsec_clock::local_time();
    m_curr = boost::posix_time::microsec_clock::local_time();

    updateRawSensorValues(acc, linacc, magn);
    
    // filter sensor data and use accelerometer data als linear acceleration if the latter is not available due to cell phone hardware
    m_accelerometer = m_lowPass.filterRaw(m_filterWeightAcc, acc, ACCELEROMETER_DATA);

    if (linacc.x == 0.f && linacc.y == 0.f && linacc.z == 0.f) {
        m_linearAcceleration = m_lowPass.filterRaw(m_filterWeightAcc, acc, LINEAR_ACCELERATION_DATA);
    } else {
        m_linearAcceleration = m_lowPass.filterRaw(m_filterWeightAcc, linacc, LINEAR_ACCELERATION_DATA);
    }

    m_magnetometer = m_lowPass.filterRaw(m_filterWeightMag, magn, MAGNETOMETER_DATA);
    
    calculateDelta();
}


bool DeadReckoning::detectMovement() const {
    const float &x = m_linearAcceleration.x;
    const float &y = m_linearAcceleration.y;
    const float &z = m_linearAcceleration.z;

    // We compare the square of the length and sensitivity, because we only need to know whether it's greater or not
    const float vectorLength = x*x + y*y + z*z;

    bool isMoving = (vectorLength >= m_detectionSensitivity);

    return isMoving;
}

// Implementation of getRotationMatrix function from android SensorManager
bool DeadReckoning::getRotationMatrix(V2d* rotation) const {

    float Ax = m_accelerometer.x;
    float Ay = m_accelerometer.y;
    float Az = m_accelerometer.z;

    const float& Ex = m_magnetometer.x;
    const float& Ey = m_magnetometer.y;
    const float& Ez = m_magnetometer.z;

    float Hx = Ey*Az - Ez*Ay;
    float Hy = Ez*Ax - Ex*Az;
    float Hz = Ex*Ay - Ey*Ax;
    
    const float normH = sqrt(Hx*Hx + Hy*Hy + Hz*Hz);
    if (normH < 0.1) {
        return false;
    }
    
    const float invH = 1/normH;
    Hx *= invH;
    Hy *= invH;
    Hz *= invH;

    const float invA = 1/sqrt(Ax*Ax + Ay*Ay + Az*Az);
    Ax *= invA;
    Ay *= invA;
    Az *= invA;
    
    const float Mx = Ay*Hz - Az*Hy;
    const float My = Az*Hx - Ax*Hz;
    const float Mz = Ax*Hy - Ay*Hx;
            
    // fill in matrix
    double matrix[3][3] = {
        { Hx, Hy, Hz },
        { Mx, My, Mz },
        { Ax, Ay, Az }
    };
    
    rotation->x = matrix[0][1];
    rotation->y = matrix[1][1];

    return true;
}


float DeadReckoning::calculateAzimuth() {
    // The azimuth will stay at -1 if we can't get a proper measurement
    float azimuth = -1;

    V2d rotation;
    if (getRotationMatrix(&rotation))
    {
        azimuth = atan2(rotation.x, rotation.y); // m_R[0][1], m_R[1][1]);
        if (azimuth < 0) { 
            azimuth += 2*M_PI;
        }
    }

    azimuth = m_lowPass.filterCompass(m_filterWeightCompass, azimuth);

    return azimuth;
}

void DeadReckoning::calculateDelta() {
    float azimuth = calculateAzimuth();
    bool moving = detectMovement();
    
    m_mutex.lock();

    // If not moving in a clear direction, don't set/change delta:
    if (moving && (azimuth > -1)) {
        
        boost::posix_time::time_duration diff = m_last - m_curr;
        float deltaUpdateRate = diff.total_milliseconds()/100.f;
        
        #ifdef TESTING
        // needed so tests do not fail
        deltaUpdateRate = 0.5f;
        #endif
        
        // Motion model in cm/s, deltaUpdateRate in s:
        m_distance = m_motionModel * deltaUpdateRate;
        // switched due to different coordinate system of compass and polar coordinates

        m_delta.x += m_distance * sin(azimuth - m_baseAngle);
        m_delta.y += m_distance * cos(azimuth - m_baseAngle);
    }
    
    m_mutex.unlock();
    
    m_last = m_curr;
}

GeoPoint DeadReckoning::getDelta() {
    m_mutex.lock();
    GeoPoint delta = GeoPoint::makePoint(m_delta.x, m_delta.y);
    m_delta = {0.0, 0.0};

    m_mutex.unlock();
        
    return delta;
}
