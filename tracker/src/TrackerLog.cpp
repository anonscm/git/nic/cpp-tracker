
#include "TrackerLog.h"

#include <string>
#include <sstream>
#include <deque>
#include <cstdarg>
#include <iostream>
#include <ctime>
#include <chrono>

static const char *levels[] = { 
    "NVM", "BTW", "FYI", "WTF", "OMG"
};

class TrackerLogSink
{
public:
    virtual void log(LogLevel level, const char* msg) const = 0;

protected:

    std::string timestamp() const {
		using namespace std::chrono;
		high_resolution_clock::time_point p = high_resolution_clock::now();
		milliseconds ms = duration_cast<milliseconds>(p.time_since_epoch());
		std::size_t fractional_seconds = ms.count() % 1000;

		std::time_t t = std::time(NULL);
		char buffer[80];
		std::strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", std::localtime(&t));

		char currentTime[84];
		sprintf(currentTime, "%s:%03d", buffer, (int)fractional_seconds);

		return currentTime;
    }

    std::string logline(LogLevel level, const char* msg) const {
        std::stringstream ss;
        ss << timestamp() << " " << levels[level] << ": " << msg << std::endl;
        return ss.str();
    }
};


class TrackerLogSinkNull: public TrackerLogSink {
    void log(LogLevel, const char*) const {
        // do nothing
    }
};

class TrackerLogSinkStderr: public TrackerLogSink {
    void log(LogLevel level, const char* msg) const {
        std::cerr << logline(level, msg);
    }

};

class TrackerLogSinkBuffer: public TrackerLogSink {

    void log(LogLevel level, const char* msg) const {
        ringbuffer.push_back(logline(level, msg));
        std::cerr << "ringbuffer size:" << ringbuffer.size() << std::endl;
        if (ringbuffer.size() > ringbufferMaxSize) {
            ringbuffer.pop_front();
        }
    }

private:
    mutable std::deque<std::string> ringbuffer;
    const int ringbufferMaxSize = 512;
};

static TrackerLogSink* sink = new TrackerLogSinkBuffer();

void TrackerLog::initializeSink(LogSink logSink)
{
    if (sink) {
        delete sink;
    }

    switch (logSink) {
        case SINK_STDERR: sink = new TrackerLogSinkStderr(); break;
        case SINK_NULL: sink = new TrackerLogSinkNull(); break;
        case SINK_BUFFER: sink = new TrackerLogSinkBuffer(); break;
    }
}

/* void TrackerLog::log(LogLevel level, const char* msg) {
    sink->log(level, msg);
} */

void TrackerLog::log(LogLevel level, const std::string& msg) {
    sink->log(level, msg.c_str());
}

void TrackerLog::log(LogLevel level, const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    char buffer[1024];
    vsnprintf(buffer, 1024, fmt, ap);
    sink->log(level, buffer);
}