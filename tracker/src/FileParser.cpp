#include "FileParser.h"
#include "Utils.h"

#include <lodepng.h>
#include <vector>
#include <fstream>

using namespace std;
using namespace json_spirit;

vector<Fingerprint> FileParser::getFingerprints(const string&filename, bool verbose) {
    if (verbose) {
	   cerr << "loading fingerprints: " << filename << endl;
    }

    vector<Fingerprint> fingerprints;
    mValue value;
    ifstream file(filename.c_str(), ifstream::in);

    if (read(file, value)) {
        mArray arr = value.get_array();
        fingerprints = parseJson(arr);
    } else {
        cerr << "Sensor data file couldn't be read! " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    file.close();

    return fingerprints;
}

vector<Fingerprint> FileParser::getFingerprintsFromString(const string& json) {
    // Could also be used in the interface of the tracker.
    vector<Fingerprint> fingerprints;
    mValue value;
    if (read(json, value)) {
        if(!value.is_null()){
            mArray arr = value.get_array();
            fingerprints = parseJson(arr);
        }
    } else {
        cerr << "Fingerprint string couldn't be read!"
             << " Check the format of the json-string." << endl;
    }
    
    return fingerprints;
}

bool FileParser::existsValue(const mObject& obj, const string& name )
{
    mObject::const_iterator i = obj.find(name);
 
    if (i == obj.end()) {
        return false;
    }
    if (i->first != name) {
        return false;
    }
 
    return true;
}

vector<Fingerprint> FileParser::parseJson(mArray arr) {
    // parses our current model for fingerprints
    // [{"histogram":{"$MAC_ADDRESS || $IBEACON_ID":{"$SIGNAL_STRENGTH": $VALUE, ...}, ...},  
    // "id": "$ID", "point": {"latitude": $VALUE, "longitude": $VALUE}}, ...]
    vector<Fingerprint> fingerprints;
    for (unsigned int i = 0; i < arr.size(); i++) {
        if(arr[i].is_null()){
            continue;
        }

        mObject obj = arr[i].get_obj();

        Histogram tmpHist;

        mObject tmp = obj.find("point")->second.get_obj();

        GeoPoint tmpPoint(0, 0);

        if (existsValue(tmp, "latitude") && existsValue(tmp, "longitude")) {
            //New fingerprint's version
            double latitude = tmp.find("latitude")->second.get_real();
            double longitude = tmp.find("longitude")->second.get_real();
            GeoPoint tmpPoint2(latitude, longitude);
            tmpPoint = tmpPoint2;
        } else {
            //Old fingerprint's version
            int latitude = tmp.find("mLatitudeE6")->second.get_int();
            int longitude = tmp.find("mLongitudeE6")->second.get_int();
            GeoPoint tmpPoint2(latitude, longitude);
            tmpPoint = tmpPoint2;
        }


        tmpHist.setId(obj.find("id")->second.get_str());

        map<string, map<int, float> > tmpHistEntry;
        map<int, float> tmpMeasurement;
        
        mObject::iterator it = obj.find("histogram")->second.get_obj().begin();
        for (; it != obj.find("histogram")->second.get_obj().end(); ++it) {

            mObject::iterator it2 = it->second.get_obj().begin();
            for (; it2 != it->second.get_obj().end(); ++it2) {
                tmpMeasurement.insert(pair<int, float>(atoi(it2->first.c_str()), it2->second.get_real()));
            }
            tmpHistEntry.insert(pair<string, map<int, float> >(it->first, tmpMeasurement));

            tmpMeasurement.clear();
        }

        tmpHist.setHistogram(tmpHistEntry);
        tmpHistEntry.clear();

        Fingerprint tmpFp(tmpHist, tmpPoint);
        fingerprints.push_back(tmpFp);
    }

    return fingerprints;
}

vector<SensorMeasurement> FileParser::getSensorMeasurements(const std::string& url, bool verbose) {
    if (verbose) {
	   cerr << "loading sensormeasurements: " << url << endl;
    }
    // parses a file with sensor data in it captured by the cell phone
    // $TIMESTAMP $XAXIS $YAXIS $ZAXIS
    vector<SensorMeasurement> measurements;

    ifstream file(url.c_str(), ifstream::in);
    if (!file.is_open()) {
        cerr << "Sensor data file couldn't be read! " << url << std::endl;
        exit (EXIT_FAILURE);
    }
  
    string line;
    SensorMeasurement tmpMeasurement;
    string tmp;
    vector<string> result;
    
    while(getline(file, line)){
        stringstream lineStream(line);
        while(getline(lineStream, tmp, ' ')) {
            result.push_back(tmp);
        }

        tmpMeasurement.timestamp = atol(result[0].c_str());
        tmpMeasurement.x = atof(result[2].c_str());
        tmpMeasurement.y = atof(result[3].c_str());
        tmpMeasurement.z = atof(result[4].c_str());
        
        measurements.push_back(tmpMeasurement);
        result.clear();
    }

    file.close();

    return measurements;
}

vector<float> FileParser::getOccupancyGridmap(const string& path, unsigned int *width, unsigned int* height, bool verbose) {
    //holds the image data, 4 bytes per pixel RGBARGBARGBA....
    if (verbose) {
	   cerr << "loading ogm: " << path << std::endl;
    }
    vector<unsigned char> image;
    vector<float> gridmap;

    unsigned int w = 0, h = 0;
    unsigned error = lodepng::decode(image, w, h, path.c_str());

    if (error) {
        cerr << "lodepng decode error " << error << ": " << lodepng_error_text(error) << std::endl;
        return gridmap;
    }

    //gridmap data, 1 float per pixel, greyscale
    for(int i = 0; i < image.size(); i+=4){
        unsigned char r = image[i];
        unsigned char g = image[i+1];
        unsigned char b = image[i+2];

        //in hsv color space (hue, saturation, value), value describes the grayscale in the interval [0,1]
        gridmap.push_back(Utils::greyscaleNormalized(r, g, b));
    }

    *width = w;
    *height = h;

    return gridmap;
}
