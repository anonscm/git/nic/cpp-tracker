#include "GeoPoint.h"
#include <cstdint>
#include <iostream>

using namespace std;

GeoPoint::GeoPoint() {
    reset();
}

GeoPoint::GeoPoint(double latitude, double longitude) {
    reset();
    m_latitude = latitude;
    m_longitude = longitude;
}

GeoPoint::GeoPoint(int latitudeE6, int longitudeE6) {
    reset();
    m_latitude = (double)latitudeE6 / (double)COMPATIBILITY_SCALE;
    m_longitude = (double)longitudeE6 / (double)COMPATIBILITY_SCALE;
}

#ifdef TESTING
GeoPoint::GeoPoint(int latitudeE6, int longitudeE6, double distance) {
    reset();
    m_latitude = (double)latitudeE6 / (double)COMPATIBILITY_SCALE;
    m_longitude = (double)longitudeE6 / (double)COMPATIBILITY_SCALE;
    m_distance = distance;
}
#endif


GeoPoint GeoPoint::makePoint(double x, double y) {
    GeoPoint gp;
    gp.setXY(x,y);
    return gp;
}

void GeoPoint::reset() {
    m_latitude = 0;
    m_longitude = 0;
    m_distance = 0;
    xyCache = {0,0};
}

double GeoPoint::getX() const {
    // Converts the longitude into an x value that is better understood by the tracker. This x value represents the
    // distance from Longitude 0 to the GeoPoints location as a Longitude value. This will also prevent
    // problems because the world is round, not flat and therefore cannot be easily split into even squares.

    if (xyCache.x == 0) {
        xyCache.x = m_longitude * getLongitudeDistanceInMeters();
    }

    return xyCache.x;
}

double GeoPoint::getY() const {
    // Converts the latitude into an y value that is better understood by the tracker. This y value represents the
    // distance from Latitude 0 to the GeoPoints location as a Latitude value. This will also prevent
    // problems because the world is round, not flat and therefore cannot be easily split into even squares.

    if (xyCache.y == 0) {
        xyCache.y = m_latitude * LATITUDE_LINE_DISTANCE;
    }

    return xyCache.y;
}

void GeoPoint::setXY(double x, double y) {
    // Converts given x and y coordinates into a longitude and a latitude in degree format and sets them.
    // This functionality is contained within a single function because the call order is of the utmost importance as
    // x (the distance to Longitude 0) depends on y (the distance to Latitude 0) resulting in a meaningless x if y is not set.
    
    // IMPORTANT: setY() MUST be called BEFORE setX()
    setY(y);
    setX(x);

    xyCache = {0, 0};
}

void GeoPoint::setX(double x) {
    m_longitude = x / getLongitudeDistanceInMeters();
}

void GeoPoint::setY(double y) {
    m_latitude = y / LATITUDE_LINE_DISTANCE;
}

double GeoPoint::getLatitudeInRadians() const {
    return m_latitude * M_PI / 180.f;
}

double GeoPoint::getLongitudeDistanceInMeters() const {
    const double latitudeInRadians = getLatitudeInRadians();

    double longitudeDistanceInMeters = (EARTH_SHAPE_CONSTANTS[0] * cos(latitudeInRadians)) +
            (EARTH_SHAPE_CONSTANTS[1] * cos(3 * latitudeInRadians)) +
            (EARTH_SHAPE_CONSTANTS[2] * cos(5 * latitudeInRadians));

    return longitudeDistanceInMeters;
}

bool GeoPoint::equals(const GeoPoint& other) {
    // Y needs to be tested before X because X changes based on the value of Y. So if Y is not the same, X will
    // likely not be the same either.
    return (getY() == other.getY()) &&
           (getX() == other.getX()) &&
           (m_distance == other.m_distance);
}

int GeoPoint::compareTo(const GeoPoint& other) {
    if (m_distance > other.m_distance) {
        return 1;
    }
    if (m_distance < other.m_distance) {
        return -1;
    }
    // When the QF distance is the same we select an arbitrary order based on the coordinates, in order to be
    // consistent with equals (i.e. compareTo should not return 0 when equals returns false).
    //int64_t = platform independent 64 bit integer type in c++ (= long in java)
    //long = 32 bits on 32 bit systems and 64 bits on 64 bit systems
    //long long = 64 bits on 32 bit systems
    int64_t a = (getLatitudeE6() + (((int64_t) getLongitudeE6()) << 32));
    int64_t b = (other.getLatitudeE6() + (((int64_t) other.getLongitudeE6()) << 32));
    if (a > b) {
        return 1;
    }
    if (a < b) {
        return -1;
    }

    return 0;
}

int GeoPoint::compare(const GeoPoint& pointX, const GeoPoint& pointY) {
    return (pointX.m_distance < pointY.m_distance);
}

double GeoPoint::calculateEuclideanDistanceTo(const GeoPoint&otherPoint) const {
    // Calculates the euclidean distance in two dimensional space between this point and the given point, in meters.
    const double dx = otherPoint.getX() - getX();
    const double dy = otherPoint.getY() - getY();

    const double distance = sqrt((dx * dx) + (dy * dy));

    return distance;
}

double GeoPoint::calculateGreatCircleDistanceTo(const GeoPoint& otherPoint) const {
    // Calculates the great circle distance in two dimensional space on a sphere between this point and the given point, in cm.
    // haversine formula
    double lat1, lat2, lon1, lon2, radLat1, radLat2, deltaLat, deltaLon;
    double a, c, distance;
    lat1 = getLatitude();
    lat2 = otherPoint.getLatitude();
    lon1 = getLongitude();
    lon2 = otherPoint.getLongitude();
    radLat1 = lat1 / 180 * M_PI;
    radLat2 = lat2 / 180 * M_PI;
    deltaLat = (lat2 - lat1) / 180 * M_PI;
    deltaLon = (lon2 - lon1) / 180 * M_PI;
    a = sin(deltaLat/2) * sin(deltaLat/2) + cos(radLat1) * cos(radLat2) * sin(deltaLon/2) * sin(deltaLon/2);
    c = 2 * atan2(sqrt(a), sqrt(1-a));
    distance = 1000 * 6371 * c;

    return distance;
}
