#ifndef TRACKER_VERSION_H
#define	TRACKER_VERSION_H

// NB: If you change the tracker version number here, you need to do three additional things:
// 
// a) go to invio-base/android/base/build.gradle and change the dependency version of 'de.tarent.invio.tracker.android.tracker' 
//    there. Don't forget to commit+push this change!
// c) go to monitorserver/monitorserver/pom.xml and change the dependency for 'de.tarent.invio.tracker.android.tracker' as well. Again, don't
//    forget to commit+push!
// b) tell the iOS developers about this change, because they need to run 'pod update' in their project to pick up the new
//    version (they'll know what this means)

// This is the major release number. It is set manually, here:
#define INVIO_TRACKER_VERSION_MAJOR 0
// This is the version of the tracker-app-interface. It is set manually, here:
#define INVIO_TRACKER_VERSION_INTERFACE 8
// This is the minor release number. It is set manually, here:
#define INVIO_TRACKER_VERSION_MINOR 7

// This is the build number from the main jenkins job
#ifndef INVIO_TRACKER_BUILD_NUMBER
#define INVIO_TRACKER_BUILD_NUMBER 0
#endif
// This is the build number of the deploy job, different for android and ios
#ifndef INVIO_TRACKER_DEPLOY_NUMBER
#define INVIO_TRACKER_DEPLOY_NUMBER 0
#endif
// This is the git hash from the commit used to build the tracker library
#ifndef INVIO_TRACKER_GIT_HASH
#define INVIO_TRACKER_GIT_HASH "debug"
#endif

// See: https://evolvis.org/plugins/mediawiki/wiki/nic/index.php/Versionnumbers

#endif	/* TRACKER_VERSION_H */

