#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <vector>
#include <string>

#include "json_spirit.h"

#include "Fingerprint.h"
#include "SensorMeasurement.h"

/**
 * Loads and parses fingerprints, sensor data and occupancy gridmap from file.
 * Only used for the standalone version of the tracker!
 */
class FileParser {    
public:
    
    static std::vector<Fingerprint> getFingerprints(const std::string&filename, bool verbose = false);
    static std::vector<Fingerprint> getFingerprintsFromString(const std::string& json);
    static std::vector<SensorMeasurement> getSensorMeasurements(const std::string& path, bool verbose = false);

    /**
     * Gets the occupancy grid map.
     *
     * @param path the path to the file, supported file formats: .png
     * @param width pointer to an unsigned integer that stores the width of the file
     * @param height pointer to an unsigned integer that stores the height of the file
     * @return the gridMap, normalized to the range [0, 1]
     */
    static std::vector<float> getOccupancyGridmap(const std::string& path, unsigned int* width, unsigned int* height, bool verbose = false);
private:
    
    static bool existsValue(const json_spirit::mObject& obj, const std::string& name );
    static std::vector<Fingerprint> parseJson(json_spirit::mArray arr);
};

#endif	/* FILEPARSER_H */

