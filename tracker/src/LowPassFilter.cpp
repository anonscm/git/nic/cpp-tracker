#include "LowPassFilter.h"

#include <cfloat>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

LowPassFilter::LowPassFilter() {
    m_filteredCompassValue = FLT_MAX;
    
    m_filteredValues[ACCELEROMETER_DATA] = SensorMeasurement(FLT_MAX, FLT_MAX, FLT_MAX);
    m_filteredValues[MAGNETOMETER_DATA] = SensorMeasurement(FLT_MAX, FLT_MAX, FLT_MAX);
    m_filteredValues[LINEAR_ACCELERATION_DATA] = SensorMeasurement(FLT_MAX, FLT_MAX, FLT_MAX);
}


// weight = 1 disables filter, ie. the input values are passed back unchanged
// weight = 0 disregards the input value, ie. return value does not change from previous call
SensorMeasurement LowPassFilter::filterRaw(float weight, const SensorMeasurement& sensor, SensorType sensorType) {
    
    SensorMeasurement filtered = m_filteredValues[sensorType];
    
    SensorMeasurement lastMean = ((filtered.x != FLT_MAX) && (filtered.y != FLT_MAX) && (filtered.z != FLT_MAX)) ? filtered : sensor;

    const float weight1 = 1.f - weight;    
    SensorMeasurement newMean;
    newMean.x = lastMean.x * weight1 + sensor.x * weight;
    newMean.y = lastMean.y * weight1 + sensor.y * weight;
    newMean.z = lastMean.z * weight1 + sensor.z * weight;
    
    m_filteredValues[sensorType] = newMean;
    
    return newMean;
} 
 
float LowPassFilter::filterCompass(float weight, float newCompassValue) { 
    const float lastMean = (m_filteredCompassValue != FLT_MAX) ? m_filteredCompassValue : newCompassValue;
    
    // consider the transition between <360° and >0°
    const float weight1 = 1.f - weight;
    const float newX = cos(lastMean) * weight1 + cos(newCompassValue) * weight;
    const float newY = sin(lastMean) * weight1 + sin(newCompassValue) * weight;
    
    float newMean = atan2(newY, newX);
    if (newMean < 0) {
        newMean += 2*M_PI;
    }
    
    m_filteredCompassValue = newMean;
    
    return m_filteredCompassValue;
}