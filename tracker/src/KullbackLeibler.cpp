#include "KullbackLeibler.h"

#include <cmath>

// TODO: this code is unused, remove it

using namespace std;

KullbackLeibler::KullbackLeibler() 
    : m_symmetricalDivergence(0.0),
      m_confidence(0.0) {
}

KullbackLeibler::~KullbackLeibler() {
}

float KullbackLeibler::compareHistograms(RouterLevelHistogram p, RouterLevelHistogram q) {
    RouterLevelHistogram cutP = p;
    RouterLevelHistogram cutQ = q;
    
    cutP.intersect(q);
    cutQ.intersect(p);
    
    RouterLevelHistogram normalP, normalQ;
    normalP.setRouterLevelHistogram(cutP.normalize());
    normalQ.setRouterLevelHistogram(cutQ.normalize());
    
    m_symmetricalDivergence = calculateDirectionalDivergence(normalP, normalQ) + calculateDirectionalDivergence(normalQ, normalP);
    m_symmetricalDivergence /= normalP.getSize();
    m_confidence = (normalP.getSize() + normalQ.getSize()) / (p.getSize() + q.getSize());
    
    return m_symmetricalDivergence;
}

float KullbackLeibler::calculateDirectionalDivergence(RouterLevelHistogram p, RouterLevelHistogram q) {
    float divergence = 0.f;
    
     for (map<string, float>::iterator it = p.getRouterLevelHistogram().begin(); it != p.getRouterLevelHistogram().end(); ++it) {
         string bssId = it->first;
         float pLevel = it->second;
         float qLevel = q.getRouterLevelHistogram().at(bssId);
         
         divergence += log(pLevel / qLevel) * pLevel;
     }
    
    return divergence;
}
