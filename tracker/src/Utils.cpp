#include "Utils.h"

unsigned char Utils::greyscale(unsigned char r, unsigned char g, unsigned char b){
    unsigned char rgbMax = r > g ? (r > b ? r : b) : (g > b ? g : b);
    return rgbMax;
}

float Utils::greyscaleNormalized(unsigned char r, unsigned char g, unsigned char b){
    return (float)greyscale(r, g, b) / 255.0f;
}
