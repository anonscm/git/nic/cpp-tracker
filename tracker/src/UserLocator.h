#ifndef USERLOCATOR_H_
#define USERLOCATOR_H_
#include <fstream>
#include <string>
#include <vector>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/asio.hpp>

#include "Fingerprint.h"
#include "GeoPoint.h"
#include "ParticleFilter.h"
#include "FingerprintPreprocessor.h"
#include "History.h"

/**
 * Localisation mode: Histogram matching.
 */
static const int LOCALISATION_WIFI = 0;

/**
 * Localisation mode: Dead reckoning.
 */
static const int LOCALISATION_DEADRECKONING = 1;

/**
 * Localisation mode: Particle filter.
 */
static const int LOCALISATION_PARTICLEFILTER = 2;

static const float MAPSCALE_DIVISOR = 1000.0f; // incoming scale is in km

class DeadReckoning;
class OccupancyGridMap;

/**
 * Manages the tracking by determining the wifi/bluetooth position and executing dead reckoning and the particle filter in a given time interval.
 * Also distributes some input/configurations to those parts.
 */
class UserLocator {
public:
    UserLocator(int localisationMode);
    ~UserLocator();

    void startTracking();
    void stopTracking();
    void localisationCallback();
    
    void addHistogram(Histogram hist);
    void localisationIteration();
    // public for testing
    std::vector<GeoPoint> calculateDistances(Histogram hist);
    std::vector<GeoPoint> getNearestNeighbours(std::vector<GeoPoint>& distanceSet);
    std::vector<GeoPoint>  removeOutliers(std::vector<GeoPoint>& nearestNeighbours);
    GeoPoint interpolatePosition(std::vector<GeoPoint>& points);
    
    void trackWithoutParticleFilter();
    void trackWithParticleFilter();
    
    void filterPosition(GeoPoint position);
    void initCenter();
    
    void setFingerprints(std::vector<Fingerprint> fingerprints);
    
    void setLocalisationMode(int mode) {
        m_localisationMode = mode;
    }

    DeadReckoning* getDeadReckoning() const {
        return m_deadReckoning;
    }
    
    void setScale(float scale) {
        m_scale = scale / MAPSCALE_DIVISOR;
    }
    
    float getScale() const {
        return m_scale;
    }
    
    GeoPoint getCurrentPosition() const {
        return m_currentPosition;
    }

    bool getIsPositionComputedWithHist() const {
        return m_isPositionComputedWithHist;
    }

    bool getIsTracking() const {
        return m_isTracking;
    }
    
    void resetParticleFilter() {
        m_particleFilter.clearParticles();
        m_pfInit = false;
    }
    
    void setSufficientNumMeasurements(int size) {
        m_sufficientNumMeasurements = size;
    }
    
    // NB: setBoundingBox needs to be called *before* setGridMap!t
    void setGridMap(OccupancyGridMap* gridMap) {
        m_gridMap = gridMap;
        m_gridMap->setBoundingBox(m_boundingBoxMin, m_boundingBoxMax);
    }
        
        
#ifdef TESTING
    void setDeadReckoning(DeadReckoning* dr) {
        m_deadReckoning = dr;
    }
    
    void setMovementThreshold(float mt) {
        m_movementThreshold = mt;
    }
#endif

    // only public for unit tests!
    void setCurrentPosition(const GeoPoint& newPosition, bool force = false);
    
    OccupancyGridMap* getGridMap() const {
        return m_gridMap;
    }
    
    void setBoundingBox(GeoPoint min, GeoPoint max) {
        m_boundingBoxMin = min;
        m_boundingBoxMax = max;

        float diffX = abs(m_boundingBoxMin.getX() - m_boundingBoxMax.getX());
        float diffY = abs(m_boundingBoxMin.getY() - m_boundingBoxMax.getY());
    
        m_centerPosition.setXY(m_boundingBoxMin.getX() + diffX/2, m_boundingBoxMin.getY() + diffY/2);
    }
    
    // for testing purposes
    GeoPoint getNextWifiPosition() {
        return m_nextWifiPosition;
    }
    
#ifdef STANDALONE
    void setWritingPositionsToFile(bool val) {
        m_writeToFile = val;
    }
#endif
       
    std::vector<Particle> particles() const;
    
private:
    GeoPoint m_currentPosition;
    GeoPoint m_centerPosition;
    GeoPoint m_nextWifiPosition;
    DeadReckoning* m_deadReckoning;
    ParticleFilter m_particleFilter;
    History m_history;
    
    OccupancyGridMap* m_gridMap;
    GeoPoint m_boundingBoxMin, m_boundingBoxMax;
        
    std::vector<Fingerprint> m_fingerprints;
    FingerprintPreprocessor m_fingerprintPreprocessor;
    int m_numPreprocessedFingerprint;

    Histogram m_hist;
    
    int m_updateRate;
    float m_scale;
    bool m_isTracking;
    int m_localisationMode;
    bool m_newHist;
    bool m_isPositionComputedWithHist;
    int m_numNeighbours;
    bool m_useOutlierDetection;
    float m_outlierDetectionThreshold;

    void init(int localisationMode);

#ifdef STANDALONE
    bool m_writeToFile;
    std::ofstream m_positionFile;
    std::ofstream m_fpposFile;
    int m_histnr;
    float m_meanerror;
    float m_meanerrorMeter;
#endif

    float m_movementThreshold;
    
    boost::mutex m_mutex;
    boost::thread m_localisationThread;
    boost::asio::io_service m_io;
    
    int m_numMeasurements;
    int m_sufficientNumMeasurements;
    
    bool m_pfInit;
    bool m_drInit;
};

#endif /* USERLOCATOR_H_ */
