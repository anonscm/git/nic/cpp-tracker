 #include "Tracker.h"
#include "DeadReckoning.h"
#include "UserLocator.h"
#include "TrackerConfig.h"
#include "Version.h"
#include "OccupancyGridMap.h"

#include <boost/lexical_cast.hpp>
#include <limits>


using namespace std;

/***
 *
 * TODOs for later - some of these are API-breaking changes
 * - make initialization more explicit and combine data that must be set together (e.g., OGM and bounding box)
 * - make memory ownership semantics explicit, don't delete memory that's used elsewhere (as eg. in setOGM, below)
 * 
 */

/*
 * Watch out for timing problems!
 */
 
static const float NO_ANGLE = std::numeric_limits<float>::min();
static const float NO_SCALE = -1.0;

Tracker::Tracker() {
    
    m_debugLogEnabled = false;
      
    // default (invalid) north angle is FLOAT_MIN (will be set later, if the float-min wasn't overwritten)
    m_northAngle = NO_ANGLE;
    m_scale = NO_SCALE;

    TrackerConfig* config = TrackerConfig::getInstance();
    m_localisationMode = config->getInt(LOCALISATION_MODE);
    
    m_userLocator = new UserLocator(m_localisationMode); 
    m_userLocator->setScale(m_scale);

    // initialise a default grid map with ([], 0, 0) -> no obstacles are considered, but stuff will work
    m_gridMap = new OccupancyGridMap(); 

    m_userLocator->setGridMap(m_gridMap);
}

Tracker::~Tracker() {
    delete m_userLocator;
    delete m_gridMap;
}

int Tracker::start() {
    stop();
    // all setters only have an effect after calling start(), parameters cannot be changed during tracking!

    // create a new UserLocator and re-initialize it so that configuration parameter changes
    // will have an effect (exeption: ogm pixel values)
    TrackerConfig* config = TrackerConfig::getInstance();
    m_localisationMode = config->getInt(LOCALISATION_MODE);

    delete m_userLocator;
    m_userLocator = new UserLocator(m_localisationMode);
    m_userLocator->setScale(m_scale);

    if (m_northAngle == NO_ANGLE) {
        m_userLocator->getDeadReckoning()->setBaseAngle(0);
    } else {
        m_userLocator->getDeadReckoning()->setBaseAngle(m_northAngle);
    }

    m_userLocator->setFingerprints(m_fingerprints);

    // it is important to call setBoundingBox() before calling setGridMap()!
    m_userLocator->setBoundingBox(m_min, m_max);
    m_userLocator->setGridMap(m_gridMap);

    // check if everything is in order for starting the localization
    switch (m_localisationMode) {
        case LOCALISATION_WIFI:
            // only fingerprints are needed for this to work
            if (m_fingerprints.empty()) {
                cerr << "No fingerprints available. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            break;
        case LOCALISATION_DEADRECKONING:
            // only scale, northangle and the bounding box (for initCenter) are needed for this to work
            if (m_scale <= 0) {
                cerr << "Scale value is invalid. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            if ((m_min.getLatitudeE6() == 0 && m_min.getLongitudeE6() == 0) || (m_max.getLatitudeE6() == 0 && m_max.getLongitudeE6() == 0)) {
                cerr << "No bounding box set. No initialisation for dead reckoning possible. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            // if scale and north angle are not set, the default values are taken -> tracker works but with most likely wrong values
            if (m_scale == NO_SCALE || m_northAngle == NO_ANGLE) {
                m_userLocator->startTracking();
                cerr << "Check values for scale and north angle. Tracking is started with default values for scale (1:1) or north angle (0 deg)." << endl;
                return OPTIONAL_MISSING;
            }
            break;
        case LOCALISATION_PARTICLEFILTER:
            // fingerprints, scale, northangle, bounding box and occupancy grid map optional
            if (m_fingerprints.empty()) {
                cerr << "No fingerprints available. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            if (m_scale <= 0) {
                cerr << "Scale value is invalid. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            if ((m_min.getLatitudeE6() == 0 && m_min.getLongitudeE6() == 0) || (m_max.getLatitudeE6() == 0 && m_max.getLongitudeE6() == 0)) {
                cerr << "No bounding box set. No initialisation for dead reckoning possible. Tracker malfunction. BEEP BEEP." << endl;
                return MANDATORY_MISSING;
            }
            if (!m_userLocator->getGridMap()->ogmProperlyInitialised()) {
                cout << "Starting invio tracker version " << version() << endl;
                m_userLocator->startTracking(); // start tracking anyways
                cerr << "No bounding box or occupancy grid map set. Tracking will work without obstacle avoidance." << endl;
                return OPTIONAL_MISSING;
            }
            // if scale and north angle are not set, the default values are taken -> tracker works but with most likely wrong values
            if (m_scale == NO_SCALE || m_northAngle == NO_ANGLE) {
                cout << "Starting invio tracker version " << version() << endl;
                m_userLocator->startTracking(); // start tracking anyways
                cerr << "Check values for scale and north angle. Tracking is started with default values for scale (1:1) or north angle (0 deg)." << endl;
                return OPTIONAL_MISSING;
            }
            break;
    }
    
    cout << "Starting invio tracker version " << version() << endl;
    m_userLocator->startTracking();
    return SUCCESS;
}


void Tracker::stop() {
    cout << "Stopping invio tracker" << endl;
    m_userLocator->stopTracking();
}


void Tracker::addSensorData(
    float accX, float accY, float accZ,
    float linAccX, float linAccY, float linAccZ,
    float magX, float magY, float magZ) {

    SensorMeasurement acc(accX, accY, accZ);
    SensorMeasurement linAcc(linAccX, linAccY, linAccZ);
    SensorMeasurement magn(magX, magY, magZ);

    m_userLocator->getDeadReckoning()->addSensorData(acc, linAcc, magn);
}


void Tracker::addHistogram(Histogram histogram) {
    m_userLocator->addHistogram(histogram);

    if (m_debugLogEnabled) {
        cout << "addHistogram" << endl;
        Histogram hg = histogram;
        cout << "  Histogram-Size: " << hg.size() << endl;
        for (map<string, map<int, float> >::iterator it = hg.begin(); it != hg.end(); ++it) {
            cout << "    Histogram[" << it->first << "]:" << endl;
            for (map<int, float>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                cout << "      \"" << it2->first << "\":" << it2->second << endl;
            }
        }
    }
}


void Tracker::setFingerprints(const vector<Fingerprint>& fingerprints) {
    m_fingerprints = fingerprints;

    if (m_debugLogEnabled) {
        cout << "setFingerprints" << endl;
        for (int i = 0; i < fingerprints.size(); i++) {
            Fingerprint fp = fingerprints[i];
            cout << "Fingerprint-ID: " << fp.getId() << endl;
            Histogram hg = fp.getHistogram();
            cout << "  Histogram-Size: " << hg.size() << endl;
            for (map<string, map<int, float> >::iterator it = hg.begin(); it != hg.end(); ++it) {
                cout << "    Histogram[" << it->first << "]:" << endl;
                for (map<int, float>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                    cout << "      \"" << it2->first << "\":" << it2->second << endl;
                }
            }
        }
    }
}

void Tracker::setScale(float scale) {
    m_scale = scale;

    if (m_debugLogEnabled) {
        cout << "setScale " << scale << endl;
    }
}


void Tracker::setNorthAngle(float northAngle) {
    m_northAngle = northAngle;

    if (m_debugLogEnabled) {
        cout << "setNorthAngle " << northAngle << endl;
    }
}


GeoPoint Tracker::getCurrentPosition() {
        GeoPoint position = m_userLocator->getCurrentPosition();
        
        if (m_debugLogEnabled) {
            cout << "getCurrentPosition with Long: " << position.getLongitudeE6() << " Lat: " << position.getLatitudeE6() << endl;
        }
        return position;
}


// TODO: the difference between getPath and getShortestPath is not obvious.
vector<GeoPoint> Tracker::getPath(GeoPoint start, GeoPoint goal) {
    if (!m_gridMap->ogmProperlyInitialised())
        cerr << "No proper occupancy grid map or bounding box provided. Planning malfunction. BEEP BEEP." << endl;
    
    vector<GeoPoint> path = m_gridMap->planPath(start, goal);
    return path;
}


// TODO: the difference between getPath and getShortestPath is not obvious.
vector<GeoPoint> Tracker::getShortestPath(GeoPoint start, vector<GeoPoint>& goals) {
    if (!m_gridMap->ogmProperlyInitialised())
        cerr << "No proper occupancy grid map or bounding box provided. Planning malfunction. BEEP BEEP." << endl;
    
    vector<GeoPoint> path = m_gridMap->planShortestPath(start, goals);
    return path;
}


void Tracker::setOccupancyGridMap(vector<float> pixelValues, int width, int height) {
    delete m_gridMap;
    m_gridMap = new OccupancyGridMap(pixelValues, width, height);
    // for planning
    m_gridMap->setBoundingBox(m_min, m_max);
    
    if (m_debugLogEnabled) {
        cout << "setOccupancyGridmap " << width << " " << height << endl;
    }
}


void Tracker::setBoundingBox(double minLat, double minLong, double maxLat, double maxLong) {
    m_min = GeoPoint(minLat, minLong);
    m_max = GeoPoint(maxLat, maxLong);  
    
    // for planning
    m_gridMap->setBoundingBox(m_min, m_max);
    
    if (m_debugLogEnabled) {
        cout << "setBoundingBox " << minLat << " " << minLong << " " << maxLat << " " << maxLong << endl;
    }
}

void Tracker::setLocalisationMode(int mode) {
    m_localisationMode = mode;
    TrackerConfig::getInstance()->setValue(LOCALISATION_MODE, boost::lexical_cast<string>(mode));
}

void Tracker::setConfigParameter(const string& param, const string& value) {
    TrackerConfig::getInstance()->setValue(param, value);
    
    if (m_debugLogEnabled) {
        cout << "setConfigParameter " << param << " : " << value << endl;
    }
}

string Tracker::getConfigParameter(const string& param) const { // NOSONAR
    return TrackerConfig::getInstance()->getString(param);
}

vector<TrackerConfigParameter> Tracker::getConfig() const {
    return TrackerConfig::getInstance()->getConfig();
}

void Tracker::setDebugLogEnabled(bool enabled) {
    m_debugLogEnabled = enabled;
}


vector<Particle> Tracker::particles() const {
    return m_userLocator->particles();
}

string Tracker::version() {
    string gitHash(INVIO_TRACKER_GIT_HASH);

    if (gitHash.length() > 7){
        gitHash = gitHash.substr(0, 7);
    }

    std::ostringstream ver;
    ver << INVIO_TRACKER_VERSION_MAJOR << "."
        << INVIO_TRACKER_VERSION_INTERFACE << "."
        << INVIO_TRACKER_VERSION_MINOR << "."
        << INVIO_TRACKER_BUILD_NUMBER << "."
        << INVIO_TRACKER_DEPLOY_NUMBER << "-"
        << gitHash;

    return ver.str();
}
