#ifndef FINGERPRINT_PREPROCESSOR_H_
#define FINGERPRINT_PREPROCESSOR_H_

#include "Fingerprint.h"
#include <vector>

struct FingerprintWithMean {
    std::map<std::string, int> transmitterMeanValues;
    double score;
    int scoreCount;
    Fingerprint fingerprint;

    FingerprintWithMean(const Fingerprint& fp) {
        fingerprint = fp;
        score = scoreCount = 0;
    }

    bool operator < (const FingerprintWithMean& other) const {
        // sort by score, then by name
        // this is necessary so that we can rely on the ordering in our unit tests.
        return score != other.score ? score < other.score : fingerprint.getId() < other.fingerprint.getId();
    }
};

class FingerprintPreprocessor {
public:
    FingerprintPreprocessor();
    FingerprintPreprocessor(const std::vector<Fingerprint>& fingerprints);

    void setFingerprints(const std::vector<Fingerprint>& fingerprints);

    const std::vector<Fingerprint>& getProcessedFingerprints() const {
        return m_processedFingerprints;
    }

    const std::vector<FingerprintWithMean>& getFingerprintsWithMeans() const {
        return m_fingerprintsWithMeans;
    }

    void excludeFingerprintsByMean(const Histogram& userHistogram, size_t maxCount);
    int getMeanOfTransmitterValues(const std::map<int, float>& transmitterValues) const;

private:
    void computeFingerprintsWithMeans();
    std::vector<Fingerprint> m_sourceFingerprints;
    std::vector<Fingerprint> m_processedFingerprints;
    std::vector<FingerprintWithMean> m_fingerprintsWithMeans;
};

#endif /* FINGERPRINT_PREPROCESSOR_H_ */
