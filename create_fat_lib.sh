#
# use libtool to combine all dependencies into one fat library 

mkdir -p build-ios/dist/lib

BOOST_LIBS=ext/boost-ios/lib/boost.a

LIBS_ARM="build-ios/arm/ext/json_spirit/Release-iphoneos/libjson_spirit.a \
build-ios/arm/tracker/Release-iphoneos/libtracker.a"

LIBS_X86="build-ios/x86/ext/json_spirit/Release-iphonesimulator/libjson_spirit.a \
build-ios/x86/tracker/Release-iphonesimulator/libtracker.a"

libtool -static $BOOST_LIBS $LIBS_ARM $LIBS_X86 -o build-ios/dist/lib/libtracker.a 2>&1 | sed -e "/has no symbols/D"

# copy the header from one of the two architecture directories

mkdir -p build-ios/dist/include
cp -r build-ios/arm/dist/include/* build-ios/dist/include
