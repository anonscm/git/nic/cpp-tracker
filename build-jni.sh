#!/bin/bash
#make make tracker library with jni support
os=`uname`
con="-"
arch=`uname -m`
dir=$os$con$arch

mkdir -p build-jni/$dir
cd build-jni/$dir
set -e
cmake -DBUILD_STANDALONE=true -DBUILD_TEST=false -DBUILD_JNI=true -G "Unix Makefiles" ../../
make install
cd -

#copy the library to the current arch resource folder
mkdir -p jni/linux/resources/lib/$dir
cp build-jni/$dir/dist/jni/libtracker-jni.so jni/linux/resources/lib/$dir
