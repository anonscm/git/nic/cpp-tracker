package de.tarent.invio.tracker.standalone;

import de.tarent.invio.entities.InvioGeoPointImpl;
import de.tarent.invio.tracker.Tracker;

public class Standalone {
    public static void main(String[] args){
        Tracker tracker = new Tracker();
        System.out.println(tracker.version());
    }
}
