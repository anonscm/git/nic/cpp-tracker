# Tracker 

### Requirements

### Linux

- gcc and g++ 4.8+
- cmake 3.x

### Windows

- MinGW-x64 at least with gcc 4.9.1
- MSYS 1.0.11
- boost 1.53+
- cmake 3.x

### MacOS X

- gcc and g++ 4.8+
- boost 1.53+
- cmake 3.x

### Android

- Android NDK r10b (https://dl.google.com/android/ndk/android-ndk32-r10b-linux-x86_64.tar.bz2)
- JDK 7+

### Cross-Compiling for Android on Windows

	sh build-android-win32.sh

### Cross-Compiling for Android on Linux

	export ANDROID_NDK=your_android_ndk_location
	export PATH=$ANDROID_NDK:$PATH
	
	chmod +x build-android.sh
	./build-android.sh
	
### Cross-Compiling for iOS on MacOS X

	sh build-ios.sh
	
### Compiling JNI for usage in Java

### Android JNI

- JNI is already build for Android using the Android cross-compiling build mechanism

### Linux JNI

For Linux x86_64 the included boost library is used. The build script in this case is:

	sh build-jni.sh

If you need a 32 bit version you currently need your own boost library,
installed locally/systemwide. That is used with this build script (which you
should _not_ use, when you have a 64 bit system and 64 bit boost, because
that will only cause confusion):

	sh build-jni-linux-local.sh
	
### Windows JNI

	build-jni-mingw32.bat

### Include Tracker lib in invio-base

	sh deploy-mavenLocal-android.sh 
	
- open [workspace]/invio-base/android/base/build.gradle and change version for 'de.tarent.invio.tracker.android' to the version in ~/.m2/repository/de/tarent/invio/tracker/android/tracker/ or to 'latest.integration' to always get the newest version
	
### Gradle dependency for Tracker

You need to build the JNI to install this dependency in your local repository

At the moment there are specific jars for Android, Windows and Linux.

	compile 'de.tarent.invio.tracker.linux:tracker:0.6.21'
	
For OS specific selection of the jar package you can use in gradle:

	import org.apache.tools.ant.taskdefs.condition.Os

	if (Os.isFamily(Os.FAMILY_WINDOWS)) {
		compile 'de.tarent.invio.tracker.windows:tracker:0.6.21'
	} else {
		compile 'de.tarent.invio.tracker.linux:tracker:0.6.21'
	}
	
For maven use

	<groupId>de.tarent.invio.tracker.linux</groupId>
	<artifactId>tracker</artifactId>
	<version>0.6.21</version>
	
### Docker Image

In the directory /docker is a Dockerfile which creates a Docker image that can be used to to compile the project (linux binaries, jni and android)
The Dockerfile itself is also useful as a documentation which packages and systems we use to compile this project.

To create an Docker image use inside /docker

	docker build -t image_name .

This is a onetime process and can take a while.

After successfully creating a Docker image you can start a temporary Docker container to build the project with the following command

	docker run --rm -v /your/host/path/cpp-tracker:/usr/dev/cpp-tracker -w /usr/dev/cpp-tracker image_name sh build.sh

Note that you need to replace /your/host/path that it points to the folder you are working in and image_name to the name you gave when creating the image.

If you are using Windows and boot2docker you need to specifiy the path with /c/Users/your/path and the folder needs to be somewhere in your Users directory.

For more information regarding Docker visit https://www.docker.com/
