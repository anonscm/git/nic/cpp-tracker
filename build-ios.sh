#!/bin/bash

mkdir -p build-ios/arm
cd build-ios/arm
cmake -DIOS_PLATFORM=OS -DCMAKE_TOOLCHAIN_FILE=../../cmake-modules/iOS.cmake -GXcode ../../
cd -
# build with xcode
xcodebuild -project build-ios/arm/cpptracker.xcodeproj/ -target install -configuration Release build GCC_PREPROCESSOR_DEFINITIONS='$GCC_PREPROCESSOR_DEFINITIONS IOS=1'

mkdir -p build-ios/x86
cd build-ios/x86
cmake -DIOS_PLATFORM=SIMULATOR -DCMAKE_TOOLCHAIN_FILE=../../cmake-modules/iOS.cmake -GXcode ../../
cd -
# build with xcode
xcodebuild -project build-ios/x86/cpptracker.xcodeproj/ -target install -configuration Release build GCC_PREPROCESSOR_DEFINITIONS='$GCC_PREPROCESSOR_DEFINITIONS IOS=1'

# mash all libraries/architectures together to a "fat lib" 
sh create_fat_lib.sh && [ "$1" = "-d" ] && ./deploy-iOS-local.sh

rm -rf build-ios/dist/lib/pkgconfig
