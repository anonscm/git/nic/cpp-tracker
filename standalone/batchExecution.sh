#!/bin/bash

# Shell script for creating parameters for the standalone tracker in batch
# mode (Optimized for ITAB), for multiple datasets and with multiple
# configurations. I.e. it just prints the parameter combinations, it doesn't
# run anything. 
# It also creates command for the evaluatePositions.py script.
# The script should have execution permission:
#    chmod u+x batchExecution.sh
# To run it just write: ./batchExecution.sh
# (which is basically the same as "bash batchExecution.sh", only shorter)

# This are the configurations for ITAB
SCALE=50
NORTH_ANGLE=81

# Giving absolute path
 SOURCES_PATH="/home/dseca/Schreibtisch/SEL-893.TrackerBug-2/ITAB/logsMovement/"
 OUTPUT_PATH="/home/dseca/Schreibtisch/SEL-893.TrackerBug-2/ITAB/logsMovement/"
# STANDALONE="/home/dseca/development/trackerProject/cpp-tracker/build/standalone/standalone"
# PYTHON_ACCURACY="python /home/dseca/development/trackerProject/cpp-tracker/plots/evaluatePositions.py"

# Giving relative path
#SOURCES_PATH="../res/itab-sources/"
#OUTPUT_PATH="../res/results/"
STANDALONE="../build/standalone/standalone"
PYTHON_ACCURACY="python ../plots/evaluatePositionsFromStandalone.py"

# Create output directory, if needed:
mkdir -p $OUTPUT_PATH

# The timestamps are the distinctive part of the filenames for one data set.
# We can give a list here, for all the datasets that we have.
#FILES=("2015-09-17_16-36-01")

FILES=("2015-09-17_15-05-16"
        "2015-09-17_15-14-38"
        "2015-09-17_15-49-31"
        "2015-09-17_16-09-09"
        "2015-09-17_16-20-37"
        "2015-09-17_16-36-01"
        "2015-09-17_17-06-15"
        "2015-09-18_09-40-01"
        "2015-09-18_12-06-07"
        "2015-09-18_13-35-08"
        "2015-09-18_13-56-50"
        "2015-09-18_14-07-09"
        "2015-09-18_14-55-56"
        "2015-09-18_15-22-49")


# Possible parameter ranges that we want to test:

# Parameters we want to set into the standalone tracker        
    # SENSOR_MEASUREMENT_UPDATE_RATE
    # WIFI_MEASUREMENT_UPDATE_RATE
    #
    # Here's an example from the tilesresource.xml for ITAB that specifies the bounding box,
    # which unfortunately (again) confuses x/y vs. latitude/longitude :(
    # <BoundingBox minx="57.74192954953035" miny="14.15308700000000" maxx="57.75710300000001" maxy="14.17525274773805"/>
    #
    # to use these values in the standalone tracker, assign them like this:
    # MIN_LON = miny, MAX_LON = maxy, MIN_LAT=minx, MAX_LAT=maxx
    #       BOUNDING_BOX_MIN_LON -> 14.15308700000000
    #       BOUNDING_BOX_MAX_LON -> 14.17525274773805
    #       BOUNDING_BOX_MIN_LAT -> 57.74192954953035
    #       BOUNDING_BOX_MAX_LAT -> 57.75710300000001

CONFIGURATIONS=("cluster_radius=200
                enable_outlier_detection=false
                has_gyroscope=true
                localisation_mode=2
                localisation_update_rate=200
                low_pass_filter_weight_accelerometer=1
                low_pass_filter_weight_compass=0.5
                low_pass_filter_weight_magnetometer=0.5
                motion_detection_threshold=0.5
                motion_model=200
                movement_threshold=200
                num_neighbours=5
                num_particles=150
                num_preprocessed_fingerprints=40
                ogm_pixel_black=0.0
                ogm_pixel_grey=0.5
                ogm_pixel_white=1.0
                outlier_detection_threshold=2
                pct_random_particles=20
                qfd_use_log_scale=false
                sigma_action=100
                sigma_init=1000
                sigma_random_particles=10000
                sigma_sensor=100
                sufficient_measurements_init=3
                position_output_freq=500
                bounding_box_min_lon=14.15308700000000
                bounding_box_max_lon=14.17525274773805
                bounding_box_min_lat=57.74192954953035
                bounding_box_max_lat=57.75710300000001
                sensor_measurement_update_rate=200
                wifi_measurement_update_rate=1000")

for FILE_SET in "${FILES[@]}"
do
    #echo "FILE_SET: "$FILE_SET
    OCCUPANCY_GRID_MAP_FILE=$SOURCES_PATH"../data/ogm_ITAB.png"
    FINGERPRINT_FILE=$SOURCES_PATH$FILE_SET"/fp.json"
    GROUND_TRUTH_FILE=$SOURCES_PATH$FILE_SET"/groundTruthLongLat.txt"

    ACCELEROMETER_FILE=$SOURCES_PATH$FILE_SET"/accelerometerLog.txt"
    LINEAR_ACCELERATION_FILE=$SOURCES_PATH$FILE_SET"/linearAccelerationLog.txt"
    MAGNETOMETER_FILE=$SOURCES_PATH$FILE_SET"/magneticFieldLog.txt"
    WIFI_MEASUREMENTS_FILE=$SOURCES_PATH$FILE_SET"/histogramLog_wifi.json"

    # Wifi is the default, but if it doesn't exist we try btle:
    if [ ! -f $WIFI_MEASUREMENTS_FILE ]
    then
        WIFI_MEASUREMENTS_FILE=$SOURCES_PATH$FILE_SET"/histogramLog_btle.json"
    fi

    if [ ! -f $FINGERPRINT_FILE ]
    then
        FINGERPRINT_FILE=$SOURCES_PATH$FILE_SET"/fingerprints.json"
    fi


    BASIC_PARAMETERS="occupancy_grid_map_file="$OCCUPANCY_GRID_MAP_FILE" north_angle="$NORTH_ANGLE" scale="$SCALE" fingerprint_file="$FINGERPRINT_FILE" wifi_measurements_file="$WIFI_MEASUREMENTS_FILE" accelerometer_file="$ACCELEROMETER_FILE" linear_acceleration_file="$LINEAR_ACCELERATION_FILE" magnetometer_file="$MAGNETOMETER_FILE

    for i in "${CONFIGURATIONS[@]}"
    do
        CONFIGURATION=$i
        POSITIONS_FILE=$OUTPUT_PATH$FILE_SET"/positionsLongLat_Standalone.txt"
        IMAGE_FILE=$OUTPUT_PATH$FILE_SET"/result_Standalone"
        MEAN_FILE=$OUTPUT_PATH$FILE_SET"/mean_Standalone.txt"

        POSITIONS_FILE=$(echo $POSITIONS_FILE | tr ' ' '_')
        IMAGE_FILE=$(echo $IMAGE_FILE | tr ' ' '_')
        MEAN_FILE=$(echo $MEAN_FILE | tr ' ' '_')

        #echo $STANDALONE $BASIC_PARAMETERS positions_file=$POSITIONS_FILE $CONFIGURATION" &"
        echo ""
        echo $PYTHON_ACCURACY -g $GROUND_TRUTH_FILE -p $POSITIONS_FILE -i $IMAGE_FILE -s $SCALE -m $MEAN_FILE" &"
        echo ""
        echo ""
        echo ""
    done 
done
