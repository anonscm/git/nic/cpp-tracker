#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <chrono>

#include "UserLocator.h"
#include "DeadReckoning.h"
#include "FileParser.h"
#include "OccupancyGridMap.h"
#include "GeoPoint.h"
#include "TrackerConfig.h"
#include "json_spirit.h"
#include "Version.h"


using namespace std;
using namespace chrono;
using namespace json_spirit;

/*
 * The main function for using this library as a standalone tracker using recorded files as an input.
 * 
 * All the sensor data and wifi measurements are synchronized properly. There are timestamps in 
 * the measurements files and they are evaluated. Then only a delay/warp-factor needs
 * to be configured, globally for all threads, to define how fast the calculation should 
 * run. The problem is, that different devices might have had different speeds for their
 * measuring.
 *
 * TODO (prio 2): replace inefficient vector::erase with a datastructure like double ended queue,
 *                which is more appropriate for this operation. (But has no real practical effect here,
 *                because we are waiting intentionally anway)
 * 
 *
 * SEGEMENTATION FAULT error? What to check:
 * 1.- getMappedTimestampSensorValues(). Has the "result" vector the same field as parsed?
 *
 * std::string json_spirit::value_type_to_string(json_spirit::Value_type): Assertion `false' failed? What to check:
 * 1.- FileParser.cpp -> FileParser::parseJson(mArray arr). Log files uses "latitude/longitude" or  "mLatitudeE6/mLongitudeE6" ?
 *      New format:
 *        double latitude = tmp.find("latitude")->second.get_real();
 *        double longitude = tmp.find("longitude")->second.get_real();
 *      Old format:
 *        int latitude = tmp.find("mLatitudeE6")->second.get_int();
 *        int longitude = tmp.find("mLongitudeE6")->second.get_int();
 */

UserLocator* userLocator = nullptr;
vector<SensorMeasurement> valuesAccelerometer;
vector<SensorMeasurement> valuesLinearAcceleration;
vector<SensorMeasurement> valuesMagnetometer;

vector<Fingerprint> measurements;

bool verbose = false;


class TimestampFingerprint : public Fingerprint {
public:
    TimestampFingerprint() : Fingerprint() {}

    TimestampFingerprint(Histogram histogram, GeoPoint geoPoint) : Fingerprint(histogram, geoPoint) {}

    uint64_t timestamp;
};



void showParameters() {
    TrackerConfig* config = TrackerConfig::getInstance();
    vector<TrackerConfigParameter> params = config->getConfig();

    cout << "supported config parameters:" << endl;

    for (auto it = params.begin(); it != params.end(); ++it) {
        TrackerConfigParameter p = *it;
        cout << p.name << " ";
        if (p.type == PT_INT) {
            cout << "(int)";
        } else if (p.type == PT_DOUBLE) {
            cout << "(double)";
        } else if (p.type == PT_BOOL) {
            cout << "(bool)";
        } else if (p.type == PT_STRING) {
            cout << "(string)";
        }

        if (p.type == PT_INT || p.type == PT_DOUBLE) {
            cout << " min=" << p.minValue << " max=" << p.maxValue;
        }

        cout << " default=" << p.defaultValue << endl;
    }
}


/**
 * for all command line arguments that look like "key=value", extract key and value
 * and stick them into our config singleton
 */
void overrideConfigParams(int argc, const char* argv[]) {
    TrackerConfig* config = TrackerConfig::getInstance();

    for (int i=1; i<argc; ++i) {
        string arg = argv[i];

        string::size_type eq = arg.find("=", 0);
        if (eq != string::npos) {
            string key = arg.substr(0, eq);
            string value = arg.substr(eq+1, string::npos);

            config->setValue(key, value);
        }
        else if (arg == "--help") {
            showParameters();
            exit(0);
        }
        else if (arg == "--verbose" || arg == "-v") {
            verbose = true;
        }
    }
}


// The recorded histogram log is stored in a json structure that is quite like
// a fingerprint, only with the addition of a timestamp.
vector<TimestampFingerprint> parseHistogramJson(mArray arr) {
    // parses our current model for fingerprints
    // [{"histogram":{"$MAC_ADDRESS || $IBEACON_ID":{"$SIGNAL_STRENGTH": $VALUE, ...}, ...},  
    // "id": "$ID", "point": {"divergence": $VALUE, "mAltitude": $VALUE, "mLatitudeE6": $VALUE, "mLongitudeE6": $VALUE}}, ...]
    vector<TimestampFingerprint> fingerprints;
    for (unsigned int i = 0; i < arr.size(); i++) {
        mObject obj = arr[i].get_obj();

        Histogram tmpHist;

        mObject tmp = obj.find("point")->second.get_obj();
        int latitude = tmp.find("mLatitudeE6")->second.get_int();
        int longitude = tmp.find("mLongitudeE6")->second.get_int();
        GeoPoint tmpPoint(latitude, longitude);

        tmpHist.setId(obj.find("id")->second.get_str());

        map<string, map<int, float> > tmpHistEntry;
        map<int, float> tmpMeasurement;
        
        mObject::iterator it = obj.find("histogram")->second.get_obj().begin();
        for (; it != obj.find("histogram")->second.get_obj().end(); ++it) {

            mObject::iterator it2 = it->second.get_obj().begin();
            for (; it2 != it->second.get_obj().end(); ++it2) {
                tmpMeasurement.insert(pair<int, float>(atoi(it2->first.c_str()), it2->second.get_real()));
            }
            tmpHistEntry.insert(pair<string, map<int, float> >(it->first, tmpMeasurement));

            tmpMeasurement.clear();
        }

        tmpHist.setHistogram(tmpHistEntry);
        tmpHistEntry.clear();

        TimestampFingerprint tmpFp(tmpHist, tmpPoint);
        tmpFp.timestamp = obj.find("timestamp")->second.get_int64();
        fingerprints.push_back(tmpFp);
    }

    return fingerprints;
}


map<uint64_t, TimestampFingerprint> getMappedTimestampWifiMeasurements(const std::string& filename) {
    if (verbose) {
        cerr << "loading Timestamp fingerprints: " << filename << std::endl;
    }

    vector<TimestampFingerprint> fingerprints;
    map<uint64_t, TimestampFingerprint> mappedTimestampWifiMeasurements;

    mValue value;
    ifstream file(filename.c_str(), ifstream::in);

    if (read(file, value)) {
        mArray arr = value.get_array();
        fingerprints = parseHistogramJson(arr);
    } else {
        cerr << "Sensor data file couldn't be read! " << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    file.close();

    typedef vector<TimestampFingerprint>::const_iterator constIteratorType;
    for (constIteratorType iterator = fingerprints.begin(); iterator != fingerprints.end(); ++iterator) {
        mappedTimestampWifiMeasurements.insert ( std::pair<uint64_t, TimestampFingerprint>(iterator->timestamp, *iterator) );
    }

    return mappedTimestampWifiMeasurements;
}


// Parses a file with sensor data in it, captured by the cell phone
// All three sensor files (accelerometerLog, linearAccelerationLog, magneticFieldLog) have
// the same format: $TIMESTAMP $dontCare $XAXIS $YAXIS $ZAXIS
map<uint64_t, SensorMeasurement> getMappedTimestampSensorValues(const std::string& filename) {
    if (verbose) {
        cerr << "Loading sensormeasurements with timestamp: " << filename << std::endl;
    }
    map<uint64_t, SensorMeasurement> mappedMeasurements;

    ifstream file(filename.c_str(), ifstream::in);
    if (!file.is_open()) {
        cerr << "Sensor data file couldn't be read! " << filename << std::endl;
        exit (EXIT_FAILURE);
    }
  
    string line;
    SensorMeasurement tmpMeasurement;
    string tmp;
    vector<string> result;
    
    while (getline(file, line)) {
        stringstream lineStream(line);
        while (getline(lineStream, tmp, ' ')) {
            result.push_back(tmp);
        }

        if (result.size() == 5) {
            tmpMeasurement.timestamp = atoll(result[0].c_str());
            tmpMeasurement.x = atof(result[2].c_str());
            tmpMeasurement.y = atof(result[3].c_str());
            tmpMeasurement.z = atof(result[4].c_str());
        } else {
            // If there are not exactly 5 fields then the format of the sensor file
            // is different and we assume that the 2nd column is missing.
            // It would be nice if the method comment above would explain why there
            // are two different formats...
            tmpMeasurement.timestamp = atoll(result[0].c_str());
            tmpMeasurement.x = atof(result[1].c_str());
            tmpMeasurement.y = atof(result[2].c_str());
            tmpMeasurement.z = atof(result[3].c_str());
        }
        
        mappedMeasurements.insert ( std::pair<uint64_t, SensorMeasurement>(tmpMeasurement.timestamp, tmpMeasurement) );

        result.clear();
    }

    file.close();

    return mappedMeasurements;
}


// We want to treat our maps of sensor- and histogram data more like lists (we only ever care about the first element)
// to make this easier, we implement front() and pop_front() as functions
template<typename K, typename V> std::pair<K,V> front(map<K,V>& container) {
    auto iterator = container.begin();
    return *iterator;
}

template<typename K, typename V> std::pair<K,V> pop_front(map<K,V>& container) {
    auto iterator = container.begin();
    pair<K,V> value = *iterator;
    container.erase(iterator);
    return value;
}


uint64_t getCurrentTimeMillis() {
    return duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
}


// This function takes the sensor logs and feeds the measurements to the tracker
// (which runs in another thread) at aporoximately the right time, to replay what
// happened on the device.
void sendWifiAndSensorData() {
    TrackerConfig* config = TrackerConfig::getInstance();

    // We have 1 value for each type of sensor which will be updated individually
    // in each iteration. As in the real application the same measurements can
    // be fed to the tracker multiple times. The faster ones will use duplicate
    // values for the slower ones because the tracker expects data from all three
    // sensors at the same time.
    SensorMeasurement currentValueAccelerometer;
    SensorMeasurement currentValueLinearAcceleration;
    SensorMeasurement currentValueMagnetometer;

    map<uint64_t, SensorMeasurement> mappedValuesAccelerometer = getMappedTimestampSensorValues(config->getString(ACCELEROMETER_FILE));
    map<uint64_t, SensorMeasurement> mappedValuesLinearAcceleration = getMappedTimestampSensorValues(config->getString(LINEAR_ACCELERATION_FILE));
    map<uint64_t, SensorMeasurement> mappedValuesMagnetometer = getMappedTimestampSensorValues(config->getString(MAGNETOMETER_FILE));
    map<uint64_t, TimestampFingerprint> mapppedValuesWifiMeasurements = getMappedTimestampWifiMeasurements(config->getString(WIFI_MEASUREMENTS_FILE));

    vector<Fingerprint> fingerprints = FileParser::getFingerprints(config->getString(FINGERPRINT_FILE));
    userLocator->setFingerprints(fingerprints);

    uint64_t smallestTimestamp = min({mappedValuesAccelerometer.begin()->first,
                                mappedValuesLinearAcceleration.begin()->first,
                                mappedValuesMagnetometer.begin()->first,
                                mapppedValuesWifiMeasurements.begin()->first});

    uint64_t biggestTimestamp = max({mappedValuesAccelerometer.rbegin()->first,
                                mappedValuesLinearAcceleration.rbegin()->first,
                                mappedValuesMagnetometer.rbegin()->first,
                                mapppedValuesWifiMeasurements.rbegin()->first});

    uint64_t currentTimeMilliseconds = getCurrentTimeMillis();

    // Compute the timestamp difference between the log files and current time to compensate in the loop.
    // I.e. we care about the relative timings, but not when the data was recorded.
    uint64_t timeDiff = currentTimeMilliseconds - smallestTimestamp;

    if (verbose) {
        cerr << "test interval: " << smallestTimestamp << " - " << biggestTimestamp
             << " (simulation will run for " << ((biggestTimestamp-smallestTimestamp)/1000) << " seconds)" << endl;
    }

    pair<uint64_t, SensorMeasurement> accelerometerData = front(mappedValuesAccelerometer);
    pair<uint64_t, SensorMeasurement> linearAccelData = front(mappedValuesLinearAcceleration);
    pair<uint64_t, SensorMeasurement> magnetometerData = front(mappedValuesMagnetometer);
    pair<uint64_t, TimestampFingerprint> histogramData = front(mapppedValuesWifiMeasurements);

    //File to save the positions. Open the file and erase all information.
    string posFileName = config->getString(POSITIONS_FILE);
    ostream *outStream;
    ofstream outFile;

    if (posFileName == "-") {
        outStream = &cout;
    } else {
        outFile.open(posFileName);
        if (!outFile.is_open()) {
            cerr << "cannot open " << posFileName << " for writing" << endl;
            exit(EXIT_FAILURE);
        }
        outStream = &outFile;
    }

    int outputFreq = config->getInt(POSITION_OUTPUT_FREQ);

    string insertedSensorType = " ";
    long insertedSensorTimestamp = 0;
    int numPositions = 0;

    for (;;) {

        bool showCountdown = false;

        boost::this_thread::sleep(boost::posix_time::milliseconds(1));

        currentTimeMilliseconds = getCurrentTimeMillis();
        currentTimeMilliseconds = currentTimeMilliseconds - timeDiff;

        if (currentTimeMilliseconds >= accelerometerData.first && mappedValuesAccelerometer.size() > 0) {
            //cout << "sending accelerometer..." << endl;

            insertedSensorType = "accelerometer";
            insertedSensorTimestamp = accelerometerData.first;

            accelerometerData = pop_front(mappedValuesAccelerometer);
            showCountdown = true;
            userLocator->getDeadReckoning()->addSensorData(accelerometerData.second, linearAccelData.second, magnetometerData.second);
        }

        if (currentTimeMilliseconds >= linearAccelData.first && mappedValuesLinearAcceleration.size() > 0) {
            //cout << "sending linear acceleration..." << endl;

            insertedSensorType = "linearAccel";
            insertedSensorTimestamp = linearAccelData.first;

            linearAccelData = pop_front(mappedValuesLinearAcceleration);
            showCountdown = true;
            userLocator->getDeadReckoning()->addSensorData(accelerometerData.second, linearAccelData.second, magnetometerData.second);
        }


        if (currentTimeMilliseconds >= magnetometerData.first && mappedValuesMagnetometer.size() > 0) {
            //cout << "sending magnetometer..." << endl;

            insertedSensorType = "magnetometer";
            insertedSensorTimestamp = magnetometerData.first;

            magnetometerData = pop_front(mappedValuesMagnetometer);
            showCountdown = true;
            userLocator->getDeadReckoning()->addSensorData(accelerometerData.second, linearAccelData.second, magnetometerData.second);
        }

        if (currentTimeMilliseconds >= histogramData.first && mapppedValuesWifiMeasurements.size() > 0) {
            //cout << "sending wifi..." << endl;

            insertedSensorType = "histogram";
            insertedSensorTimestamp = histogramData.first;

            if (verbose) {
                cerr << "HG: " << histogramData.second.getHistogram().getId() << endl;
            }

            histogramData = pop_front(mapppedValuesWifiMeasurements);
            showCountdown = true;
            userLocator->addHistogram(histogramData.second.getHistogram());
        }

        if ((currentTimeMilliseconds % outputFreq) == 0) {
            //cout << "trying to get position..." << endl;
            GeoPoint position = userLocator->getCurrentPosition();
            if (verbose) {
                cerr << "Live user position #: " << position.getLongitudeE6() / 1E6 << " " << position.getLatitudeE6() / 1E6 << std::endl;
            }
            
            if (position.getLongitudeE6() / 1E6 > 1 && position.getLatitudeE6() / 1E6 > 1) {
                cerr << "Live user position #: " << position.getLongitudeE6() / 1E6 << " " << position.getLatitudeE6() / 1E6 << std::endl;

                //Get sensor information
                GeoPoint normDelta = userLocator->getDeadReckoning()->getDelta();
                //normDelta.setXY(normDelta.getX()*userLocator->getScale(), normDelta.getY()*userLocator->getScale());normDelta.setXY(normDelta.getX()*userLocator->getScale(), normDelta.getY()*userLocator->getScale());

                int lastUpdatedSensor = userLocator->getDeadReckoning()->getLastUpdatedSensor();
                SensorMeasurement acc = userLocator->getDeadReckoning()->getRawAcceleration();
                SensorMeasurement linacc = userLocator->getDeadReckoning()->getRawLinearAcceleration();
                SensorMeasurement magn = userLocator->getDeadReckoning()->getRawMagnetometer();


                string usedSensor = "histogram";

                if (!userLocator->getIsPositionComputedWithHist()) {
                    switch (lastUpdatedSensor) {
                        case SENSOR_ACCELEROMETER:
                            usedSensor = "accelerometer";
                            break;
                        case SENSOR_LINEAR_ACCELERATION:
                            usedSensor = "linearAccel";
                            break;
                        case SENSOR_MAGNETOMETER:
                            usedSensor = "magnetometer";
                            break;
                        default:
                            usedSensor = "UNKNOWN";
                    }
                }

                numPositions++;


                // Write the position to our output file
                (*outStream) << numPositions << " "
                             << currentTimeMilliseconds << " "
                             << position.getLongitudeE6() / 1E6 << " "
                             << position.getLatitudeE6() / 1E6 << " "
                             << insertedSensorType << " "
                             << insertedSensorTimestamp << " "

                             << userLocator->getIsPositionComputedWithHist() << " "                             

                             << userLocator->getNextWifiPosition().getLongitude() << " "
                             << userLocator->getNextWifiPosition().getLatitude() << " "

                             << usedSensor << " "

                             << userLocator->getDeadReckoning()->detectMovement() << " "                             
                             << userLocator->getDeadReckoning()->getDistance() << " "

                             << normDelta.getLongitude() << " "
                             << normDelta.getLatitude() << " "

                             << acc.x << " "
                             << acc.y << " "
                             << acc.z << " "

                             << linacc.x << " "
                             << linacc.y << " "
                             << linacc.z << " "

                             << magn.x << " "
                             << magn.y << " "
                             << magn.z << " "

                             << endl;
            }
        }

        if (verbose && showCountdown) {
            cerr << 
                currentTimeMilliseconds << ": " <<
                mappedValuesAccelerometer.size() << " " <<
                mappedValuesLinearAcceleration.size() << " " <<
                mappedValuesMagnetometer.size() << " " <<
                mapppedValuesWifiMeasurements.size() << endl;
        }

        if (mappedValuesAccelerometer.size() == 0 && 
            mappedValuesLinearAcceleration.size() == 0 &&
            mappedValuesMagnetometer.size() == 0 &&
            mapppedValuesWifiMeasurements.size() == 0) {
                break;
        }
    }

    outStream->flush();

    //cout << "standalone tracker has ended. Computing the accuracy with python script..." << endl;
    //The call to python script. For the moment we do not want to do it from standalone tracker
    //ostringstream os;
    //os << "python ..../plots/evaluatePositions.py -gt " <<  
    //      config->getString(GROUND_TRUTH_FILE) << " -pos " << 
    //      config->getString(POSITIONS_FILE) << " -img " << config->getString(ACCURACY_IMAGE_RESULT_FILE);
    //string s = os.str();
    //system(s.c_str());


    if (verbose) {
        cerr << "Finished! You can find the results in " << posFileName << endl;
    }
}


int main(int argc, const char* argv[]) {

    TrackerConfig* config = TrackerConfig::getInstance();

    if (argc < 2) {
       showParameters();
       exit(0);
    }

    overrideConfigParams(argc, argv);

    // make sure all required input file names are set
    if (config->getString(OCCUPANCY_GRID_MAP_FILE).size() == 0 ||
        config->getString(FINGERPRINT_FILE).size() == 0 ||
        config->getString(ACCELEROMETER_FILE).size() == 0 ||
        config->getString(LINEAR_ACCELERATION_FILE).size() == 0 ||
        config->getString(MAGNETOMETER_FILE).size() == 0 ||
        config->getString(WIFI_MEASUREMENTS_FILE).size() == 0) {
        cerr << "required input file name missing" << endl;
        exit(1);
    }


    // make sure the bounding box is set
    if (config->getDouble(BOUNDING_BOX_MIN_LAT) == 0 || 
        config->getDouble(BOUNDING_BOX_MAX_LAT) == 0 ||
        config->getDouble(BOUNDING_BOX_MIN_LON) == 0 ||
        config->getDouble(BOUNDING_BOX_MAX_LON) == 0) {
        cerr << "required bounding box coordinates missing" << endl;
        exit(1);
    }

    // get the occupancy grid map
    unsigned int width, height;
    vector<float> pixelValues = FileParser::getOccupancyGridmap(config->getString(OCCUPANCY_GRID_MAP_FILE), &width, &height, verbose);
    OccupancyGridMap* gridMap = new OccupancyGridMap(pixelValues, width, height);

    // ... and the bounding box
    GeoPoint min(config->getDouble(BOUNDING_BOX_MIN_LAT), config->getDouble(BOUNDING_BOX_MIN_LON));
    GeoPoint max(config->getDouble(BOUNDING_BOX_MAX_LAT), config->getDouble(BOUNDING_BOX_MAX_LON));

    // ... to initialize the userlocator.
    // NB: these calls MUST happen in this specific order. 
    userLocator = new UserLocator(config->getInt(LOCALISATION_MODE));
    userLocator->setBoundingBox(min, max);
    userLocator->setGridMap(gridMap);
    userLocator->setScale(config->getDouble(SCALE));
    userLocator->getDeadReckoning()->setBaseAngle(config->getDouble(NORTH_ANGLE));

    cout << "SCALE: " << config->getDouble(SCALE) << endl;
    cout << "NORTH_ANGLE: " << config->getDouble(NORTH_ANGLE) << endl;

    userLocator->setWritingPositionsToFile(true);


    cout << "LOCALISATION_UPDATE_RATE: " << config->getInt(LOCALISATION_UPDATE_RATE) << endl;
    cout << "POSITION_OUTPUT_FREQ: " << config->getInt(POSITION_OUTPUT_FREQ) << endl;

    string version = str(boost::format("%1%.%2%.%3%.%4%.%5%")
                            % INVIO_TRACKER_VERSION_MAJOR       // NOSONAR - this may be 0
                            % INVIO_TRACKER_VERSION_INTERFACE   // NOSONAR - this may be 0
                            % INVIO_TRACKER_VERSION_MINOR       // NOSONAR - this may be 0
                            % INVIO_TRACKER_BUILD_NUMBER
                            % INVIO_TRACKER_DEPLOY_NUMBER);

    cout << "VERSION: " << version << endl;
    userLocator->startTracking();

    sendWifiAndSensorData();
    
    return 0;
}
