mkdir build-jni\Windows-x86
cd build-jni\Windows-x86
cmake -DBUILD_STANDALONE=true -DBUILD_TEST=false -DBUILD_JNI=true -G "MSYS Makefiles" ..\..\
mingw32-make install
cd ..
cd ..

mkdir jni\windows\resources\lib\Windows-x86
copy build-jni\Windows-x86\dist\jni\*.dll jni\windows\resources\lib\Windows-x86\*.dll

cd jni\windows
gradlew install
cd ..
cd ..