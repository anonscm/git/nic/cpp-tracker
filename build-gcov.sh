#!/bin/bash
mkdir build
cd build
set -e
cmake -DENABLE_GCOV=true -DBUILD_STANDALONE=true -DBUILD_TEST=true -G "Unix Makefiles" ../
make install
cd -
