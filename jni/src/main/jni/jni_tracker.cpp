#include <jni.h>
#include <string>
#include <map>
#include <iostream>
#include "Tracker.h"

using namespace std;

#ifdef __ANDROID__
#include <android/log.h>

class androidbuf: public std::streambuf {
public:
    enum { bufsize = 128 }; // ... or some other suitable buffer size
    androidbuf() { this->setp(buffer, buffer + bufsize - 1); }
private:
    int overflow(int c) {
        if (c == traits_type::eof()) {
            *this->pptr() = traits_type::to_char_type(c);
            this->sbumpc();
        }
        return this->sync()? traits_type::eof(): traits_type::not_eof(c);
    }
    int sync() {
        int rc = 0;
        if (this->pbase() != this->pptr()) {
            __android_log_print(ANDROID_LOG_INFO,
                               "Tracker-jni",
                               "%s",
                               std::string(this->pbase(),
                                           this->pptr() - this->pbase()).c_str());
            rc = 0;
            this->setp(buffer, buffer + bufsize - 1);
        }
        return rc;
    }
    char buffer[bufsize];
};

#endif

#ifdef __cplusplus
extern "C" {
#endif

static jclass classTracker;
static jmethodID classTrackerAppendPathPoint;
static jmethodID classTrackerAppendParticle;
    
static jclass invioGeoPoint;
static jmethodID invioGeoPoint_constructor;
static jmethodID invioGeoPoint_setLatitude;
static jmethodID invioGeoPoint_setLongitude;
static jmethodID invioGeoPoint_getLatitude;
static jmethodID invioGeoPoint_getLongitude;

static jclass classTrackerConfigParameter;
static jmethodID classTrackerConfigParameter_constructor;
static jfieldID classTrackerConfigParameter_name;
static jfieldID classTrackerConfigParameter_description;
static jfieldID classTrackerConfigParameter_type;
static jfieldID classTrackerConfigParameter_minValue;
static jfieldID classTrackerConfigParameter_maxValue;
static jfieldID classTrackerConfigParameter_defaultValue;
static jfieldID classTrackerConfigParameter_currentValue;


JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* aVm, void* aReserved){
    //redirect cout to android logcat as cout points to /dev/null on android
	
#ifdef __ANDROID__
    std::cout.rdbuf(new androidbuf);
    std::cerr.rdbuf(new androidbuf);
#endif

    std::cout << "JNI_OnLoad" << std::endl;

    JNIEnv* env;
    if (aVm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
    {
		std::cout << "Failed to get the JNIEnv" << std::endl;
		return -1;
    }

    // FindClass returns Local References, so we need to add a new Global Reference to the JavaVM
    // so that the class object is not getting garbage collected.

    classTracker = (jclass)env->NewGlobalRef(env->FindClass("de/tarent/invio/tracker/Tracker"));

    classTrackerAppendPathPoint = env->GetMethodID(classTracker, "appendPathPoint", "(DD)V");
    classTrackerAppendParticle = env->GetMethodID(classTracker, "appendParticle", "(DDF)V");

    invioGeoPoint = (jclass)env->NewGlobalRef(env->FindClass("de/tarent/invio/entities/InvioGeoPoint"));
    
    // GetMethodID returns already Global References
    invioGeoPoint_constructor = env->GetMethodID(invioGeoPoint, "<init>", "()V");    
    invioGeoPoint_getLatitude = env->GetMethodID(invioGeoPoint, "getLatitude", "()D");
    invioGeoPoint_getLongitude = env->GetMethodID(invioGeoPoint, "getLongitude", "()D");
    invioGeoPoint_setLatitude = env->GetMethodID(invioGeoPoint, "setLatitude", "(D)V");
    invioGeoPoint_setLongitude = env->GetMethodID(invioGeoPoint, "setLongitude", "(D)V");

    classTrackerConfigParameter = (jclass)env->NewGlobalRef(env->FindClass("de/tarent/invio/tracker/TrackerConfigParameter"));
    classTrackerConfigParameter_constructor = env->GetMethodID(classTrackerConfigParameter , "<init>", "()V");
    classTrackerConfigParameter_name = env->GetFieldID(classTrackerConfigParameter, "name", "Ljava/lang/String;");
    classTrackerConfigParameter_description = env->GetFieldID(classTrackerConfigParameter, "description", "Ljava/lang/String;");
    classTrackerConfigParameter_type = env->GetFieldID(classTrackerConfigParameter, "type", "I");
    classTrackerConfigParameter_minValue = env->GetFieldID(classTrackerConfigParameter, "minValue", "F");
    classTrackerConfigParameter_maxValue = env->GetFieldID(classTrackerConfigParameter, "maxValue", "F");
    classTrackerConfigParameter_defaultValue = env->GetFieldID(classTrackerConfigParameter, "defaultValue", "Ljava/lang/String;");
    classTrackerConfigParameter_currentValue = env->GetFieldID(classTrackerConfigParameter, "currentValue", "Ljava/lang/String;");
    
    return JNI_VERSION_1_6;
}

JNIEXPORT jlong JNICALL Java_de_tarent_invio_tracker_Tracker_ncreate( JNIEnv* env, jobject thiz )
{
    Tracker* tracker = new Tracker();
    return (jlong)tracker;
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nrelease( JNIEnv* env, jobject thiz, jlong ptr)
{
    Tracker* tracker = (Tracker*)ptr;
    delete tracker;
}

JNIEXPORT jint JNICALL Java_de_tarent_invio_tracker_Tracker_nstart( JNIEnv* env, jobject thiz, jlong ptr)
{
    Tracker* tracker = (Tracker*)ptr;
    return (jint)tracker->start();
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nstop( JNIEnv* env, jobject thiz, jlong ptr)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->stop();
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_naddSensorData(JNIEnv* env, jobject thiz, jlong ptr, float accX, float accY, float accZ,
                        float linAccX, float linAccY, float linAccZ,
                        float magX, float magY, float magZ) {
    Tracker* tracker = (Tracker*)ptr;
    tracker->addSensorData(accX, accY, accZ, linAccX, linAccY, linAccZ, magX, magY, magZ);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nputTempHistogram( JNIEnv* env, jobject thiz, jlong ptr, jstring key, jint rssi, jfloat divergence)
{
    Histogram* hist = (Histogram*)ptr;
    
    const char *nativeString = env->GetStringUTFChars(key, JNI_FALSE);
    std::string keyString(nativeString);
    env->ReleaseStringUTFChars(key, nativeString);

    hist->m_histogram[keyString][(int)rssi] = (float)divergence;
}

JNIEXPORT jlong JNICALL Java_de_tarent_invio_tracker_Tracker_ncreateTempHistogram( JNIEnv* env, jobject thiz, jstring id)
{
    const char* nativeString = env->GetStringUTFChars(id, JNI_FALSE);
    std::string stringId(nativeString);
    env->ReleaseStringUTFChars(id, nativeString);

    Histogram* hist = new Histogram(stringId);
    return (jlong)hist;
}


JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nreleaseTempHistogram( JNIEnv* env, jobject thiz, jlong ptr)
{
    Histogram* hist = (Histogram*)ptr;
    delete hist;
}

JNIEXPORT jlong JNICALL Java_de_tarent_invio_tracker_Tracker_ncreateTempFingerprintsArray( JNIEnv* env, jobject thiz)
{
    vector<Fingerprint>* fingerprints = new vector<Fingerprint>(); 
    return (jlong)fingerprints;
}


JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nreleaseTempFingerprintsArray( JNIEnv* env, jobject thiz, jlong ptr)
{
    vector<Fingerprint>* fingerprints = (vector<Fingerprint>*)ptr;
    delete fingerprints;
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nputTempFingerprintsArray( JNIEnv* env, jobject thiz, jlong ptr, 
        jlong histogramPtr, jdouble latitude, jdouble longitude)
{
    vector<Fingerprint>* fingerprints = (vector<Fingerprint>*)ptr;
    Histogram* hist = (Histogram*)histogramPtr;
    Histogram histCopy = *hist;
    GeoPoint gp(latitude, longitude);
    Fingerprint fp(histCopy, gp);
    fingerprints->push_back(fp);
}


JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_naddHistogram( JNIEnv* env, jobject thiz, jlong ptr, jlong histogramPtr)
{
    Tracker* tracker = (Tracker*)ptr;
    Histogram* hist = (Histogram*)histogramPtr;
    Histogram histCopy = *hist;
    tracker->addHistogram(histCopy);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetFingerprints( JNIEnv* env, jobject thiz, jlong ptr, jlong fingerprintsPtr)
{
    Tracker* tracker = (Tracker*)ptr;
  
    vector<Fingerprint>* fingerprints = (vector<Fingerprint>*)fingerprintsPtr;
    vector<Fingerprint> copy(*fingerprints);

    tracker->setFingerprints(copy);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetConfigParameter( JNIEnv* env, jobject thiz, jlong ptr, jstring param, jstring value)
{
    Tracker* tracker = (Tracker*)ptr;
   
    const char* nativeString = env->GetStringUTFChars(param, JNI_FALSE);
    std::string paramString(nativeString);
    env->ReleaseStringUTFChars(param, nativeString);
    
    const char* nativeString2 = env->GetStringUTFChars(value, JNI_FALSE);
    std::string valueString(nativeString2);
    env->ReleaseStringUTFChars(value, nativeString2);
    
    tracker->setConfigParameter(paramString, valueString);
}
    
JNIEXPORT jstring JNICALL Java_de_tarent_invio_tracker_Tracker_ngetConfigParameter( JNIEnv* env, jobject thiz, jlong ptr, jstring param)
{
    Tracker* tracker = (Tracker*)ptr;
    
    const char* nativeString = env->GetStringUTFChars(param, JNI_FALSE);
    std::string paramString(nativeString);
    env->ReleaseStringUTFChars(param, nativeString);
    
    std::string value = tracker->getConfigParameter(paramString);
    return env->NewStringUTF(value.c_str());
    
}
    
JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetScale( JNIEnv* env, jobject thiz, jlong ptr, float scale)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->setScale(scale);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetNorthAngle( JNIEnv* env, jobject thiz, jlong ptr, float northAngle)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->setNorthAngle(northAngle);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetBoundingBox( JNIEnv* env, jobject thiz, jlong ptr, double minLat, double minLong, double maxLat, double maxLong)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->setBoundingBox(minLat, minLong, maxLat, maxLong);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetOccupancyGridMap( JNIEnv* env, jobject thiz, jlong ptr, jfloatArray pixelValues, jint width, jint height)
{
    Tracker* tracker = (Tracker*)ptr;

    jfloat *pixelValuesArray = env->GetFloatArrayElements(pixelValues, 0);
    jsize len = env->GetArrayLength(pixelValues);
    std::vector<float> gridMap(pixelValuesArray, pixelValuesArray + len);
    env->ReleaseFloatArrayElements(pixelValues, pixelValuesArray, 0);
    tracker->setOccupancyGridMap(gridMap, width, height);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetLocalisationMode( JNIEnv* env, jobject thiz, jlong ptr, int mode)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->setLocalisationMode(mode);
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nsetDebugLogEnabled( JNIEnv* env, jobject thiz, jlong ptr, jboolean enabled)
{
    Tracker* tracker = (Tracker*)ptr;
    tracker->setDebugLogEnabled(enabled);
}

JNIEXPORT jstring JNICALL Java_de_tarent_invio_tracker_Tracker_nversion(JNIEnv* env)
{
    std::string version = Tracker::version();
    return env->NewStringUTF(version.c_str());
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_nparticles( JNIEnv* env, jobject thiz, jlong ptr)
{
    Tracker* tracker = (Tracker*)ptr;
    vector<Particle> particles = tracker->particles();

    // because of the JNI's restriction to 512 object references, we rely on the java code to
    // have created an ArrayList for us, and we defer all object creation to java
    for (int i=0; i<particles.size(); i++) {
        Particle p = particles[i];
        GeoPoint gp = GeoPoint::makePoint(p.x, p.y);
        env->CallVoidMethod(thiz, classTrackerAppendParticle, gp.getLatitude(), gp.getLongitude(), p.importanceWeight);
    }
}

JNIEXPORT jobject JNICALL Java_de_tarent_invio_tracker_Tracker_ngetCurrentPosition( JNIEnv* env, jobject thiz, jlong ptr)
{
    Tracker* tracker = (Tracker*)ptr;
    GeoPoint geoPoint = tracker->getCurrentPosition();
    jobject jgeoPoint = env->NewObject(invioGeoPoint, invioGeoPoint_constructor);
    env->CallVoidMethod(jgeoPoint, invioGeoPoint_setLatitude, geoPoint.getLatitude());
    env->CallVoidMethod(jgeoPoint, invioGeoPoint_setLongitude, geoPoint.getLongitude());
    return jgeoPoint;
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_ngetPlannedPath( JNIEnv* env, jobject thiz, jlong ptr, jobject start, jobject goal)
{
    // convert the java invioGeoPoint to a c++ GeoPoint
    Tracker* tracker = (Tracker*)ptr;
    jdouble start_lat = env->CallDoubleMethod(start, invioGeoPoint_getLatitude);
    jdouble start_long = env->CallDoubleMethod(start, invioGeoPoint_getLongitude);

    jdouble goal_lat = env->CallDoubleMethod(goal, invioGeoPoint_getLatitude);
    jdouble goal_long = env->CallDoubleMethod(goal, invioGeoPoint_getLongitude);

    GeoPoint gp_start(start_lat, start_long);
    GeoPoint gp_goal(goal_lat, goal_long);

    std::vector<GeoPoint> path = tracker->getPath(gp_start, gp_goal);

    // because of the JNI's restriction to 512 object references, we rely on the java code to
    // have created an ArrayList for us, and we defer all object creation to java
    for (int i=0; i<path.size(); i++) {
        env->CallVoidMethod(thiz, classTrackerAppendPathPoint, path[i].getLatitude(), path[i].getLongitude());
    }
}

JNIEXPORT void JNICALL Java_de_tarent_invio_tracker_Tracker_ngetShortestPath( JNIEnv* env, jobject thiz, jlong ptr, jobject start, jobjectArray goals)
{
    Tracker* tracker = (Tracker*)ptr;

    //convert the java invioGeoPoint to a c++ GeoPoint
    jdouble start_lat = env->CallDoubleMethod(start, invioGeoPoint_getLatitude);
    jdouble start_long = env->CallLongMethod(start, invioGeoPoint_getLongitude);

    GeoPoint gp_start(start_lat, start_long);

    std::vector<GeoPoint> vecGoals;

    jsize len = env->GetArrayLength(goals);

	for (jint i = 0; i < len; ++i) {
       jobject goal = env->GetObjectArrayElement(goals, i);

       jdouble goal_lat = env->CallLongMethod(goal, invioGeoPoint_getLatitude);
       jdouble goal_long = env->CallLongMethod(goal, invioGeoPoint_getLongitude);

       GeoPoint gp_goal(goal_lat, goal_long);

       vecGoals.push_back(gp_goal);
    }

    std::vector<GeoPoint> path = tracker->getShortestPath(gp_start, vecGoals);

    // because of the JNI's restriction to 512 object references, we rely on the java code to
    // have created an ArrayList for us, and we defer all object creation to java
    for (int i=0; i<path.size(); i++) {
        env->CallVoidMethod(thiz, classTrackerAppendPathPoint, path[i].getLatitude(), path[i].getLongitude());
    }
}

JNIEXPORT jobjectArray JNICALL Java_de_tarent_invio_tracker_Tracker_ngetConfig(JNIEnv* env, jobject thiz, jlong ptr) {
    Tracker* tracker = (Tracker*)ptr;

    vector<TrackerConfigParameter> params = tracker->getConfig();
    jobject initParam = env->NewObject(classTrackerConfigParameter, classTrackerConfigParameter_constructor);

    jobjectArray result = env->NewObjectArray(params.size(), classTrackerConfigParameter, initParam);
    if (result == NULL) {
        return NULL; /* out of memory error thrown */
    }

    for (int i=0; i<params.size(); ++i) {
        TrackerConfigParameter param = params[i];

        jobject jparam = env->NewObject(classTrackerConfigParameter, classTrackerConfigParameter_constructor);

        env->SetObjectField(jparam, classTrackerConfigParameter_name, env->NewStringUTF(param.name.c_str()));
        env->SetObjectField(jparam, classTrackerConfigParameter_description, env->NewStringUTF(param.description.c_str()));
        env->SetIntField(jparam, classTrackerConfigParameter_type, param.type);
        env->SetFloatField(jparam, classTrackerConfigParameter_minValue, param.minValue);
        env->SetFloatField(jparam, classTrackerConfigParameter_maxValue, param.maxValue);
        env->SetObjectField(jparam, classTrackerConfigParameter_defaultValue, env->NewStringUTF(param.defaultValue.c_str()));
        env->SetObjectField(jparam, classTrackerConfigParameter_currentValue, env->NewStringUTF(param.currentValue.c_str()));

        env->SetObjectArrayElement(result, i, jparam);
    }

    return result;
}

#ifdef __cplusplus
}
#endif
