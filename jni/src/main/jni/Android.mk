LOCAL_PATH := $(call my-dir)
# prepare libtracker
include $(CLEAR_VARS)
LOCAL_MODULE    := libtrackerall
LOCAL_SRC_FILES := ../../../../build-android/dist/lib/$(TARGET_ARCH_ABI)/libtrackerall.a
include $(PREBUILT_STATIC_LIBRARY)

# prepare boost
include $(CLEAR_VARS)
LOCAL_CPPFLAGS  += -fexceptions
LOCAL_CPPFLAGS  += -frtti

LOCAL_MODULE    := libboost_atomic
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_atomic.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_date_time
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_date_time.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_iostreams
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_iostreams.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_program_options
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_program_options.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_regex
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_regex.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_signals
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_signals.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_system
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_system.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_filesystem
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_filesystem.a
include $(PREBUILT_STATIC_LIBRARY)

LOCAL_MODULE    := libboost_thread
LOCAL_SRC_FILES := ../../../../ext/boost-android/lib/$(TARGET_ARCH_ABI)/libboost_thread.a
include $(PREBUILT_STATIC_LIBRARY)

# build JNI
include $(CLEAR_VARS)

#LOCAL_CPPFLAGS += -fexceptions
#LOCAL_CPPFLAGS += -frtti

LOCAL_STATIC_LIBRARIES := trackerall boost_atomic boost_system boost_thread
LOCAL_MODULE    := libtracker-jni
LOCAL_C_INCLUDES := $(NDK_APP_PROJECT_PATH)/../../../build-android/dist/include $(NDK_APP_PROJECT_PATH)/../../../ext/boost-android/include
LOCAL_SRC_FILES := jni_tracker.cpp
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog
include $(BUILD_SHARED_LIBRARY)