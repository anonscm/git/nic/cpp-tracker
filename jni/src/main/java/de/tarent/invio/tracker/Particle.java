package de.tarent.invio.tracker;

/**
 * Particles of the Tracker, mainly for displaying in the Admin App
 */
public class Particle {

    public Particle(double latitude, double longitude, float weight) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.importanceWeight = weight;
    }

    /**
     * The latitude of the particles
     */
    public double latitude; //NOSONAR - Does not need to be private.

    /**
     * The longitude of the particles
     */
    public double longitude; //NOSONAR - Does not need to be private.

    /**
     * The importance weight of the particle.
     */
    public float importanceWeight; //NOSONAR - Does not need to be private.
}
