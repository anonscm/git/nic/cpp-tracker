package de.tarent.invio.tracker;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;

/**
 * A java implementation of the tracker linked with the jni interface.
 */
public class Tracker {

    /**
     * The localisation parameter: wifi.
     */
    public static final int LOCALISATION_WIFI = 0;

    /**
     * The localisation parameter: dead reckoning.
     */
    public static final int LOCALISATION_DEAD_RECKONING = 1;

    /**
     * The localisation parameter: particle filter.
     */
    public static final int LOCALISATION_PARTICLE_FILTER = 2;

    static {
        Native.loadLibrary("tracker-jni");
    }

    // Native C pointer address.
    private long pointer = 0;

    /**
     * The java wrapper class for our Tracker written in C++
     * <p/>
     * Wraps the interface as defined in Tracker.h using the Java JNI
     */
    public Tracker() {
        pointer = ncreate();
    }

    @Override
    protected void finalize() throws Throwable {
        nrelease(pointer);
        super.finalize();
    }

    /**
     * Starts the Tracker.
     *
     * @throws TrackerException when something went wrong while initialising the tracker
     */
    public synchronized void start() throws TrackerException {
        final int retVal = nstart(pointer);
        if (retVal != 0) {
            throw new TrackerException("TrackerException", retVal);
        }
    }

    /**
     * Stops the Tracker.
     */
    public synchronized void stop() {
        nstop(pointer);
    }

    /**
     * Add a Histogram (e.g. from the Device) to the Tracker.
     *
     * @param histogram the {@link Histogram}
     */
    public synchronized void addHistogram(final Histogram histogram) {
        calculateHistogramProbability(histogram);

        final long ptr = createNativeHistogram(histogram);

        naddHistogram(pointer, ptr);

        nreleaseTempHistogram(ptr);
    }

    private void calculateHistogramProbability(final Histogram histogram) {
        for (final Map<Integer, Float> transmitterValues : histogram.values()) {
            final Set<Integer> keys = transmitterValues.keySet();

            int transmitterSignalCount = 0;

            for (final Integer key : keys) {
                transmitterSignalCount += transmitterValues.get(key);
            }

            for (final Integer key : keys) {
                final float value = transmitterValues.get(key) / transmitterSignalCount;
                transmitterValues.put(key, value);
            }
        }
    }

    private long createNativeHistogram(final Histogram histogram) {
        String sId = histogram.getId();
        if (sId == null) {
            sId = "null";
        }
        final long ptr = ncreateTempHistogram(sId);

        for (final Map.Entry<String, Map<Integer, Float>> entry : histogram.entrySet()) {
            for (final Map.Entry<Integer, Float> innerEntry : entry.getValue().entrySet()) {
                nputTempHistogram(ptr, entry.getKey(), innerEntry.getKey(), innerEntry.getValue());
            }
        }
        return ptr;
    }

    /**
     * Add a SensorData (e.g. from the Device) to the Tracker
     *
     * @param accX    accelerometer X
     * @param accY    accelerometer Y
     * @param accZ    accelerometer Z
     * @param linAccX linearAcceleration X, may be 0 if no linearAcceleration is available
     * @param linAccY linearAcceleration Y, may be 0 if no linearAcceleration is available
     * @param linAccZ linearAcceleration Z, may be 0 if no linearAcceleration is available
     * @param magX    magnetometer X
     * @param magY    magnetometer Y
     * @param magZ    magnetometer Z
     */
    public synchronized void addSensorData(final float accX, final float accY, final float accZ,
                                           final float linAccX, final float linAccY, final float linAccZ,
                                           final float magX, final float magY, final float magZ) {
        naddSensorData(pointer, accX, accY, accZ,
                linAccX, linAccY, linAccZ,
                magX, magY, magZ);
    }


    /**
     * Set the Fingerprints as a list of Fingerprints.
     *
     * @param fingerprints the list of {@link Fingerprint}s
     */
    public synchronized void setFingerprints(final List<Fingerprint> fingerprints) {
        if (fingerprints == null) {
            return;
        }

        final long fingerprintsPtr = ncreateTempFingerprintsArray();
        for (final Fingerprint fp : fingerprints) {
            if (fp == null) {
                return;
            }

            final Histogram histogram = fp.getHistogram();
            if (histogram != null) {
                final long histPtr = createNativeHistogram(fp.getHistogram());
                final InvioGeoPoint point = fp.getPoint();
                nputTempFingerprintsArray(fingerprintsPtr, histPtr, point.getLatitude(), point.getLongitude());
                nreleaseTempHistogram(histPtr);
            }
        }
        nsetFingerprints(pointer, fingerprintsPtr);
        nreleaseTempFingerprintsArray(fingerprintsPtr);
    }

    /**
     * Sets the config parameter to the given value.
     * <p/>
     * Some config parameters only get loaded on the start of the tracker so a stop and start
     * is required.
     *
     * @param parameter the parameter
     * @param value     the value
     */
    public synchronized void setConfigParameter(final String parameter, final String value) {
        nsetConfigParameter(pointer, parameter, value);
    }

    /**
     * Gets the config value to the given parameter.
     *
     * @param parameter the parameter
     * @return the value to the given parameter as a String
     */
    public synchronized String getConfigParameter(final String parameter) {
        return ngetConfigParameter(pointer, parameter);
    }

    /**
     * Set the scale for the DeadReckoning.
     *
     * @param scale the scale
     */
    public synchronized void setScale(final float scale) {
        nsetScale(pointer, scale);
    }

    /**
     * Set the northAngle for the DeadReckoning in degrees.
     *
     * @param northAngle the north angle
     */
    public synchronized void setNorthAngle(final float northAngle) {
        nsetNorthAngle(pointer, northAngle);
    }

    /**
     * Set the occupancy grid map.
     *
     * @param pixelValues the array of pixel values
     * @param width       the width
     * @param height      the height
     */
    public synchronized void setOccupancyGridMap(final float[] pixelValues, final int width, final int height) {
        if (width != 0 && height != 0 && pixelValues != null && pixelValues.length > 0) {
            nsetOccupancyGridMap(pointer, pixelValues, width, height);
        }
    }

    /**
     * Set the bounding box.
     *
     * @param min the min point
     * @param max the max point
     */
    public synchronized void setBoundingBox(final InvioGeoPoint min, final InvioGeoPoint max) {
        nsetBoundingBox(pointer, min.getLatitude(), min.getLongitude(), max.getLatitude(), max.getLongitude());
    }

    /**
     * Set the localisationMode.
     * <p/>
     * LOCALISATION_WIFI = 0;
     * LOCALISATION_DEAD_RECKONING = 1;
     * LOCALISATION_PARTICLE_FILTER = 2;
     *
     * @param mode the mode
     */
    public synchronized void setLocalisationMode(final int mode) {
        nsetLocalisationMode(pointer, mode);
    }

    /**
     * Enabled lots of debugging output printed in logcat.
     *
     * @param debugLog if debug mode is enabled or not
     */
    public synchronized void setDebugLogEnabled(final boolean debugLog) {
        nsetDebugLogEnabled(pointer, debugLog);
    }

    /**
     * Get the particles.
     *
     * TODO: this needs to be fixed in the same way we did for planShortestPath
     * to circumvent the 512-references restriction (see SEL-666)
     * 
     * @return the array of {@link Particle}s
     */
    public synchronized Particle[] particles() {
        particles = new ArrayList<Particle>();
        nparticles(pointer);
        return particles.toArray(new Particle[particles.size()]);
    }
	
	/**
     * Gets the version.
     *
     * @return the version string
     */
    public static String version() {
        return Tracker.nversion();
    }

	/**
	* @return the current config as an array of TrackerConfigParameter
	*/
    public synchronized TrackerConfigParameter[] getConfig() {
        return ngetConfig(pointer);
    }

    /**
     * Gets the current Position as latitude/longitude.
     *
     * @return The GeoPoint containing latitude/longitude coordinates
     */
    public synchronized InvioGeoPoint getCurrentPosition() {
        return ngetCurrentPosition(pointer);
    }

    /**
     * Plan the map from the start point to the end point.
     * NB: because of the stupid restriction to 512 object references in the C++ code, we
     * need to create the waypoints array here and have the C++ part call `appendPathPoint´
     * (see SEL-666)
     *
     * @param start the start {@link InvioGeoPoint}
     * @param goal  the end {@link InvioGeoPoint}
     * @return the plotted path
     */
    public synchronized InvioGeoPoint[] planPath(final InvioGeoPoint start, final InvioGeoPoint goal) {
        waypoints = new ArrayList<InvioGeoPoint>();
        ngetPlannedPath(pointer, start, goal);
        return waypoints.toArray(new InvioGeoPoint[waypoints.size()]);
    }

    /**
     * Plan the shortest path from the start point to the end point.
     * NB: because of the stupid restriction to 512 object references in the C++ code, we
     * need to create the waypoints array here and have the C++ part call `appendPathPoint´
     * (see SEL-666)
     *
     * @param start the start {@link InvioGeoPoint}
     * @param goals the end {@link InvioGeoPoint}
     * @return the shortest plotted path
     */
    public synchronized InvioGeoPoint[] planShortestPath(final InvioGeoPoint start, final InvioGeoPoint[] goals) {
        waypoints = new ArrayList<InvioGeoPoint>();
        ngetShortestPath(pointer, start, goals);
        return waypoints.toArray(new InvioGeoPoint[waypoints.size()]);
    }

    /*
     * helper methods and lists for planPath/planShortestPath and getParticles
     */
    private void appendPathPoint(final double latitude, final double longitude) {
        InvioGeoPoint geoPoint = new InvioGeoPoint(latitude, longitude);
        waypoints.add(geoPoint);
    }

    private void appendParticle(final double latitude, final double longitude, final float weight) {
        Particle particle = new Particle(latitude, longitude, weight);
        particles.add(particle);
    }

    private List<InvioGeoPoint> waypoints;
    private List<Particle> particles;

    /*
    --------------------------------------------------
    ---------------- JNI GLUE METHODS ----------------
    --------------------------------------------------
     */

    private native long ncreate();

    private native void nrelease(final long ptr);

    private native int nstart(final long ptr);

    private native void nstop(final long ptr);

    private native void naddHistogram(final long ptr, final long histogramPtr);

    private native void naddSensorData(final long ptr, final float accX, final float accY, final float accZ,
                                       final float linAccX, final float linAccY, final float linAccZ,
                                       final float magX, final float magY, final float magZ);

    private native void nsetFingerprints(final long ptr, final long fingerprintsPtr);

    private native void nsetConfigParameter(final long ptr, final String param, final String value);

    private native String ngetConfigParameter(final long ptr, final String param);

    private native void nsetScale(final long ptr, final float scale);

    private native void nsetNorthAngle(final long ptr, final float northAngle);

    private native void nsetOccupancyGridMap(final long ptr, final float[] pixelValues, int width, int height);

    private native void nsetBoundingBox(final long ptr, final double minLat, final double minLong, final double maxLat,
                                        final double maxLong);

    private native void nsetLocalisationMode(final long ptr, final int mode);

    private native InvioGeoPoint ngetCurrentPosition(final long ptr);

    private native void ngetPlannedPath(final long ptr, final InvioGeoPoint start, final InvioGeoPoint goal);

    private native void ngetShortestPath(final long ptr, final InvioGeoPoint start,
                                                    final InvioGeoPoint[] goals);

    // Histogram helper functions.
    private native long ncreateTempHistogram(final String id);

    private native void nreleaseTempHistogram(final long ptr);

    private native void nputTempHistogram(final long ptr, final String key, final int rssi, final float divergence);

    //Fingerprint helper functions
    private native long ncreateTempFingerprintsArray();

    private native void nreleaseTempFingerprintsArray(final long ptr);

    private native void nputTempFingerprintsArray(final long ptr, final long histogramPtr,
                                                  final double latitude, final double longitude);

    private native void nsetDebugLogEnabled(final long ptr, final boolean enabled);

    private native void nparticles(final long ptr);

    private static native String nversion();

	public native TrackerConfigParameter[] ngetConfig(final long ptr);
}
