package de.tarent.invio.tracker;

/**
 * An exception that gets thrown when something happens within the tracker.
 */
public class TrackerException extends Exception {

    /**
     * Error code 0 when the tracker was started successfully.
     */
    public static final int SUCCESS = 0;

    /**
     * Error code 1 when the tracker was started with some optional parts are missing.
     */
    public static final int OPTIONAL_MISSING = 1;

    /**
     * Error code 2 when the tracker could not start because mandatory parts are missing.
     */
    public static final int MANDATORY_MISSING = 2;

    private int errorCode = -1;

    /**
     * Constructor.
     *
     * @param message   the error message
     * @param errorCode the error code
     */
    public TrackerException(final String message, final int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Returns the error message for the given error code.
     *
     * @return the error message
     */
    public String getErrorMessage() {
        switch (this.errorCode) {
            case SUCCESS:
                return "Tracker started successfully.";
            case OPTIONAL_MISSING:
                return "Tracker started with warnings because optional parts are missing.";
            case MANDATORY_MISSING:
                return "Tracker failed to start because mandatory parts are missing.";
            default:
                return "Tracker returned unknown error code: " + errorCode;
        }
    }

    public int getErrorCode() {
        return errorCode;
    }
}
