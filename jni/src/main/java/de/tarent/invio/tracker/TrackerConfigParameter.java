package de.tarent.invio.tracker;

/**
 * Configuration parameters of the Tracker, for use in the Admin App
 */
public class TrackerConfigParameter {

    public static final int INT = 0;
    public static final int BOOL = 1;
    public static final int DOUBLE = 2;
    public static final int STRING = 3;

    /**
     * The name of the parameter
     */
    public String name;

    /**
     * The description for the parameter
     */
    public String description; //NOSONAR - Does not need to be private.

    /**
     * The type of the parameter
     */
    public int type; //NOSONAR - Does not need to be private.

    /**
     * The minimum value of the parameter (undefined for STRING)
     */
    public float minValue; //NOSONAR - Does not need to be private.

    /**
     * The maximum value of the parameter (undefined for STRING)
     */
    public float maxValue; //NOSONAR - Does not need to be private.

    /**
     * The default value of the parameter
     */
    public String defaultValue; //NOSONAR - Does not need to be private.

    /**
     * The current of the parameter
     */
    public String currentValue; //NOSONAR - Does not need to be private.
}