mkdir build
cd build
cmake -DBUILD_STANDALONE=true -DBUILD_TEST=true -DBUILD_JNI=false -G "MSYS Makefiles" ../
mingw32-make install
cd ..