#!/bin/bash
mkdir -p build
cd build
set -e
cmake -DBUILD_STANDALONE=false -DBUILD_TEST=true -G "Unix Makefiles" ../
make install && [ "$1" = "-r" ] && (cd ..; ./build/dist/bin/tracker_test $*)
